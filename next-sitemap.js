// next-sitemap.js
module.exports = {
  siteUrl: 'https://www.example.com', // Thay bằng URL của bạn
  generateRobotsTxt: true,
  sitemapSize: 7000,
  changefreq: 'daily',
  priority: 0.7,
  // Thêm các cài đặt khác nếu cần
};


