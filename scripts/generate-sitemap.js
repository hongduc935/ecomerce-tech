const fs = require('fs');
const globby = require('globby');

(async () => {
  const pages = await globby([
    'pages/**/*{.js,.ts,.jsx,.tsx}', // Bao gồm các tệp trang
    '!pages/_*.js', // Loại trừ các tệp bắt đầu bằng dấu gạch dưới
    '!pages/api', // Loại trừ thư mục api
  ]);

  const sitemap = `
  <?xml version="1.0" encoding="UTF-8"?>
  <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
          xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">
    ${pages
      .map((page) => {
        const path = page
          .replace('pages', '')
          .replace('.js', '')
          .replace('.jsx', '')
          .replace('.ts', '')
          .replace('.tsx', '');
        const route = path === '/index' ? '' : path;

        return `
        <url>
          <loc>${`https://www.example.com${route}`}</loc>
          <image:image>
            <image:loc>https://www.example.com/images/sample.jpg</image:loc>
            <image:caption>Mô tả cho hình ảnh mẫu</image:caption>
            <image:title>Tiêu đề cho hình ảnh mẫu</image:title>
          </image:image>
        </url>
        `;
      })
      .join('')}
  </urlset>
  `;

  fs.writeFileSync('public/sitemap.xml', sitemap);
})();
