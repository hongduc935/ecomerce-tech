import { IAboutPage } from './AboutPage/index';
import { IHomePage } from './HomePage/index';
import { IBasePage } from './BasePage';
import { IDashBoard } from './DashBoard';
import { INotFoundPage } from './NotFound';
export type DataPage = IHomePage | IAboutPage | IBasePage | IDashBoard | INotFoundPage;
