

interface IUser {

  username:String;
  address:String;

}

interface IResponse {
  msg:string;
  statusCode:Number; 
}

export interface IRegisterRes extends IResponse{
  data:any
}