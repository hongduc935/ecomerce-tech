import { TYPE_PAGE } from '@/@types/TypePage/typePage';
import ObjectSW from '../ObjectSW';

class ObjectSWPage extends ObjectSW {

	private obj: Record<TYPE_PAGE, any>;

	constructor(obj: Record<TYPE_PAGE, any>) {
		super();
		this.obj = obj;
	}

	get(key: typeof TYPE_PAGE[keyof typeof TYPE_PAGE]): any {
		return this.obj[key];
	}
  
}

export default ObjectSWPage;
