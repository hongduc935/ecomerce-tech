const Authenticated = (ctx: any) => {
	const { cookies } = ctx.req;
	if (cookies.get('token')) {
		//Pass case
		// Call API get profile user
	}
	//redirect to login page
	return undefined;
};

const UnAuthenticated = (ctx: any) => {
	const { cookies } = ctx.req;
	if (cookies.get('token')) {
		// call API get profile user
	}
	// return user
	return undefined;
};

const Auth = { Authenticated, UnAuthenticated };

export default Auth;
