import type {
  FetchFnReturnDataType,
  FetchFnReturnErrorType,
  FetchFnReturnExtraType,
  FetchFnReturnType,
  FetchFnType,
} from "./interface";
import { injectParams } from "@/infra/fetch/util";

const interceptors: {
  headers:
    | (() => Record<string, string> | Promise<Record<string, string> | undefined> | undefined)
    | undefined;
} = {
  headers: undefined,
};

export const updateFetchFnInterceptors = (
  headerInterceptors: () =>
    | Record<string, string>
    | Promise<Record<string, string> | undefined>
    | undefined,
) => {
  Object.assign(interceptors, {
    headers: headerInterceptors,
  });
};

const getHeaders = async ({
  headersRaw,
  isFormData,
}: {
  headersRaw?: Record<string, string>;
  isFormData: boolean;
}) => {
  let headers: Record<string, string> = {};
  if (!isFormData) {
    headers["Content-Type"] = "application/json";
  }
  if (typeof interceptors.headers === "function") {
    headers = { ...headers, ...(await interceptors.headers()) };
  }
  headers = { ...headers, ...headersRaw };

  return headers;
};

export async function fetchFn<T, ExtraType = undefined>({
  url,
  method = "GET",
  headers,
  params,
  query,
  body,
  dataParser,
  throwOnError,
}: FetchFnType<T, ExtraType>): Promise<FetchFnReturnType<T, ExtraType>> {
  const isBodyInstanceofFormData = body instanceof FormData;
  const fetchUrl = injectParams(url, { params, query });
  const options: RequestInit = {
    method,
    headers: await getHeaders({
      headersRaw: headers,
      isFormData: isBodyInstanceofFormData,
    }),
    body: method === "GET" ? undefined : isBodyInstanceofFormData ? body : JSON.stringify(body),
  };

  const before = Date.now();

  let raw;
  let res: FetchFnReturnType<T, ExtraType>;

  // Fetch API
  try {
    raw = await fetch(fetchUrl, options);
  } catch (e) {
    const err = e as FetchFnReturnErrorType["error"];
    raw = {
      ok: false,
      status: err.status || 500,
      error: err,
    };
  }

  console.info("fetch", {
    url: (options.method ?? "GET") + " " + fetchUrl,
    body: options.body,
    status: raw.status,
    duration: Math.floor((Date.now() - before) / 1000),
  });

  if (raw instanceof Response) {
    let json: any;

    const contentType = raw.headers.get("Content-Type");
    const contentDisposition = raw.headers.get("Content-Disposition");

    if (contentDisposition?.includes("attachment")) {
      json = {
        filename: contentDisposition.split("filename=")[1],
        data: new Uint8Array(await raw.arrayBuffer()),
      };
    } else {
      json = contentType?.includes("application/json") ? await raw.json() : await raw.text();
    }

    const jsonBody: { data?: T; extra?: ExtraType; ok?: boolean } = (() => {
      if (!dataParser) {
        return { data: json };
      }

      let parsed;
      try {
        parsed = dataParser(json, { ok: raw.ok, status: raw.status });
      } catch {
        parsed = { data: json as T };
      }

      if (
        parsed &&
        typeof parsed === "object" &&
        ("data" in parsed || "extra" in parsed || "ok" in parsed)
      ) {
        return parsed as { data?: T; extra?: ExtraType; ok?: boolean };
      }
      return { data: parsed };
    })();

    const ok = raw.ok && (jsonBody.ok === undefined || jsonBody.ok);

    if (ok) {
      res = {
        isError: false,
        ...(jsonBody as FetchFnReturnDataType<T> & FetchFnReturnExtraType<ExtraType>),
      };
    } else {
      let errorMessage;
      // @ts-expect-error skip type check
      if (typeof jsonBody.data?.message === "string") errorMessage = jsonBody.data.message;
      // @ts-expect-error skip type check
      else if (typeof jsonBody.extra?.message === "string") errorMessage = jsonBody.extra.message;
      // @ts-expect-error skip type check
      else if (typeof jsonBody.data?.error === "string") errorMessage = jsonBody.data.error;
      // @ts-expect-error skip type check
      else if (typeof jsonBody.extra?.error === "string") errorMessage = jsonBody.extra.error;
      else if (typeof jsonBody.data === "string") errorMessage = jsonBody.data;
      else if (jsonBody.data) errorMessage = JSON.stringify(jsonBody.data);
      else errorMessage = "Unknown error";

      res = {
        isError: true,
        error: {
          status: raw.status,
          message: errorMessage,
        },
      };
    }
  } else {
    res = {
      isError: true,
      error: {
        status: raw.status,
        message: raw.error.message,
      },
    };
  }

  if ("error" in res && throwOnError) {
    // eslint-disable-next-line
    throw res.error;
  }

  return res;
}
