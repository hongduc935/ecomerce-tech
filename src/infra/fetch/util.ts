import { type FetchFnType } from "@/infra/fetch/interface";

export const getQueryKey = <T, ExtraType = undefined>(
  props: FetchFnType<T, ExtraType> & { prefix?: string },
): unknown[] => {
  const { prefix, url, params, query, body } = props;
  return [prefix, url, params, query, body];
};

export const injectParams = (
  url: string,
  { params, query }: { params?: Record<string, unknown>; query?: Record<string, unknown> } = {},
) => {
  if (!url) {
    console.error({ url, params }, "Empty url");
    return "";
  }
  let parsedUrl = `${url}`;

  if (params) {
    Object.keys(params).forEach((key) => {
      parsedUrl = parsedUrl.replace("#{" + key + "}", String(params[key]));
    });
    if (parsedUrl.includes("#")) {
      console.error({ url: parsedUrl, params }, "Missing params for url");
    }
  }
  if (query) {
    const queryWithoutNull: Record<string, string> = {};
    for (const key in query) {
      if (!query[key] && query[key] !== 0) {
        continue;
      }
      queryWithoutNull[key] = String(query[key]);
    }
    const parsedQuery = new URLSearchParams(queryWithoutNull);
    parsedUrl = parsedUrl + "?" + parsedQuery.toString();
  }

  return parsedUrl;
};
