import {
  type InfiniteQueryType,
  type MutationOptionsType,
  type MutationType,
  type QueryOptionsType,
  type QueryType,
} from "./interface";

export { fetchFn } from "./fetchFn";
export { useFetcher } from "./useFetcher";

export type { FetchFnType } from "./interface";

// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace FetcherType {
  export type Query<T, ExtraType = undefined> = QueryType<T, ExtraType>;
  export type InfiniteQuery<T, ExtraType = undefined> = InfiniteQueryType<T, ExtraType>;
  export type Mutation<T, ExtraType = undefined> = MutationType<T, ExtraType>;

  export type QueryOptions<T, ExtraType = undefined> = QueryOptionsType<T, ExtraType>;
  export type MutationOptions<T, ExtraType = undefined> = MutationOptionsType<T, ExtraType>;
}
