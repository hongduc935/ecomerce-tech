import { useInfiniteQuery } from "./useInfiniteQuery";
import { useMutation } from "./useMutation";
import { useQuery } from "./useQuery";
import {
  type FetcherReturnType,
  type FetcherType,
  type InfiniteQueryReturnType,
  type InfiniteQueryType,
  type MutationReturnType,
  type MutationType,
  type QueryReturnType,
  type QueryType,
} from "@/infra/fetch/interface";

export function useFetcher<T, ExtraType = undefined>(
  options: QueryType<T, ExtraType>,
): QueryReturnType<T, ExtraType>;
export function useFetcher<T, ExtraType = undefined>(
  options: InfiniteQueryType<T, ExtraType>,
): InfiniteQueryReturnType<T, ExtraType>;
export function useFetcher<T, ExtraType = undefined>(
  options: MutationType<T, ExtraType>,
): MutationReturnType<T, ExtraType>;
export function useFetcher<T, ExtraType = undefined>(
  options: FetcherType<T, ExtraType>,
): FetcherReturnType<T, ExtraType> {
  switch (options.type) {
    case undefined:
    case "query":
      return useQuery<T, ExtraType>(options);
    case "infinite_query":
      return useInfiniteQuery<T, ExtraType>(options);
    case "mutation":
      return useMutation<T, ExtraType>(options);
    default:
      throw new Error("Unsupported fetcher type");
  }
}
