"use client";

import { useQuery as useRQuery } from "@tanstack/react-query";
import { useEffect } from "react";
import { fetchFn } from "@/infra/fetch/fetchFn";
import type {
  FetchFnReturnDataType,
  FetchFnReturnErrorType,
  FetchFnReturnExtraType,
  FetchFnReturnType,
  QueryReturnType,
  QueryType,
} from "@/infra/fetch/interface";
import { getQueryKey } from "@/infra/fetch/util";
import { queryClient } from "@/infra/react-query";

export function useQuery<T, ExtraType = undefined>({
  type,
  keepPreviousDataAsPlaceholder,
  placeholderData,
  queryOptions,
  queryKey: inputQueryKey,
  querySelect,
  prefix,
  ...fetchFnProps
}: QueryType<T, ExtraType>): QueryReturnType<T, ExtraType> {
  const queryKey = inputQueryKey ?? getQueryKey({ prefix, ...fetchFnProps });

  const {
    data,
    isFetching,
    isError,
    error,
    refetch: refetchQuery,
  } = useRQuery<FetchFnReturnType<T, ExtraType>, FetchFnReturnErrorType["error"]>({
    queryKey,
    queryFn: async () => await fetchFn({ ...fetchFnProps, throwOnError: true }),
    placeholderData: (previousData) => {
      if (keepPreviousDataAsPlaceholder && previousData) return previousData;
      return placeholderData;
    },
    ...queryOptions,
  });

  const selected = querySelect && !isError && data ? querySelect(data.data) : undefined;
  const selectedData = (() => {
    if (!selected) return;

    if (typeof selected === "object" && ("data" in selected || "extra" in selected)) {
      return selected;
    }
    return { data: selected };
  })() as { data: T; extra: ExtraType } | undefined;

  const refetch = ({ refetchByPrefix = false } = {}) => {
    if (refetchByPrefix && prefix) {
      // INFO: Wait for the query key updated (e.g: go to page 1) before refetch
      setTimeout(() => {
        void queryClient.invalidateQueries({ queryKey: [prefix] });
      }, 0);
    } else {
      void refetchQuery();
    }
  };

  useEffect(() => {
    async function handleLifecycle() {
      if (isFetching) return;

      if (isError && queryOptions?.onError) {
        await queryOptions.onError(error);
        if (queryOptions.onSettled) {
          // @ts-expect-error skip type check
          await queryOptions.onSettled(undefined, error);
        }
      } else if (data && !data.isError && queryOptions?.onSuccess) {
        await queryOptions.onSuccess(selectedData ?? data);
        if (queryOptions.onSettled) {
          // @ts-expect-error skip type check
          await queryOptions.onSettled(selectedData ?? data, undefined);
        }
      }
    }

    void handleLifecycle();
  }, [data, error]);

  if (isFetching) {
    // @ts-expect-error skip type check
    return {
      isLoading: true,
      isError: false,
      type,
      queryKey,
      prefix,
      refetch,
      ...(selectedData ?? data),
    };
  }

  if (isError) {
    return {
      isLoading: false,
      isError: true,
      type,
      queryKey,
      prefix,
      refetch,
      error,
      ...(selectedData ??
        (data as Partial<FetchFnReturnDataType<T> & FetchFnReturnExtraType<ExtraType>>)),
    };
  }

  return {
    isLoading: false,
    isError: false,
    type,
    queryKey,
    prefix,
    refetch,
    ...(selectedData ?? (data as FetchFnReturnDataType<T> & FetchFnReturnExtraType<ExtraType>)),
  };
}
