"use client";

import { useMutation as useRMutation } from "@tanstack/react-query";
import { fetchFn } from "@/infra/fetch/fetchFn";
import type {
  FetchFnReturnDataType,
  FetchFnReturnErrorType,
  FetchFnReturnExtraType,
  FetchFnReturnType,
  MutationReturnType,
  MutationType,
} from "@/infra/fetch/interface";
import { getQueryKey } from "@/infra/fetch/util";

export function useMutation<T, ExtraType = undefined>({
  type,
  mutationOptions,
  mutationKey: inputMutationKey,
  prefix,
  ...fetchFnProps
}: MutationType<T, ExtraType>): MutationReturnType<T, ExtraType> {
  const mutationKey = inputMutationKey ?? getQueryKey({ prefix, ...fetchFnProps });

  const { data, isPending, isError, error, mutate } = useRMutation<
    FetchFnReturnType<T, ExtraType>,
    FetchFnReturnErrorType["error"]
    // @ts-expect-error skip type check
  >({
    mutationKey,
    mutationFn: async (body: unknown) =>
      await fetchFn({
        ...fetchFnProps,
        body: body as Record<string, unknown> | FormData,
        throwOnError: true,
      }),
    ...mutationOptions,
  });
  const mutateFn = mutate as (body: unknown) => void;

  if (isPending) {
    return {
      isLoading: true,
      isError: false,
      type,
      mutate: mutateFn,
      mutationKey,
      prefix,
    };
  }

  if (isError) {
    return {
      isLoading: false,
      isError: true,
      type,
      mutate: mutateFn,
      mutationKey,
      prefix,
      error,
    };
  }

  return {
    isLoading: false,
    isError: false,
    type,
    mutate: mutateFn,
    mutationKey,
    prefix,
    ...(data as FetchFnReturnDataType<T> & FetchFnReturnExtraType<ExtraType>),
  };
}
