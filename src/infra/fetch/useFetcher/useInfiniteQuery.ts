"use client";

import { useInfiniteQuery as useRInfiniteQuery } from "@tanstack/react-query";
import { useEffect } from "react";
import { fetchFn } from "@/infra/fetch/fetchFn";
import type {
  FetchFnReturnErrorType,
  FetchFnReturnType,
  InfiniteQueryReturnType,
  InfiniteQueryType,
} from "@/infra/fetch/interface";
import { getQueryKey } from "@/infra/fetch/util";

export function useInfiniteQuery<T, ExtraType = undefined>({
  type,
  initialPageParam,
  queryOptions,
  queryKey: inputQueryKey,
  prefix,
  ...fetchFnProps
}: InfiniteQueryType<T, ExtraType>): InfiniteQueryReturnType<T, ExtraType> {
  const queryKey = inputQueryKey ?? getQueryKey({ prefix, ...fetchFnProps });

  const { data, isFetching, isError, error, hasNextPage, fetchNextPage } = useRInfiniteQuery<
    FetchFnReturnType<T, ExtraType>,
    FetchFnReturnErrorType["error"]
  >({
    queryKey,
    queryFn: async ({ pageParam }) =>
      await fetchFn({
        ...fetchFnProps,
        query: {
          ...fetchFnProps.query,
          ...(pageParam as Record<string, unknown> | undefined),
        },
        throwOnError: true,
      }),
    initialPageParam,
    getNextPageParam: (lastPage, _, lastPageParam) => {
      if (lastPage.isError) return;

      const extra = lastPage.extra as Record<string, unknown>;
      const lastPageParamRecord = lastPageParam as Record<string, unknown>;

      if (extra.page) {
        const nextPage = Number(extra.page) + 1;
        return { ...lastPageParamRecord, page: nextPage };
      }

      if (extra.cursor) {
        return { ...lastPageParamRecord, cursor: extra.cursor };
      }
    },
    ...queryOptions,
  });

  const lastPage = data?.pages.slice(-1)[0];
  const selectedData = {
    data: (data?.pages.flatMap((page) => page.data ?? []) ?? []) as T,
    extra: (lastPage && !lastPage.isError ? lastPage.extra : undefined) as ExtraType,
  };

  useEffect(() => {
    async function handleLifecycle() {
      if (isFetching) return;

      if (isError && queryOptions?.onError) {
        await queryOptions.onError(error);
        if (queryOptions.onSettled) {
          // @ts-expect-error skip type check
          await queryOptions.onSettled(undefined, error);
        }
      } else if (queryOptions?.onSuccess) {
        await queryOptions.onSuccess(selectedData);
        if (queryOptions.onSettled) {
          // @ts-expect-error skip type check
          await queryOptions.onSettled(selectedData, undefined);
        }
      }
    }

    void handleLifecycle();
  }, [data, error]);

  if (isFetching) {
    return {
      isLoading: true,
      isError: false,
      type,
      hasNextPage,
      fetchNextPage,
      queryKey,
      prefix,
      ...selectedData,
    };
  }

  if (isError) {
    return {
      isLoading: false,
      isError: true,
      type,
      hasNextPage,
      fetchNextPage,
      queryKey,
      prefix,
      error,
      ...selectedData,
    };
  }

  return {
    isLoading: false,
    isError: false,
    type,
    hasNextPage,
    fetchNextPage,
    queryKey,
    prefix,
    ...selectedData,
  };
}
