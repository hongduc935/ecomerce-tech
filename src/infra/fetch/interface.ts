// FetchFn interface
export interface FetchFnType<T, ExtraType = undefined> {
  url: string;
  method?: "GET" | "POST" | "PUT" | "DELETE" | "PATCH";
  headers?: Record<string, string>;
  params?: Record<string, unknown>;
  query?: Record<string, unknown>;
  body?: Record<string, unknown> | FormData;
  /**
   * Transform response body to desired data structure.
   * Support transform success response to error response (e.g: api always return 200 but return error message in body).
   * For type safety, you should define BodyType on the fly.
   */
  dataParser?: (
    body: any,
    options: { ok: boolean; status: number },
  ) => T | { ok?: boolean; data: T; extra?: ExtraType };
  throwOnError?: boolean;
  usePomerium?: boolean;
}

export interface FetchFnReturnExtraType<T> {
  extra: T;
}

export interface FetchFnReturnDataType<T> {
  data: T;
}

export interface FetchFnReturnErrorType {
  error: {
    status: number;
    message: string;
  };
}

export type FetchFnReturnType<T, ExtraType = undefined> =
  | ({ isError: false } & FetchFnReturnDataType<T> &
      FetchFnReturnExtraType<ExtraType> &
      Partial<FetchFnReturnErrorType>)
  | ({ isError: true } & FetchFnReturnErrorType &
      Partial<FetchFnReturnDataType<T> & FetchFnReturnExtraType<ExtraType>>);

// Fetcher interface
export interface QueryOptionsType<T, ExtraType = undefined> {
  onSuccess?: (
    data: FetchFnReturnDataType<T> & FetchFnReturnExtraType<ExtraType>,
  ) => Promise<unknown> | unknown;
  onError?: (error: FetchFnReturnErrorType["error"]) => Promise<unknown> | unknown;
  onSettled?: (
    data: Partial<FetchFnReturnDataType<T> & FetchFnReturnExtraType<ExtraType>>,
    error: Partial<FetchFnReturnErrorType["error"]>,
  ) => Promise<unknown> | unknown;
  [key: string]: unknown;
}

export interface MutationOptionsType<T, ExtraType = undefined> {
  onMutate?: () => Promise<unknown> | unknown;
  onSuccess?: (
    data: FetchFnReturnDataType<T> & FetchFnReturnExtraType<ExtraType>,
  ) => Promise<unknown> | unknown;
  onError?: (error: FetchFnReturnErrorType["error"]) => Promise<unknown> | unknown;
  onSettled?: (
    data: Partial<FetchFnReturnDataType<T> & FetchFnReturnExtraType<ExtraType>>,
    error: Partial<FetchFnReturnErrorType["error"]>,
  ) => Promise<unknown> | unknown;
  [key: string]: unknown;
}

export type QueryType<T, ExtraType = undefined> = FetchFnType<T, ExtraType> & {
  type?: "query";
  keepPreviousDataAsPlaceholder?: boolean;
  placeholderData?: FetchFnReturnType<T, ExtraType>;
  queryOptions?: QueryOptionsType<T, ExtraType>;
  queryKey?: unknown[];
  /**
   * Transform response body to desired data structure after cache.
   * Support success query only. Check dataParser for more information.
   * For type safety, you should define BodyType on the fly.
   */
  querySelect?: (body: any) => T | { data: T; extra?: ExtraType };
  prefix?: string;
};

export type QueryReturnType<T, ExtraType = undefined> = {
  type?: "query";
  queryKey: unknown[];
  prefix?: string;
  refetch: (options?: { refetchByPrefix?: boolean }) => void;
} & (
  | ({ isLoading: true; isError: false } & Partial<
      FetchFnReturnDataType<T> & FetchFnReturnExtraType<ExtraType> & FetchFnReturnErrorType
    >)
  | ({ isLoading: false; isError: true } & Partial<
      FetchFnReturnDataType<T> & FetchFnReturnExtraType<ExtraType>
    > &
      FetchFnReturnErrorType)
  | (({ isLoading: false; isError: false } & FetchFnReturnDataType<T> &
      FetchFnReturnExtraType<ExtraType>) &
      Partial<FetchFnReturnErrorType>)
);

export type InfiniteQueryType<T, ExtraType> = FetchFnType<T, ExtraType> & {
  type: "infinite_query";
  initialPageParam?: unknown;
  queryOptions?: QueryOptionsType<T, ExtraType>;
  queryKey?: unknown[];
  prefix?: string;
};

export type InfiniteQueryReturnType<T, ExtraType = undefined> = {
  type: "infinite_query";
  hasNextPage: boolean;
  fetchNextPage: () => Promise<unknown>;
  queryKey: unknown[];
  prefix?: string;
} & (
  | ({ isLoading: true; isError: false } & Partial<
      FetchFnReturnDataType<T> & FetchFnReturnExtraType<ExtraType> & FetchFnReturnErrorType
    >)
  | ({ isLoading: false; isError: true } & Partial<
      FetchFnReturnDataType<T> & FetchFnReturnExtraType<ExtraType>
    > &
      FetchFnReturnErrorType)
  | (({ isLoading: false; isError: false } & FetchFnReturnDataType<T> &
      FetchFnReturnExtraType<ExtraType>) &
      Partial<FetchFnReturnErrorType>)
);

export type MutationType<T, ExtraType> = FetchFnType<T, ExtraType> & {
  type: "mutation";
  mutationOptions?: MutationOptionsType<T, ExtraType>;
  mutationKey?: unknown[];
  prefix?: string;
};

export type MutationReturnType<T, ExtraType = undefined> = {
  type: "mutation";
  mutate: (body: unknown) => void;
  mutationKey: unknown[];
  prefix?: string;
} & (
  | ({ isLoading: true; isError: false } & Partial<
      FetchFnReturnDataType<T> & FetchFnReturnExtraType<ExtraType> & FetchFnReturnErrorType
    >)
  | ({ isLoading: false; isError: true } & Partial<
      FetchFnReturnDataType<T> & FetchFnReturnExtraType<ExtraType>
    > &
      FetchFnReturnErrorType)
  | (({ isLoading: false; isError: false } & FetchFnReturnDataType<T> &
      FetchFnReturnExtraType<ExtraType>) &
      Partial<FetchFnReturnErrorType>)
);

export type FetcherType<T, ExtraType = undefined> =
  | QueryType<T, ExtraType>
  | InfiniteQueryType<T, ExtraType>
  | MutationType<T, ExtraType>;

export type FetcherReturnType<T, ExtraType = undefined> =
  | QueryReturnType<T, ExtraType>
  | InfiniteQueryReturnType<T, ExtraType>
  | MutationReturnType<T, ExtraType>;
