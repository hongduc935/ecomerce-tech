import { QueryCache, QueryClient } from "@tanstack/react-query";

export const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      staleTime: 5 * 60 * 1000,
      retry: false,
    },
  },
  queryCache: new QueryCache({
    onError: (error, query) => {
      const { errorMessage, shouldShowError = true } = (query.meta ?? {}) as {
        errorMessage?: string;
        shouldShowError?: boolean;
        getErrorMessage?: () => string;
      };

      if (shouldShowError) {
        console.error(errorMessage ?? error.message);
      }
    },
  }),
});
