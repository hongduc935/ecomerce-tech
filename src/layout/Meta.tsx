// import Head from 'next/head';
// import { useRouter } from 'next/router';
// import { NextSeo } from 'next-seo';
// import { AppConfig } from '../utils/AppConfig';

// type IMetaProps = {
//   title: string;
//   description: string;
//   canonical?: string;
//   image_url?: string;
// };

// const Meta = (props: IMetaProps) => {
  
//   const router = useRouter();
//   const defaultImage = `${router.basePath}/default-image.jpg`; 

//   return (
//     <>
//       <Head>
//         <meta charSet="UTF-8" key="charset" />
//         <meta
//           name="viewport"
//           content="width=device-width,initial-scale=1"
//           key="viewport"
//         />
//         <meta name="robots" content="index, follow" key="robots" />
//         <link
//           rel="apple-touch-icon"
//           href={`${router.basePath}/apple-touch-icon.png`}
//           key="apple"
//         />
//         <link
//           rel="icon"
//           type="image/png"
//           sizes="32x32"
//           href={`${router.basePath}/favicon-32x32.png`}
//           key="icon32"
//         />
//         <link
//           rel="icon"
//           type="image/png"
//           sizes="16x16"
//           href={`${router.basePath}/favicon-16x16.png`}
//           key="icon16"
//         />
//         <link
//           rel="icon"
//           href={`${router.basePath}/favicon.ico`}
//           key="favicon"
//         />
//         {/* Thực hiện tra cứu DNS, thiết lập kết nối TCP, */}
//         <link rel="preconnect" href="//https://store.storeimages.cdn-apple.com"/>
//         {/* Thực hiện tra cứu DNS trước khi tải các tải nguyên */}
//         <link rel="dns-prefetch" href="//https://store.storeimages.cdn-apple.com"/>
//         <script type="application/ld+json">
//           {JSON.stringify({
//             "@context": "https://schema.org",
//             "@type": "BlogPosting",
//             "headline": props.title,
//             "image": props.image_url || defaultImage,
//             "author": {
//               "@type": "Person",
//               "name": "Your Name"
//             },
//             "datePublished": new Date(), // Chỉnh theo ngày thực tế
//             "description": props.description,
//           })}
//         </script>
//       </Head>
//       <NextSeo
//         title={props.title}
//         description={props.description}
//         canonical={props.canonical}
//         openGraph={{
//           title: props.title,
//           description: props.description,
//           url: props.canonical,
//           locale: AppConfig.locale,
//           site_name: AppConfig.site_name,
//           images: [
//             {
//               url: props.image_url || defaultImage, // Hình ảnh hiển thị khi chia sẻ
//               width: 1200,
//               height: 630,
//               alt: props.title,
//             },
//           ],
//         }}
//         twitter={{
//           handle: '@yourTwitterHandle',
//           site: '@yourTwitterHandle',
//           cardType: 'summary_large_image', // Hiển thị dạng card với hình ảnh lớn
//         }}
//       />
//     </>
//   );
// };

// export { Meta };

import Head from 'next/head';
import { useRouter } from 'next/router';
import { NextSeo } from 'next-seo';
import { AppConfig } from '../utils/AppConfig';

type IMetaProps = {
  title: string;
  description: string;
  canonical?: string;
  image_url?: string;
};

const Meta = (props: IMetaProps) => {
  
  const router = useRouter();
  const defaultImage = `${router.basePath}/default-image.jpg`; 

  const jsonLdData = {
    "@context": "https://schema.org",
    "@type": "BlogPosting",
    "headline": props.title,
    "image": props.image_url || defaultImage,
    "author": {
      "@type": "Person",
      "name": "Your Name"
    },
    "datePublished": new Date().toISOString(),
    "description": props.description,
  };

  return (
    <>
      <Head>
        <meta charSet="UTF-8" key="charset" />
        <meta
          name="viewport"
          content="width=device-width,initial-scale=1"
          key="viewport"
        />
        <meta name="robots" content="index, follow" key="robots" />
        <link
          rel="apple-touch-icon"
          href={`${router.basePath}/apple-touch-icon.png`}
          key="apple"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href={`${router.basePath}/favicon-32x32.png`}
          key="icon32"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href={`${router.basePath}/favicon-16x16.png`}
          key="icon16"
        />
        <link
          rel="icon"
          href={`${router.basePath}/favicon.ico`}
          key="favicon"
        />
        <link rel="preconnect" href="https://store.storeimages.cdn-apple.com"/>
        <link rel="dns-prefetch" href="https://store.storeimages.cdn-apple.com"/>
        <script type="application/ld+json" dangerouslySetInnerHTML={{ __html: JSON.stringify(jsonLdData) }} />
      </Head>
      <NextSeo
        title={props.title}
        description={props.description}
        canonical={props.canonical}
        openGraph={{
          title: props.title,
          description: props.description,
          url: props.canonical,
          locale: AppConfig.locale,
          site_name: AppConfig.site_name,
          images: [
            {
              url: props.image_url || defaultImage,
              width: 1200,
              height: 630,
              alt: props.title,
            },
          ],
        }}
        twitter={{
          handle: '@yourTwitterHandle',
          site: '@yourTwitterHandle',
          cardType: 'summary_large_image',
        }}
      />
    </>
  );
};

export { Meta };

