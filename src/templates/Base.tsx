import { NextPage } from 'next'
import { Meta } from '../layout/Meta'
import { AppConfig } from '../utils/AppConfig'
import { Banner } from './Banner'
import { Footer } from './Footer'
// import { Hero } from './Hero'
import { Sponsors } from './Sponsors'
import { VerticalFeatures } from './VerticalFeatures'

const Base:NextPage = () => {

  return <div className="text-gray-600 antialiased">
    <Meta title={AppConfig.title} description={AppConfig.description} />
    {/* <Hero /> */}
    <Sponsors />
    <VerticalFeatures />
    <Banner />
    <Footer />
  </div>

};

export default  Base ;
