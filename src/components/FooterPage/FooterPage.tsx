import React from 'react'
import styles from './styles/footer-page.module.scss'
const FooterPage = () => {
  return (
    <div className={`${styles.footer} container`}>
        <div className={`${styles.mainFooter} item-btw`}>
            <div className={`${styles.item1}  ${styles.item}`}>
                <p className={styles.title}>IT QUY NHƠN</p>
                <p className={styles.note}>Địa chỉ 1: 201/10 Lê Văn Việt , Phường Hiệp Phú , Thành Phố Thủ Đức</p>
                <p className={styles.note}>Địa chỉ 2: 276 Đường Đào Tấn , Phường Nhơn Bình , Quy Nhơn , Bình Định</p>
                <p className={styles.note}>Fangpage : https://www.facebook.com/duc.hong.58760608/</p>
                <p className={styles.note}>Hotline 0354 298 203</p>
                <p className={styles.note}>E-mail: hongduc935@gmail.com</p>
                <p className={styles.title}>TRUYỀN THÔNG VÀ XÃ HỘI</p>
                <ul className={`${styles.icon} `}>
                    <li className={styles.itemIC}>
                      <div className={`${styles.ic} ${styles.fb}`}/>
                    </li>
                    <li className={styles.itemIC}>
                      <div className={`${styles.ic} ${styles.insta}`}/>
                    </li>
                    <li className={styles.itemIC}>
                      <div className={`${styles.ic} ${styles.mess}`}/>
                    </li>
              
                    <li className={styles.itemIC}>
                    <div className={`${styles.ic} ${styles.zalo}`}/>
                    </li >
                </ul>
            </div>
            <div className={`${styles.item1}  ${styles.item}`}>
                <p className={styles.title}>THÔNG TIN CHUNG</p>
                <p className={styles.note}>Giới thiệu</p>
                <p className={styles.note}>Sản phẩm</p>
                <p className={styles.note}>Tin Tức</p>
                <p className={styles.note}>Liên hệ</p>
                <p className={styles.note}>Điều khoản và điều kiện</p>
                <p className={styles.note}>Chính sách bảo mật thông tin</p>
            </div>
            <div className={`${styles.item1}  ${styles.item}`}>
                <p className={styles.title}>IT QUY NHƠN</p>
                <p className={styles.note}>Quy định về đặt mua sản phẩm, dịch vụ</p>
                <p className={styles.note}>Hướng dẫn thanh toán và thủ tục hoàn tiền</p>
                <p className={styles.note}>Hướng dẫn xử lý sự cố và khiếu nại</p>
                <p className={styles.note}>Đăng ký làm đại lý</p>
            </div>
        </div>
        <div className={`${styles.copyright} item-btw container`}>
            <div className={styles.source}>
                <p className={styles.shareContent}>© {new Date().getFullYear()}  Bản quyền thuộc về IT QUY NHƠN</p>
            </div>
            <div className={`${styles.right} item-center`}>
                <p className={styles.shareContent}>Điều khoản và điều kiện</p>
                <p className={styles.shareContent}> Chính sách bảo mật thông tin</p>
            </div>

        </div>
      
    </div>


  )
}

export default FooterPage
