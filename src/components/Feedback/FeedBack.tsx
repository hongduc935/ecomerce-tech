import React from 'react'
import styles from './styles/feedback.module.scss'


const FeedBack = () => {
  return (
    <section id='fbForm' className={styles.feedbackForm}>
      <div className={`${styles.gridFeedback} container`}>
          <h2 className={styles.titleFeedback}>Thu thập độ hài lòng của khách hàng tại IT Quy Nhơn</h2>
          <div className={styles.frameItem}>
            <div className={styles.rowItem}>
                <div className={styles.item}>
                    <input className={styles.ckb} type='checkbox' />
                    <p className={styles.standard}> &nbsp; Chất lượng phục vụ của nhân viên </p>
                </div>
                <div className={styles.item}>
                    <input className={styles.ckb} type='checkbox' />
                    <p className={styles.standard}> &nbsp; Chất lượng phục vụ của nhân viên </p>
                </div>
                <div className={styles.item}>
                    <input className={styles.ckb} type='checkbox' />
                    <p className={styles.standard}> &nbsp; Chất lượng phục vụ của nhân viên </p>
                </div>
             
            </div>
            <div className={styles.rowItem}>
                <div className={styles.item}>
                    <input className={styles.ckb} type='checkbox' />
                    <p className={styles.standard}> &nbsp; Chất lượng phục vụ của nhân viên </p>
                </div>
                <div className={styles.item}>
                    <input className={styles.ckb} type='checkbox' />
                    <p className={styles.standard}> &nbsp; Chất lượng phục vụ của nhân viên </p>
                </div>
                <div className={styles.item}>
                    <input className={styles.ckb} type='checkbox' />
                    <p className={styles.standard}> &nbsp; Chất lượng phục vụ của nhân viên </p>
                </div>
            </div>
          </div>
          
      </div>
    </section>
  )
}

export default FeedBack