import React from 'react';
import { motion } from 'framer-motion';

type Props = {
  children?: React.ReactNode;
};

// Hiệu ứng Slide In từ phải
const SlideInRight: React.FC<Props> = ({ children }) => {
  const slideInRight = {
    hidden: { x: 100, opacity: 0 },
    visible: { x: 0, opacity: 1 },
  };

  return (
    <motion.div
      initial="hidden"
      whileInView="visible"
      viewport={{ once: true }}
      transition={{ duration: 0.6 }}
      variants={slideInRight}
    >
      {children}
    </motion.div>
  );
};
export default SlideInRight