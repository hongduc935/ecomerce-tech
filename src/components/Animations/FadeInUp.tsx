import React from 'react';
import { motion } from 'framer-motion';

type Props = {
  children?: React.ReactNode;
};

// Hiệu ứng Fade In Up
const FadeInUp: React.FC<Props> = ({ children }) => {
  const fadeInUp = {
    hidden: { opacity: 0, y: 50 },
    visible: { opacity: 1, y: 0 },
  };

  return (
    <motion.div
      initial="hidden"
      whileInView="visible"
      viewport={{ once: true }}
      transition={{ duration: 0.6 }}
      variants={fadeInUp}
    >
      {children}
    </motion.div>
  );
};




export default FadeInUp ;
