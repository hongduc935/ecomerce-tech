import React from 'react';
import { motion } from 'framer-motion';

type Props = {
  children?: React.ReactNode;
};
// Hiệu ứng Zoom In
const ZoomIn: React.FC<Props> = ({ children }) => {
  const zoomIn = {
    hidden: { scale: 0.8, opacity: 0 },
    visible: { scale: 1, opacity: 1 },
  };

  return (
    <motion.div
      initial="hidden"
      whileInView="visible"
      viewport={{ once: true }}
      transition={{ duration: 0.6 }}
      variants={zoomIn}
    >
      {children} 
    </motion.div>
  );
};

export default ZoomIn