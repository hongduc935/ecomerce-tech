import React from 'react';
import { motion } from 'framer-motion';

type Props = {
  children?: React.ReactNode;
};

// Hiệu ứng Slide In từ trái
const SlideInLeft: React.FC<Props> = ({ children }) => {
  const slideInLeft = {
    hidden: { x: -100, opacity: 0 },
    visible: { x: 0, opacity: 1 },
  };

  return (
    <motion.div
      initial="hidden"
      whileInView="visible"
      viewport={{ once: true }}
      transition={{ duration: 0.6 }}
      variants={slideInLeft}
    >
      {children}
    </motion.div>
  );
};
export default SlideInLeft