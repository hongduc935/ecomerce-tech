import React from 'react'
import styles from './styles/group-contact.module.scss'
import Link from 'next/link'


const GroupContact = () => {
  return (
    <div className={styles.groupContact}>
      <ul className={styles.listContact}>
          <li className={styles.icons}>
            <Link href={'#'}><div className={`${styles.icon} ${styles.zaloIcons}`}></div></Link>
          </li>
          <li className={styles.icons}>
            <Link href={'#'}><div className={`${styles.icon} ${styles.messengerIcons}`}></div></Link>
          </li>
          <li className={styles.icons}>
            <Link href={'#'}><div className={`${styles.icon} ${styles.telegramIcons}`}></div></Link>
          </li>
      </ul>
    </div>
  )
}

export default GroupContact