import React from 'react'
import styles from './styles/newsletter.module.scss'


const Newsletter = () => {
  return (
    <section className={`${styles.newsletter} container`}>
      <h2 className={styles.titleNewsletter}>Đăng kí để nhận thêm thông tin từ IT QUY NHƠN </h2>
      <div className={`${styles.formNewSletter} `}>
          <form className={`${styles.forms} item-center`}>
                <input className={styles.fieldEmail} placeholder='Nhập gmail của bạn ' type='text'/>
                <button className={styles.submit}>Đăng kí</button>
          </form>
      </div>
    </section>
  )
}

export default Newsletter