import React from 'react'
import styles from './styles/header-page.module.scss'
import Link from 'next/link'
import Image from 'next/image'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {  faBars, faCircleUser, faEnvelope, faPhone } from '@fortawesome/free-solid-svg-icons'
import { useAuth } from '@/contexts/AuthProvider'
import MenuBar from './components/MenuBar/MenuBar'

const HeaderPage = () => {

  const { user, isAuthenticated } = useAuth();

  console.log('user:' + user)

  console.log('isAuthenticated:' + isAuthenticated)

  // const isAuthenticated = true 

  return (
    <>
    <div className={`${styles.headerPage} item-center`}>
      <div className={`${styles.listItem} item-btw w-90`}>
        <div className={styles.logo}>
          <Link href={"/"} aria-label="Download the annual financial report">
            <div className={styles.img}>
              <Image
                src={'/logo.webp'}
                alt='IT QUY NHƠN '
                width={0}
                height={0}
                loading="lazy"
                sizes="100vw"
                style={{ width: '100%', height: 'auto' }} // optional
              />
              {/* IT QUY NHƠN */}
            </div>

          </Link>
        </div>
        <div className={styles.category}>
          <ul className={`${styles.menu} item-btw`}>
            <li className={`${styles.item} ${styles.hoverUnderlineAnimation}`}><Link href={"/mac"}>Mac </Link></li>
            <li className={`${styles.item} ${styles.hoverUnderlineAnimation}`}><Link href={"/laptop"}>Window</Link></li>
            <li className={`${styles.item} ${styles.hoverUnderlineAnimation}`}><Link href={"/airpods"}>AirPods</Link></li>
            <li className={`${styles.item} ${styles.hoverUnderlineAnimation}`}><Link href={"/iphone"}>IPhone</Link></li>
            <li className={`${styles.item} ${styles.hoverUnderlineAnimation}`}><Link href={"/airtag"}>AirTag</Link></li>
            <li className={`${styles.item} ${styles.hoverUnderlineAnimation}`}><Link href={"/watch"}>Apple Watch</Link></li>
            <li className={`${styles.item} ${styles.hoverUnderlineAnimation}`}><Link href={"/mac-studio"}>Mac Studio</Link></li>
            <li className={`${styles.item} ${styles.hoverUnderlineAnimation}`}><Link href={"/accessory"}>Phụ Kiện</Link></li>
            <li className={`${styles.item} ${styles.hoverUnderlineAnimation}`}><Link href={"/services"}>Dịch Vụ</Link></li>
            <li className={`${styles.item} ${styles.hoverUnderlineAnimation}`}><Link href={"/blogs"}>Blog IT</Link></li>
          </ul>
          
        </div>
        <div className={styles.authen}>
          <div className={`${styles.layoutAuthen} item-center`}>

            {
              isAuthenticated ?
                <div className={styles.infoUser}>
                  <span className={styles.icon}>
                    <FontAwesomeIcon icon={faCircleUser} />
                  </span>&nbsp;
                  <span className={styles.username}>Nguyễn Hồng Đức {isAuthenticated}</span>
                </div>
                :
                <div className={styles.auth}>
                  <Link href={'/login'}><button className={styles.btnLogin}>Đăng nhập</button></Link>
                </div>
            }

            <div className={styles.line}></div>
            <div className={styles.hotline}>
              <p className={styles.phone}><FontAwesomeIcon icon={faPhone} />&nbsp; 0354298203</p>
              <p className={styles.support}><FontAwesomeIcon icon={faEnvelope} />&nbsp; info@gmaiil.com</p>
            </div>
          </div>
        </div>
        <div className={styles.barIcon}>
          <FontAwesomeIcon icon={faBars} />
        </div>
      </div>
    </div>
    <div className={` ${styles.menu} item-center`}>
      <div className={`${styles.layout} w-70`}>
          <MenuBar/>
      </div>
        
    </div>
    </>
  )
}

export default HeaderPage
