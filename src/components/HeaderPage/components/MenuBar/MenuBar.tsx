import React from 'react'
import styles from './styles/menubar.module.scss'
import { faBriefcase, faCalendar, faHandshake, faKeyboard, faLaptop, faNewspaper, faQuestion, faWrench } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const MenuBar = () => {
  return (
    <nav className={styles.menubar}>
          <ul className={styles.menu}>
            <li className={styles.itemLink}><FontAwesomeIcon icon={faLaptop} /> Laptop</li>
            <li className={styles.itemLink}><FontAwesomeIcon icon={faHandshake} /> Đối tác</li>
            <li className={styles.itemLink}><FontAwesomeIcon icon={faCalendar} /> Sự kiện</li>
            <li className={styles.itemLink}><FontAwesomeIcon icon={faNewspaper} /> Tin tức</li>
            <li className={styles.itemLink}><FontAwesomeIcon icon={faQuestion} /> Tư vấn</li>
            <li className={styles.itemLink}><FontAwesomeIcon icon={faBriefcase} /> Nhân sự</li>
            <li className={styles.itemLink}><FontAwesomeIcon icon={faWrench} /> Sửa chữa</li>
            <li className={styles.itemLink}><FontAwesomeIcon icon={faKeyboard} /> Phụ Kiện</li>
          </ul>  
    </nav>
  )
}

export default MenuBar