import React from 'react'
import styles from './styles/scroll-to-top.module.scss'



const ScrollToTop = () => {
  return (
    <div className={styles.scrollToTop}>
      ScrollToTop
    </div>
  )
}

export default ScrollToTop