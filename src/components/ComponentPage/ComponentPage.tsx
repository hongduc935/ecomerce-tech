import React from 'react'
import HeaderPage from '../HeaderPage/HeaderPage';
import FooterPage from '../FooterPage/FooterPage';
import GroupContact from '../GroupContact/GroupContact';

type  Props = {
  isHiddenHeader:boolean;
  isHiddenFooter:boolean;
  children?: React.ReactNode; 
}

const ComponentPage : React.FC<Props>= ({ isHiddenHeader, isHiddenFooter,children }) => {
  return (
    <div>
      {isHiddenHeader && <HeaderPage/> }
      {children }
      {isHiddenFooter && <FooterPage/> }
      <GroupContact/>
    </div>
  )
}

export default ComponentPage
