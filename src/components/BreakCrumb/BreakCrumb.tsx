import React from 'react'
import styles from './styles/break-crumb.module.scss'

const BreakCrumb = () => {
  return (
    <div className={`${styles.breakCrumb} item-btw`}>
      <div className={styles.break}>
          <nav className={styles.breadcrumbs}>
            <a href="#home" className={styles.breadcrumbs__item}>Trang Chủ</a>
            <a href="#shop" className={styles.breadcrumbs__item}>Airpods</a> 
            <a href="#cart" className={styles.breadcrumbs__item}>Airpods Pro Gen 2</a>
            <a href="#checkout" className={`${styles.breadcrumbs__item} ${styles.is_active}`}>0</a> 
          </nav>
      </div>
      <div className={styles.itemRight}>

      </div>
      
    </div>
  )
}

export default BreakCrumb
