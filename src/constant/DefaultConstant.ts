const DOMAIN_SERVER  = "http://localhost:4000"
const DOMAIN_PORT  = "http://localhost:4000"
const BASE_URL = 'http://localhost:3000';


const DefaultConstant = {
  DOMAIN_SERVER,
  DOMAIN_PORT,
  BASE_URL
}

export default DefaultConstant

