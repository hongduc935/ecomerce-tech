const serializing = (data:any)=>{
  const sanitizedData = JSON.parse(JSON.stringify(data, (key:string, value:any) =>key && value === undefined ? null : value));
  return sanitizedData
}

const format = (response:any)=>{
  
  const {statusCode, data,msg } = response

  if(Number(statusCode) === 2000  ){
    //Toastify 
  }
  else if (Number(statusCode) === 1000  ){
    //Toastify 
  }
  else{
     //Toastify 
  }
  return {
    statusCode, 
    data,
    msg
  }

}

const HandleResponse = {
  format,
  serializing
}

export default HandleResponse