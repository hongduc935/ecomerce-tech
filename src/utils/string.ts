
const StringUtils = {

  convertStringToSnakeCase:(string:string)=>{
    return string.toLowerCase().replaceAll(' ','_')
  }

}

export default StringUtils