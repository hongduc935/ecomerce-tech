import Repository from "@/configs/axiosClient";


import DefaultConstant from '../../constant/DefaultConstant'

const {DOMAIN_SERVER} = DefaultConstant

const AuthService = {

    login: async (credentials:any) => {
      return await Repository({url:`${DOMAIN_SERVER}/api/v1/auth/login`,method:"POST",payload :{body:credentials,params:{}}})
    },
    register: async (credentials:any) => {
      return await Repository({url:`${DOMAIN_SERVER}/api/v1/auth/register`,method:"POST",payload :{body:credentials,params:{}}})
    },
  
    getAccount: async () => {
      return await Repository({url:`${DOMAIN_SERVER}/api/v1/user/profile`,method:"GET",payload :{body:{},params:{}}})
    }


}

export default AuthService