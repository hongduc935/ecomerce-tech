import Repository from "@/configs/axiosClient"


const BlogService = {

  getBlogBySlug : async (slug:string)=>{
    return await Repository({url:`https://hqltools.com/api/v1/blog/${slug}`,method:"POST",payload :{body:{},params:{}}})
  },

}

export default BlogService