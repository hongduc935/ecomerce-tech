import Repository from "@/configs/axiosClient"


const ProductService = {

  

  getAllProductByCategory : async (data:any)=>{
    return await Repository({
      url:`/product/get-all-by-category`,
      method:"POST",
      payload :{body:data,params:{}}
    })
  },

  getMacByFilter : async (pager:any,data:any)=>{
    return await Repository({
      url:`/product/mac?page=${pager.page}&limit=${pager.limit}`,
      method:"POST",
      payload :{
        body:data,
        params:{}
      }
    })
  },

  getProductById : async (data:any)=>{
    return await Repository({
      url:`/api/v1/product/get-by-id`,
      method:"POST",
      payload :{
        body:data,params:{}
      }
    })
  },

  updateProducts : async (id:string)=>{
      return await Repository({
        url:"/product",
        method:"POST",
        payload :{
          body:{id},params:{}
        }
      })
  },

  createProducts : async (product:any)=>{
    return await Repository({url:"/product",method:"POST",payload :{body:{product},params:{}}})
  }

}

export default ProductService