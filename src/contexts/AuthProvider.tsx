// contexts/AuthContext.tsx
import AuthService from '@/apis/auth/AuthService';
import LocalStorage from '@/configs/localStorage';
import NotifyUtils from '@/utils/notice/toastify';
import { useRouter } from 'next/router';
import { createContext, useContext, useState, ReactNode, useEffect } from 'react';

// Định nghĩa interface cho context
interface AuthContextType {
  user: any;
  login: (email: string, password: string) => void;
  logout: () => void;
  isAuthenticated: boolean;
  loading: boolean;
  error: string | null;
}

// Tạo context
const AuthContext = createContext<AuthContextType | undefined>(undefined);

// Provider component
export function AuthProvider({ children }: { children: ReactNode }) { // Dùng để quản lí trạng thái đăng nhập

  const [user, setUser] = useState<any>(null);
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState<string | null>(null);
  const router = useRouter();
  const login = async (email: string, password: string) => {
    try {
      const credentials = { email, password }
      const response:any = await AuthService.login(credentials)

      const data = response.data
      const msg = response.msg
      const statusCode = response.statusCode

      if(statusCode && Number(statusCode) === 2000){
        NotifyUtils.error(msg)
      }
      LocalStorage.setToLocalStorage('auth_token',data.access_token)
      NotifyUtils.success(msg)
      setUser(data.user);
      setIsAuthenticated(true);
      setError(null);
      router.push('/');
    
    } catch (error: any) {
      setError(error.message);
    }
  };

  const logout = () => {
    setUser(null);
    setIsAuthenticated(false);
    // Optionally, you can also clear the token cookie on the server-side
  };

  useEffect(() => {

    const token = LocalStorage.getFromLocalStorage('auth_token');

    const initAuth = async () => {
      try {

        if(token){
          const accountData = await AuthService.getAccount();
          setUser(accountData);

          console.log("USER INIT AUTH: "+JSON.stringify(accountData))
          setIsAuthenticated(true);
        }

      } catch (err) {
        console.error(err);
      } finally {
        setLoading(false);
      }
    };

    initAuth();
  }, []);

  // useEffect(() => {
  //   // if (!loading && isAuthenticated) {

  //   //   console.log(isAuthenticated)
  //   //   router.push('/');
  //   // }
  // }, [loading, isAuthenticated, router]);
    // if (!loading && isAuthenticated) {

    //   console.log(isAuthenticated)
    //   router.push('/');
    //   return;
    // }
  return (
    <AuthContext.Provider value={{ user, isAuthenticated, login, logout, loading, error }}>
      {children}
    </AuthContext.Provider>
  );
}

// Custom hook để sử dụng context
export function useAuth() {
  const context = useContext(AuthContext);
  if (context === undefined) {
    throw new Error('useAuth must be used within an AuthProvider');
  }
  return context;
}
