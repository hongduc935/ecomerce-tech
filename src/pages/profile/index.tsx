import ComponentPage from '@/components/ComponentPage/ComponentPage'
import { Meta } from '@/layout/Meta'
import { MACBOOK } from '@/utils/AppConfig'
import React, { useState } from 'react'
import BankAccount from './components/BankAccount/BankAccount'
import Address from './components/Address/Address'
import Cart from './components/Cart/Cart'
import MyProfile from './components/MyProfile/MyProfile'
import styles from './styles/profile.module.scss'

const Profile = () => {
  const [activeTab, setActiveTab] = useState(0);

  const tabs = ['Tài khoản của tôi ', 'Tài khoản ngân hàng ','Địa chỉ','Giỏ hàng'];
  const userProfile = {
    name: 'John Doe',
    email: 'johndoe@example.com',
    phone: '123-456-7890',
    address: '123 Main St, Anytown, USA',
    imageUrl: 'https://via.placeholder.com/150'
  };

  const userAddress = {
    name: 'John Doe',
    street: '123 Main St',
    city: 'Anytown',
    state: 'CA',
    zip: '12345',
    country: 'USA'
  };

  const userBankAccount = {
    accountHolderName: 'John Doe',
    bankName: 'Bank of America',
    accountNumber: '1234567890',
    routingNumber: '987654321'
  };

  const cartItems = [
    { name: 'Product 1', quantity: 1, price: 29.99 },
    { name: 'Product 2', quantity: 2, price: 19.99 },
    { name: 'Product 3', quantity: 1, price: 49.99 }
  ];
  const content = [
    <MyProfile  
          name={userProfile.name}
          email={userProfile.email}
          phone={userProfile.phone}
          address={userProfile.address}
          imageUrl={userProfile.imageUrl}/>,
    <BankAccount {...userBankAccount}/>,
    <Address {...userAddress} />,
    <Cart items={cartItems}/>
  ];

  
  return (
    <ComponentPage isHiddenFooter={true} isHiddenHeader={true}>
    <Meta title={MACBOOK.title} description={MACBOOK.description} />
    <div className={styles.profile}>
          <div className={styles.category}>
            <div className={styles.tabs}>
              {tabs.map((tab, index) => (
                <div
                  key={index}
                  className={`${styles.tab} ${activeTab === index ? styles.active : ''}`}
                  onClick={() => setActiveTab(index)}
                >
                  {tab}
                </div>
              ))}
            </div>
          </div>
          
          <div className={styles.content}>
            {content[activeTab]}
          </div>
    </div>
      
    </ComponentPage>
   
  )
}

export default Profile