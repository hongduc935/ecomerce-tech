import React from 'react'
import styles from './styles/bank-account.module.scss'


const BankAccount = ({ accountHolderName, bankName, accountNumber, routingNumber }:any) => {
  return (
    <div className={styles.bankAccountContainer}>
      <div className={styles.head}>
        <h2>Tài khoản ngân hàng của bạn</h2>
        <p>Quản lý thông tin hồ sơ để bảo mật tài khoản</p>
      </div>
      <div className={styles.bankAccountDetails}>
        <h3>{accountHolderName}</h3>
        <p>Bank: {bankName}</p>
        <p>Account Number: {accountNumber}</p>
        <p>Routing Number: {routingNumber}</p>
      </div>
  </div>
  )
}

export default BankAccount