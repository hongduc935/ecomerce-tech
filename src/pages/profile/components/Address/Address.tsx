import React from 'react'
import styles from './styles/address.module.scss'


const Address = ({ name, street, city, state, zip, country }:any) => {
  return (
    <div className={styles.addressContainer}>
    <div className={styles.addressDetails}>
      <h3>{name}</h3>
      <p>{street}</p>
      <p>{city}, {state} {zip}</p>
      <p>{country}</p>
    </div>
  </div>
  )
}

export default Address