import React from 'react'
import styles from './styles/my-profile.module.scss'


const MyProfile = ({ name, email, phone, address, imageUrl }:any) => {


  console.log(name)
  console.log(email)
  console.log(phone)
  console.log(address)
  console.log(imageUrl)
  return (
    <div className={styles.myProfile}>
      <div className={styles.head}>
        <h2>Hồ sơ của tôi</h2>
        <p>Quản lý thông tin hồ sơ để bảo mật tài khoản</p>
      </div>
      <div className={styles.profileContainer}>
        
        <div className={styles.profileDetails}>
          <form className={styles.form}>
              <div className={styles.rowForm}>
                  <div className={styles.label}>
                      Tên đăng nhập
                  </div>
                  <div className={styles.fields}>
                      <input className={styles.input} placeholder='Tên đăng nhập'/>
                  </div>
              </div>
              <div className={styles.rowForm}>
                  <div className={styles.label}>
                      Email
                  </div>
                  <div className={styles.fields}>
                      <input className={styles.input} placeholder='Your Email'/>
                  </div>
              </div>
              <div className={styles.rowForm}>
                  <div className={styles.label}>
                      Địa chỉ 
                  </div>
                  <div className={styles.fields}>
                      <input className={styles.input} placeholder='Địa chỉ của bạn'/>
                  </div>
              </div>
              <div className={styles.rowForm}>
                  <div className={styles.label}>
                      Số điện thoại
                  </div>
                  <div className={styles.fields}>
                      <input className={styles.input} placeholder='Số điện thoại của bạn'/>
                  </div>
              </div>
              <div className={`${styles.rowForm} ${styles.itemButton}`}>
               
                  <button className={styles.saveInfo}>Lưu thông tin</button>
               
                
              </div>

          </form>
          
        </div>
        <div className={styles.image}>
          <img src={imageUrl} alt={`${name} profile`} className={styles.profileImage} />
        </div>
      </div>
    </div>
  )
}

export default MyProfile