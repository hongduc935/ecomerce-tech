import React from 'react'
import styles from './styles/500.module.scss'


const ErrorPage = () => {
  return (
    <div className={styles.error}>
      This is Error Page
    </div>
  )
}

export default ErrorPage
