import React from 'react'
import ComponentPage from '@/components/ComponentPage/ComponentPage'
import  {MACBOOK}  from '@/utils/AppConfig'
import { Meta } from '@/layout/Meta'
import ListCategory from './components/ListCategory/ListCategory'
import ListProduct from './components/ListProduct/ListProduct'
import AboutApplication from './components/AboutApplication/AboutApplication'
import Accessory from './components/Accessory/Accessory'
import BlogSuggest from './components/BlogSuggest/BlogSuggest'
import Repository from '@/configs/axiosClient'
import {  NextPage } from 'next'
// import HandleResponse from '@/utils/handle-response'
import BannerMac from './components/BannerMac/BannerMac'
import { GetServerSideProps } from 'next'
import DefaultConstant from '@/constant/DefaultConstant'
import ProcessTest from './components/ProcessTest/ProcessTest'
// import SloganMac from './components/SloganMac/SloganMac'
import Newsletter from '@/components/Newsletter/Newsletter'
import FeedBack from '@/components/Feedback/FeedBack'
import StringUtils from '@/utils/string'
// import ProductService from '@/apis/product/ProductService'


interface IProps {
  macs:any;
  blogs?:any
  attributes:any
}
const Mac:NextPage<IProps> = ({macs,blogs,attributes}) => {

  // console.log("blogs"+blogs)
  // console.log("iphones"+JSON.stringify(macs))





  return (
    <ComponentPage isHiddenFooter={true} isHiddenHeader={true}>
      <Meta title={MACBOOK.title} description={MACBOOK.description} />
      <ListCategory/>
      {/* <SloganMac/> */}
      <BannerMac/>
      <ListProduct products={macs} total_pages={5} total_items={21} page={1} attributes={attributes}/>
      <Accessory/>
      <ProcessTest/>
      <BlogSuggest blogs={blogs}/>
      <Newsletter/>
      <FeedBack/>
      <AboutApplication/>
    </ComponentPage>
  )
}

export const getServerSideProps: GetServerSideProps<IProps> = async (context) => {

  const { query } = context
 
  const PAYLOAD_BLOG:any={
    body:{
      category_name:"Macs"
    },
    params:{}
  }

  const blogsApi  =  Repository({
    url:`${DefaultConstant.DOMAIN_SERVER}/api/v1/blog/mac`,
    method:'POST',
    payload:{
      body:{category_id:1},
      params:{}
    }
  })

  const attributesApi  =  Repository({
    url:`${DefaultConstant.DOMAIN_SERVER}/api/v1/attribute/category`,
    method:'POST',
    payload:{
      body:{category_id:1},
      params:{}
    }
  })

  const [blogResponse,atrributesResponse]:any = await Promise.allSettled([blogsApi,attributesApi])

  const dataAttr = atrributesResponse?.value?.data
  // Lấy các key trong value 
  let keys:any = {}
  if(dataAttr && dataAttr.length > 0 ){
    for(const attr of dataAttr ){
      const key =StringUtils.convertStringToSnakeCase(attr.attribute_name)
      keys[key] = key
    }
  }

  const newBody:any = {
    // category:'Mac'
  }
  if(query){
    for(const key in keys){
      newBody[key] = query[key] 
    }
  }
 
  const macsApi  = Repository({
    url:`${DefaultConstant.DOMAIN_SERVER}/api/v1/product/test?page=${newBody.page}&limit=${newBody.limit}`,
    method:"POST",
    payload :{
      body:newBody,
      params:{}
    }
  })

  const [macResponse]:any = await Promise.allSettled([macsApi])
  
  console.log("macResponse:"+JSON.stringify(macResponse))
  return {
    props: {
      macs:macResponse?.value?.data,
      blogs:[],
      attributes:dataAttr
    }
  };
};
export default Mac
