
import React from 'react'
import styles from './styles/mac-detail.module.scss'
import ComponentPage from '@/components/ComponentPage/ComponentPage'
import { Meta } from '@/layout/Meta'
import { MACBOOK } from '@/utils/AppConfig'
import { GetServerSideProps, NextPage } from 'next'
// import Repository from '@/configs/axiosClient'
import ProductService from '@/apis/product/ProductService'
import BreakCrumb from '@/components/BreakCrumb/BreakCrumb'
import Gallery,{ IImage } from './components/Gallery/Gallery'
import InfoBrand from './components/InfoBrand/InfoBrand'
import InfoProduct from './components/InfoProduct/InfoProduct'
import SimilarProduct from './components/SimilarProduct/SimilarProduct'
// import Gallery from './components/Gallery/Gallery'
// import dynamic from 'next/dynamic'
// const Gallery = dynamic(() => import('./components/Gallery/Gallery'), {
//   ssr: false,
// });

interface IProps {
    product:any
    seo:any
}

const MacDetail :NextPage<IProps>= ({product,seo}) => {

  console.log("PRODUCT: "+product)
  console.log("seo: "+seo)
  const imageList:IImage[]  = [
    "https://media.cnn.com/api/v1/images/stellar/prod/220921163441-airpods-pro-2-review-1.jpg?c=16x9&q=w_800,c_fill",
    "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/AirPods_Pro_%282nd_generation%29.jpg/1200px-AirPods_Pro_%282nd_generation%29.jpg",
    "https://media.cnn.com/api/v1/images/stellar/prod/220921163441-airpods-pro-2-review-1.jpg?c=16x9&q=w_800,c_fill",
    "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/AirPods_Pro_%282nd_generation%29.jpg/1200px-AirPods_Pro_%282nd_generation%29.jpg",
    "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/AirPods_Pro_%282nd_generation%29.jpg/1200px-AirPods_Pro_%282nd_generation%29.jpg",
    "https://media.cnn.com/api/v1/images/stellar/prod/220921163441-airpods-pro-2-review-1.jpg?c=16x9&q=w_800,c_fill",
    "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/AirPods_Pro_%282nd_generation%29.jpg/1200px-AirPods_Pro_%282nd_generation%29.jpg"

  ]
 
  return (
    <ComponentPage isHiddenFooter={true} isHiddenHeader={true}>
    <Meta title={MACBOOK.title} description={MACBOOK.description} />
      

      <div className={`${styles.macDetail} container`}>
        <BreakCrumb/>
        <h1 className={styles.titlePageDetail}>IT QUY NHƠN - Airpods Pro Gen 2 | Cũ 99 % </h1>
        <div className={styles.layoutContent}>
            <div className={styles.listImage }>
                 <Gallery listImage={imageList}/> 
            </div>
            <div className={styles.infoBrand}>
               <InfoBrand/>
            </div>
        </div>
        <div className={styles.infoProduct }>
            <InfoProduct/>
        </div>
        <div className={styles.productList}>
            <SimilarProduct/>
        </div>
      
        
      </div>
    </ComponentPage>
  )
}

export const getServerSideProps: GetServerSideProps <IProps>= async (context)=>{

  const {  params } = context

  const { id } = params as { id: string };

  console.log("ID:"+id)

  const payload = {body:{ id},params:{}}

  const productDetail =await ProductService.getProductById(payload.body)

  console.log("productDetail:"+JSON.stringify(productDetail))

  console.log("ID:"+id)

  const product_fake = {
    id:1,
    product_name:"",
    product_description:"",
    product_image:[],
    quantity:2,
    product_condition:"",
    more_info:""
  }


  
  return {
    props:{
      product:product_fake,
      seo:{}
    }
  }
}

export default MacDetail
