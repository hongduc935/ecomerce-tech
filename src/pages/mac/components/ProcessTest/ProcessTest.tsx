import React from 'react'
import styles from './styles/process-test.module.scss'
import {  faSquareCheck } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


const ProcessTest = () => {
  return (
    <section className={`${styles.processTest} container`}>
      <h2 className={styles.title}>Hướng dẫn Quy Trình Kiểm Tra Laptop tại IT Quy Nhơn</h2>
      <div className={styles.step}>
        <h3 className={styles.nameStep}> <div className={styles.divider} />Kiểm tra ngoại hình</h3>
        <p className={styles.desStep}><em>Kiểm tra bề mặt ngoại hình: Đây là quá trình đánh giá tổng thể vỏ máy, bàn phím, bản lề, và các cổng kết nối để phát hiện các vết trầy xước, hư hỏng vật lý hoặc lỗi bên ngoài, đảm bảo laptop không có dấu hiệu móp méo, nứt vỡ và các phím cũng như touchpad hoạt động tốt.</em></p>
        <ul className={styles.list}>
          <li className={styles.item}>
            <div className={styles.icon}>
              <FontAwesomeIcon icon={faSquareCheck} />
            </div>
            <div className={styles.text}>
              <strong>Vỏ ngoài</strong>: Kiểm tra kỹ lưỡng vỏ máy, các góc cạnh để phát hiện trầy xước, móp méo hoặc các hư hỏng khác.
            </div>
          </li>
          <li className={styles.item}><div className={styles.icon}>
            <FontAwesomeIcon icon={faSquareCheck} />
          </div>
            <div className={styles.text}> <strong>Bản lề</strong>: Kiểm tra bản lề màn hình có còn chắc chắn và không bị gãy hoặc lỏng lẻo.</div> </li>
          <li className={styles.item}><div className={styles.icon}>
            <FontAwesomeIcon icon={faSquareCheck} />
          </div>
            <div className={styles.text}> <strong>Cổng kết nối</strong>: Đảm bảo các cổng USB, HDMI, tai nghe và khe cắm thẻ nhớ còn nguyên vẹn, không bị mòn, gãy, hoặc gặp trục trặc khi cắm thiết bị vào. </div></li>

          <li className={styles.item}><div className={styles.icon}>
            <FontAwesomeIcon icon={faSquareCheck} />
          </div>
            <div className={styles.text}> <strong>Bàn phím & Touchpad</strong>: Kiểm tra từng phím và touchpad để đảm bảo tất cả hoạt động tốt, không có phím nào bị liệt hoặc kẹt.</div></li>
        </ul>
      </div>

      <div className={styles.step}>
        <h3 className={styles.nameStep}><div className={styles.divider} /> Kiểm tra Pin Laptop </h3>
        <p className={styles.desStep}><em><strong>Kiểm tra pin</strong>: Đây là quá trình đánh giá dung lượng pin, thời gian sạc và sử dụng thực tế, so sánh với thông số của nhà sản xuất. Cần kiểm tra độ chai pin, phát hiện hiện tượng pin phồng hoặc quá nhiệt, đảm bảo pin còn duy trì hiệu năng ổn định và an toàn khi sử dụng. </em></p>
        <ul className={styles.list}>
          <li className={styles.item}><div className={styles.icon}>
            <FontAwesomeIcon icon={faSquareCheck} />
          </div>
            <div className={styles.text}><strong> Kiểm tra dung lượng pin hiện tại </strong>: Sử dụng phần mềm quản lý pin hoặc công cụ tích hợp của hệ điều hành để xem dung lượng pin tối đa hiện tại so với dung lượng ban đầu (Full Charge Capacity vs. Design Capacity)</div></li>
          <li className={styles.item}><div className={styles.icon}>
            <FontAwesomeIcon icon={faSquareCheck} />
          </div>
            <div className={styles.text}><strong> Kiểm tra thời gian sạc</strong>: Quan sát thời gian sạc từ 0% đến 100% và xem pin có đạt được thời gian sạc tối ưu hay không và Kiểm tra pin có tự ngắt sạc khi đạt 100% hay không để tránh tình trạng sạc quá mức. </div></li>
          <li className={styles.item}><div className={styles.icon}>
            <FontAwesomeIcon icon={faSquareCheck} />
          </div>
            <div className={styles.text}> <strong>Kiểm tra thời gian sử dụng</strong>: Sử dụng laptop cho các tác vụ thông thường (lướt web, xem video, làm việc văn phòng) để kiểm tra thời gian sử dụng pin so với thông số của nhà sản xuất.</div> </li>
          <li className={styles.item}><div className={styles.icon}>
            <FontAwesomeIcon icon={faSquareCheck} />
          </div>
            <div className={styles.text}> <strong>Kiểm tra nhiệt độ pin</strong>: Quan sát nhiệt độ của pin trong quá trình sử dụng và sạc bằng phần mềm giám sát nhiệt độ (như HWMonitor), đảm bảo pin không bị quá nhiệt trong khi hoạt động hoặc sạc. </div></li>
          <li className={styles.item}><div className={styles.icon}>
            <FontAwesomeIcon icon={faSquareCheck} />
          </div>
            <div className={styles.text}> <strong>Kiểm tra hiện tượng phồng pin</strong>: Kiểm tra vật lý để xem pin có dấu hiệu phồng rộp, cong vênh không. Pin phồng có thể ảnh hưởng đến vỏ máy hoặc gây nguy cơ cháy nổ.</div> </li>
          <li className={styles.item}><div className={styles.icon}>
            <FontAwesomeIcon icon={faSquareCheck} />
          </div>
            <div className={styles.text}> <strong>Kiểm tra tính năng bảo vệ pin</strong>: Kiểm tra xem laptop có tính năng ngắt sạc khi không sử dụng hoặc giảm sạc khi máy đạt 100% để kéo dài tuổi thọ pin và Kiểm tra chế độ tiết kiệm pin và xem có hoạt động đúng khi pin yếu không.</div></li>
          <li className={styles.item}><div className={styles.icon}>
            <FontAwesomeIcon icon={faSquareCheck} />
          </div>
            <div className={styles.text}><strong> Kiểm tra hệ thống báo lỗi pin</strong>: Kiểm tra hệ thống báo lỗi pin của hệ điều hành, xem có cảnh báo hoặc thông báo bất thường về tình trạng pin không. </div></li>
        </ul>
      </div>

      <div className={styles.step}>
        <h3 className={styles.nameStep}><div className={styles.divider} /> Kiểm tra cấu hình Laptop</h3>
        <p className={styles.desStep}><em>Kiểm tra cấu hình: Đây là quá trình đánh giá phần cứng của laptop, bao gồm CPU, RAM, ổ cứng (HDD/SSD), và card đồ họa. Mục tiêu là đảm bảo các thành phần này hoạt động đúng với thông số kỹ thuật của nhà sản xuất, đo hiệu năng xử lý, tốc độ đọc/ghi của ổ cứng, và khả năng xử lý đồ họa. Các công cụ benchmark và kiểm tra hệ thống thường được sử dụng để xác minh tính chính xác và ổn định của cấu hình máy.</em> </p>
        <ul className={styles.list}>
          <li className={styles.item}>
            <div className={styles.icon}>
              <FontAwesomeIcon icon={faSquareCheck} />
            </div>
            <div className={styles.text}>
              <strong>Kiểm tra thông số phần cứng</strong>: Sử dụng lệnh hệ điều hành (như DxDiag trên Windows) hoặc phần mềm như CPU-Z để kiểm tra chi tiết thông số của CPU, RAM, GPU, và ổ cứng và đối chiếu với các thông số này với cấu hình nhà sản xuất công bố (số nhân, tốc độ xung nhịp của CPU, dung lượng RAM, loại ổ cứng HDD/SSD, dung lượng thực tế).
            </div>
          </li>
          <li className={styles.item}>
            <div className={styles.icon}>
              <FontAwesomeIcon icon={faSquareCheck} />
            </div>
            <div className={styles.text}>
              <strong>Kiểm tra hiệu năng CPU</strong>: Chạy công cụ benchmark (như Cinebench hoặc Geekbench) để kiểm tra sức mạnh xử lý của CPU trong các tác vụ đơn nhân và đa nhân
            </div>
          </li>
          <li className={styles.item}>
            <div className={styles.icon}>
              <FontAwesomeIcon icon={faSquareCheck} />
            </div>
            <div className={styles.text}>
              <strong>Kiểm tra dung lượng và tốc độ RAM</strong>: Kiểm tra dung lượng RAM thực tế có đủ và tương thích với thông số nhà sản xuất hay không.
            </div>
          </li>
          <li className={styles.item}>
            <div className={styles.icon}>
              <FontAwesomeIcon icon={faSquareCheck} />
            </div>
            <div className={styles.text}>
              <strong>Kiểm tra ổ cứng (HDD/SSD)</strong>: Sử dụng CrystalDiskMark để kiểm tra tốc độ đọc/ghi của ổ cứng, đảm bảo tốc độ tương ứng với loại ổ (HDD hoặc SSD).
            </div>
          </li>
          <li className={styles.item}>
            <div className={styles.icon}>
              <FontAwesomeIcon icon={faSquareCheck} />
            </div>
            <div className={styles.text}>
              <strong>Kiểm tra hiệu năng card đồ họa (GPU)</strong>: Đối với card đồ họa tích hợp, kiểm tra hiệu năng trong các tác vụ nhẹ hơn như xem video chất lượng cao hoặc chơi game ở mức đồ họa cơ bản.
            </div>
          </li>
          <li className={styles.item}>
            <div className={styles.icon}>
              <FontAwesomeIcon icon={faSquareCheck} />
            </div>
            <div className={styles.text}>
              <strong> Kiểm tra nhiệt độ và hệ thống tản nhiệt</strong>: Sử dụng phần mềm như HWMonitor hoặc Core Temp để theo dõi nhiệt độ của CPU, GPU khi chạy các bài kiểm tra hiệu năng.
            </div>
          </li>
          <li className={styles.item}>
            <div className={styles.icon}>
              <FontAwesomeIcon icon={faSquareCheck} />
            </div>
            <div className={styles.text}>
              <strong> Kiểm tra hiệu năng tổng thể</strong>: Chạy một số công cụ benchmark tổng thể như PCMark hoặc PassMark để đánh giá hiệu năng toàn diện của máy (bao gồm cả CPU, GPU, RAM và ổ cứng) trong các tác vụ văn phòng, đồ họa và giải trí.
            </div>
          </li>
        </ul>
      </div>

    </section>
  )
}

export default ProcessTest