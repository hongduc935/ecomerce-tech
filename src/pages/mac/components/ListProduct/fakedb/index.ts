import { IOption } from "../ListProduct"

const MONITOR :IOption[]= [
  {
    value:"30.41 x 21.24 x 1.56 cm",
    name:"13 inch"
  },
  {
    value:"31.26 x 22.12 x 1.55 cm",
    name:"14 inch"
  },
  {
    value:"35.79 x 24.59 x 1.68 cm",
    name:"16 inch"
  },
]
const GEN :IOption[]= [
  {
    value:"MACBOOK PRO",
    name:"MACBOOK PRO"
  },
  {
    value:"MACBOOK AIR",
    name:"MACBOOK AIR"
  },
  {
    value:"THE NEW MACBOOK",
    name:"THE NEW MACBOOK"
  },
]
const RAMS :IOption[]= [
  {
    value:"8GB",
    name:"8GB"
  },
  {
    value:"16GB",
    name:"16GB"
  },
  {
    value:"24GB",
    name:"24GB"
  },
  {
    value:"32GB",
    name:"32GB"
  }
]
const SSD :IOption[]= [
  {
    value:"256GB SSD",
    name:"256GB"
  },
  {
    value:"512GB SSD",
    name:"512GB"
  },
  {
    value:"1TB SSD",
    name:"1TB"
  },
  {
    value:"2TB SSD",
    name:"2TB"
  }
]
const CHIP :IOption[]= [
  {
    value:"I5",
    name:"I5"
  },
  {
    value:"I7",
    name:"I7"
  },
  {
    value:"I9",
    name:"I9"
  },
  {
    value:"Apple M1",
    name:"Apple M1"
  },
  {
    value:"Apple M1 Max",
    name:"Apple M1 Max"
  },
  {
    value:"Apple M1 Pro",
    name:"Apple M1 Pro"
  },
]

export {
  MONITOR,
  CHIP,
  GEN,
  SSD,
  RAMS
}