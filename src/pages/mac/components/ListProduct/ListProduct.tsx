import React, { useEffect } from 'react'
import styles from './styles/list-product.module.scss'
import Image from 'next/image'
import Link from 'next/link'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleLeft, faAngleRight, faAnglesLeft, faAnglesRight, faArrowDown, faArrowUp, faMagnifyingGlass } from '@fortawesome/free-solid-svg-icons'
import { SubmitHandler, useForm } from 'react-hook-form'
import { useRouter } from 'next/router'
import StringUtils from '@/utils/string'
import { CHIP, GEN, MONITOR, RAMS, SSD } from './fakedb'

const ListProductDB:Number[] = [1,2,3,4,5,6,7,8]

export interface IOption  {
  value:any,
  name:string
}

interface IMacProps{
  products:any,
  total_pages:number,
  total_items:number,
  page:number,
  attributes:any
}

const ListProduct= ({ products,total_items,total_pages,page,attributes}:IMacProps) => {

  console.log("Products 5555: "+JSON.stringify(products))

  // Lấy các key trong value 
  let keys:any = {}
  if(attributes && attributes.length > 0 ){
    for(const attr of attributes ){
      const key =StringUtils.convertStringToSnakeCase(attr.attribute_name)
      keys[key] = key
    }
  }
  
  const { register, setValue,handleSubmit, formState: { errors } } = useForm<any>();

  const router = useRouter();

  // Xử lí phân trang 
  const handleNextPage = (page:number) =>{
    const { query } = router;

    try{
      const newQuery:any = {page, limit:10}
      for(const key in keys){
        if(query[key] &&  Number(query[key]) > 0 ){
          newQuery[key] = query[key]
        }
      }
      router.push({ pathname:'/mac',query:newQuery})
    }catch(error:any ){
        alert('Lỗi catch ở xử lí phân trang ')
    }
  }

  // Submit filter product
  const onSubmitRegisterFilterProduct: SubmitHandler<any> = async (data_submit:any) => {
    
    try {
      console.log("Data: "+JSON.stringify(data_submit))
      const newQuery:any = { page:1,limit:10}
      for(const key in keys){
          if( data_submit[key] > 0){
            newQuery[key] = data_submit[key]
          }
      }
      router.push({ pathname: '/mac',query: newQuery});
    } catch (err) {
      // console.error(err)
      alert('Lỗi catch ở xử lí lọc sản phẩm .')
    }
  }
  const { query } = router

  useEffect(() => {
    for(const key in keys){
      const value = Array.isArray(query[key]) ? null : query[key];
      if (value) setValue(key, value);
    }
  
  }, [query, setValue]);

  return (
    
    <div className={`${styles.listProduct} container`}>
      <h2 className={styles.titleSection}> Macbook tại IT QUY NHƠN </h2>
      <div className={styles.desSection}>
          Tìm kiếm sản phẩm theo thương hiệu 
      </div>
      <div className={`${styles.multiSearch}`}>
        
          <form className={`${styles.filterOption} w-100 item-btw` } onSubmit={handleSubmit(onSubmitRegisterFilterProduct)}>

          {attributes && attributes.length > 0 ?  attributes.map((attr:any) => (
                <div className={styles.itemSearch} key={attr.attribute_name}>
                    <select 
                      className={styles.monitor} 
                      id={attr.attribute_name}
                      {...register(`${StringUtils.convertStringToSnakeCase(attr.attribute_name)}`, { required: true })}
                    >
                        <option key={`item.attribute_id`} value={-1} selected>
                            Choose Option
                        </option>
                        {attr.attribute_values.map((item:any) => (
                            <option key={item.attribute_id} value={item.attribute_id}>
                                {item.value}
                            </option>
                        ))}
                    </select>
                </div>
            )) : 
            (<><div className={styles.itemSearch}>
                  <select className={styles.monitor} {...register("gen", { required: true })}>
                      {GEN.map((item:IOption,index:number)=>{
                        return <option key={`chip_${index}`}>{item.value}</option>
                      })}
                  </select>
              </div>
              <div className={styles.itemSearch}>
                  <select className={styles.monitor} {...register("chip", { required: true })}>
                      {CHIP.map((item:IOption,index:number)=>{
                        return <option key={`chip_${index}`}>{item.value}</option>
                      })}
                  </select>
              </div>
              <div className={styles.itemSearch}>
                  <select className={styles.monitor} {...register("dimensions", { required: true })}>
                      {MONITOR.map((item:IOption,index:number)=>{
                        return <option key={`monitor_${index}`} value={item.value}>{item.name}</option>
                      })}
                  </select>
              </div>
              <div className={styles.itemSearch}>
                  <select className={styles.monitor} {...register("ssd", { required: true })}>
                      {SSD.map((item:IOption,index:number)=>{
                        return <option key={`ssd_${index}`}>{item.value}</option>
                      })}
                  </select>
              </div>
              <div className={styles.itemSearch}>
                  <select className={styles.monitor}  {...register("ram", { required: true })}>
                      {RAMS.map((item:IOption,index:number)=>{
                        return <option key={`ram_${index}`}>{item.value}</option>
                      })}
                  </select>
              </div></>  )
          }
              <div className={styles.itemSearch}>
                  <button className={`${styles.btnSearch} `} type='submit'><FontAwesomeIcon icon={faMagnifyingGlass} /> &nbsp;Tìm kiếm</button>
              </div>

              <div className={styles.itemSearch}>
                  <button className={`${styles.btnSort} `}>
                      Tăng dần&nbsp; &nbsp;<FontAwesomeIcon icon={faArrowUp} />
                  </button>
              </div>

              <div className={styles.itemSearch}>
                    <button className={`${styles.btnSort} `}>
                        Giảm dần &nbsp;&nbsp; <FontAwesomeIcon icon={faArrowDown} />
                    </button>
              </div>
          </form>
      </div>
      <div className={styles.mainListProducts}>
         <div className={`${styles.grid} `}>
            { 
                products.flatMap((value: any, index: number) => (
                    <div className={styles.cardProduct} key={`product_${index}`}>
                        <div className={`${styles.logoBrand} item-center`}>
                            <Image
                                src={'https://s3.amazonaws.com/cdn.designcrowd.com/blog/100-Famous-Brand%20Logos-From-The-Most-Valuable-Companies-of-2020/apple-logo.png'}
                                alt='Brand From Apple'
                                width={50} // Cần chỉ định chiều rộng thực tế
                                height={50} // Cần chỉ định chiều cao thực tế
                                loading="lazy"
                                sizes="100vw"
                            />
                        </div>
                        <div className={`${styles.image} item-center`}>
                            <Image
                                src={'https://www.apple.com/v/mac/home/by/images/overview/select/product_tile_mbp_14_16__bkl8zusnkpw2_large.png'}
                                alt='Laptop'
                                width={300} // Cần chỉ định chiều rộng thực tế
                                height={200} // Cần chỉ định chiều cao thực tế
                                loading="lazy"
                                sizes="100vw"
                            />
                        </div>
                        <div className={styles.shortInfo}>
                            <p className={styles.title}>{`${value.product_name} 2024 Ram 16GB , SSD 512`}</p>
                            <p className={styles.price}>{`${value.product_price}đ`}</p>
                            <p className={styles.day}>Ngày đăng: {value.created_at}</p>
                            <div className={`${styles.groupBtn} item-btw`}>
                                <Link href={`/mac/${index}`} target='blank'><button className={`${styles.buy} ${styles.btn}`}>Mua</button></Link>
                            </div>
                        </div>
                    </div>
                )
            ) 
            }

         </div>
         {products.length === 0 &&
            (
              <div className={styles.noProducts}>
                  <img
                      src="https://cdni.iconscout.com/illustration/premium/thumb/sorry-item-not-found-illustration-download-in-svg-png-gif-file-formats--available-product-tokostore-pack-e-commerce-shopping-illustrations-2809510.png?f=webp"  // Thay đổi URL bằng đường dẫn hình ảnh của bạn
                      alt="No products found"
                  />
                  <p>Hiện tại không có sản phẩm nào phù hợp.</p>
              </div>
            )
         }
         <div className={`${styles.listBtnPagination} item-center`}>
              <div className={`${styles.groupBtn} `}>
                <button className={styles.btn}>
                  <FontAwesomeIcon icon={faAnglesLeft} />
                </button>
                <button className={styles.btn}>
                <FontAwesomeIcon icon={faAngleLeft} />
                </button>
                  {              
                      Array.from({ length: total_pages }, (_, i) => {
                        const pageNumber = i + 1; // i bắt đầu từ 0, nên cần cộng thêm 1
                        return (
                          <button
                            key={pageNumber}
                            className={page === pageNumber ? ` ${styles.btn} ${styles.active}` :`${styles.btn} ` }
                            onClick={() => handleNextPage(pageNumber)}
                          >
                            {pageNumber}
                          </button>
                        );
                      })
                  }
                  <button className={styles.btn}>
                    <FontAwesomeIcon icon={faAngleRight} />
                  </button>
                  <button className={styles.btn}>
                  <FontAwesomeIcon icon={faAnglesRight} />
                  </button>
              </div>
              
         </div>
      </div>
    </div>
  )
}

export default ListProduct