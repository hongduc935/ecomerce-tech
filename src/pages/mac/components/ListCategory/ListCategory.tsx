import React from 'react'
import styles from './styles/list-category.module.scss'


const ListCategory = () => {
  return (
    <div className={`${styles.listCategory} item-center w-100`}>
          <div className={`${styles.listItem} item-center w-70 `}>
                <ul className={`${styles.menu} item-btw`}>
                    <li className={`${styles.item} `}>
                        <a className={`${styles.link}`} href="/vn/macbook-air/" data-analytics-title="macbook air">
                          <figure className={`${styles.icon} ${styles.air}`} ></figure>
                          <p className={`${styles.label}`} role="text">MacBook Air</p>
                          <p className={`${styles.new}`} >Mới</p>
                        </a>
                    </li>
                    <li className={`${styles.item} `}>
                        <a className={`${styles.link}`} href="/vn/macbook-air/" data-analytics-title="macbook air">
                          <figure className={`${styles.icon} ${styles.pro}`} ></figure>
                          <p className={`${styles.label}`} role="text">MacBook Pro</p>
                          <p className={`${styles.new}`} >Mới</p>
                        </a>
                    </li>
                    <li className={`${styles.item} `}>
                        <a className={`${styles.link}`} href="/vn/macbook-air/" data-analytics-title="macbook air">
                          <figure className={`${styles.icon} ${styles.imac}`} ></figure>
                          <p className={`${styles.label}`} role="text">IMac</p>
                          <p className={`${styles.new}`} >Mới</p>
                        </a>
                    </li>
                    <li className={`${styles.item} `}>
                        <a className={`${styles.link}`} href="/vn/macbook-air/" data-analytics-title="macbook air">
                          <figure className={`${styles.icon} ${styles.mini}`} ></figure>
                          <p className={`${styles.label}`} role="text">Mac Mini</p>
                          <p className={`${styles.new}`} >Mới</p>
                        </a>
                    </li>
                    <li className={`${styles.item} `}>
                        <a className={`${styles.link}`} href="/vn/macbook-air/" data-analytics-title="macbook air">
                          <figure className={`${styles.icon} ${styles.monitor}`} ></figure>
                          <p className={`${styles.label}`} role="text">Màn hình</p>
                          <p className={`${styles.new}`} >Mới</p>
                        </a>
                    </li>
                    
                </ul>

          </div>
    </div>
  )
}

export default ListCategory
