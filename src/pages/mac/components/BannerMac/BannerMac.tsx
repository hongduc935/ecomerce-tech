import React, { useCallback, useRef } from 'react'
import { Swiper, SwiperSlide } from 'swiper/react'
import styles from './styles/banner-mac.module.scss'
import 'swiper/css'
import 'swiper/css/pagination'
import 'swiper/css/navigation'
import { Autoplay, Pagination, Navigation } from 'swiper/modules'
import Image from 'next/image'
import { faArrowLeftLong, faArrowRightLong } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
const BannerMac = () => {



  const sliderRef = useRef<any>(null);

  const handlePrev = useCallback(() => {
    if (!sliderRef.current) return;
    sliderRef.current.swiper.slidePrev();
  }, []);

  const handleNext = useCallback(() => {
    if (!sliderRef.current) return;
    sliderRef.current.swiper.slideNext();
  }, []);

  
  return (
    <div className={`${styles.bannerMac} container`}>
      <Swiper
        spaceBetween={30}
        centeredSlides={true}
        autoplay={{
          delay: 2500,
          disableOnInteraction: false,
        }}
        navigation={false}
        modules={[Autoplay, Pagination, Navigation]}
        className={styles.bannerMacSw}
      >
        <SwiperSlide>
          <div className={styles.sliderItem}>
              <Image
                src={`https://insieutoc.vn/wp-content/uploads/2021/02/mau-banner-quang-cao-khuyen-mai.jpg`}
                alt={`IT QUY NHƠN | `}
                width={0}
                height={0}
                sizes="100vw"
                style={{ width: '100%', height: 'auto' }} // optional
              />                 
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className={styles.sliderItem}>
              <Image
                src={`https://insieutoc.vn/wp-content/uploads/2021/02/mau-banner-quang-cao-khuyen-mai.jpg`}
                alt={`IT QUY NHƠN | `}
                width={0}
                height={0}
                sizes="100vw"
                style={{ width: '100%', height: 'auto' }} // optional
              />                 
          </div>
        </SwiperSlide>
      </Swiper>
      <div className={styles.groupCustomNav}>
          <div className={styles.preMac} onClick={handlePrev} ><FontAwesomeIcon icon={faArrowLeftLong} /></div>
          <div className={styles.nextMac} onClick={handleNext} ><FontAwesomeIcon icon={faArrowRightLong} /></div>
      </div>
    </div>
  );
}

export default BannerMac