import React, { useEffect, useState } from 'react';
import styles from './styles/slogan-mac.module.scss';

const slogans = [
  {
    slogan: "Hiệu Năng Đỉnh Cao Cho Mọi Tác Vụ",
    des: 'Mô tả Slogan 1',
    img: 'https://cdn.tgdd.vn/Files/2022/06/22/1441650/hinh-nen-dong-macbook.jpg'
  },
  {
    slogan: "Thiết Kế Tinh Tế, Đẳng Cấp Đỉnh Cao",
    des: 'Mô tả Slogan 2',
    img: 'https://cdn.tgdd.vn/Files/2022/06/22/1441650/hinh-nen-dong-macbook.jpg'
  },
  {
    slogan: "Màn Hình Retina Rực Rỡ, Sống Động Từng Chi Tiết",
    des: 'Mô tả Slogan 3',
    img: 'https://www.zdnet.com/a/img/resize/245fe42922204d23bd1a2f4f56d54f67a806deb5/2024/03/04/6571c255-430c-471b-a84f-c8b5526cd7cf/dsc09975-2.jpg?auto=webp&fit=crop&height=900&width=1200'
  }
];

const SloganMac: React.FC = () => {
  const [currentSlogan, setCurrentSlogan] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      setCurrentSlogan((prev) => (prev + 1) % slogans.length);
    }, 4000); // Đổi slogan mỗi 4 giây

    return () => clearInterval(interval);
  }, []);

  return (
    <div className={styles.sloganMac}>
      <div className={styles.sloganContainer}>
        {slogans.map((slogan, index) => (
          <div
            key={index}
            className={`${styles.sloganGroup} ${index === currentSlogan ? styles.active : ''}`}
          >
            <img src={slogan.img} alt={`Slogan ${index + 1}`} className={styles.sloganImage} />
            <div className={styles.sloganText}>
              <h1>{slogan.slogan}</h1>
              <p>{slogan.des}</p>
            </div>
          </div>
        ))}
        
      </div>
      <button className={styles.ctaButton}>Khám Phá MacBook</button>
    </div>
  );
};

export default SloganMac;
