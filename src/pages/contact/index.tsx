import React from 'react'
import styles from './styles/contact.module.scss'
import { NextPage } from 'next'
import { Meta } from '@/layout/Meta'
import ComponentPage from '@/components/ComponentPage/ComponentPage'
import { faEnvelope, faMessage, faUser } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

interface ISEO {
  title:string 
  description :string
} 

const SEO_CONTACT_PAGE: ISEO = {
  title:"IT QUY NHƠN | Mua Bán Sản Phẩm Apple Chính Hãng Giá Tốt",
  description:"IT QUY NHƠN chuyên cung cấp các sản phẩm Apple chính hãng như iPhone, iPad, MacBook, Apple Watch, AirPods với giá cạnh tranh. Mua sắm ngay để nhận nhiều ưu đãi hấp dẫn và dịch vụ tư vấn chuyên nghiệp."
}


const Contact:NextPage = () => {
  return (

    <ComponentPage isHiddenFooter={true} isHiddenHeader={true}>
    <Meta title={SEO_CONTACT_PAGE.title} description={SEO_CONTACT_PAGE.description} />
    
    <div className={`${styles.contact} item-center container`}>
          <div className={`${styles.contactInfo} w-50`}>
              
              <p className={styles.contactWith}>Liên hệ với chúng tôi</p>
              <h2 className={styles.titleSeo}>Liên hệ với chung tôi để phát triển sản phẩm nhiều hơn </h2>
              <div className={styles.groupInfo}>
                  <h3 className={styles.titleGroup}>Email</h3>
                  <p className={styles.detailInfo}>choappleonline1@gmail.com</p>
                  <p className={styles.detailInfo}>choappleonline2@gmail.com</p>
              </div>
              <div className={styles.groupInfo}>
                  <h3 className={styles.titleGroup}>Phone</h3>
                  <p className={styles.detailInfo}>choappleonline1@gmail.com</p>
                  <p className={styles.detailInfo}>choappleonline2@gmail.com</p>
              </div>
          </div>
          <div className={`${styles.contactForm} w-50`}>
              <div className={styles.layoutForm}>
                <form className={`${styles.formContact} container`}>
                      <div className={styles.groupField}>
                            <p className={styles.labelForm}>Name</p>
                            <span className={styles.icons}>
                              <FontAwesomeIcon icon={faUser} />
                            </span>
                            <input className={styles.inputField} placeholder='Họ và tên'/>
                      </div>
                      <div className={styles.groupField}>
                            <p className={styles.labelForm}>Email</p>
                            <span className={styles.icons}>
                              <FontAwesomeIcon icon={faEnvelope} />
                            </span>
                            <input className={styles.inputField} placeholder='Email Address'/>
                      </div>
                      <div className={`${styles.groupField} `}>
                            <p className={styles.labelForm}>Message</p>
                            <span className={styles.icoMessage}>
                              <FontAwesomeIcon icon={faMessage} />
                            </span>
                            <textarea className={styles.textField} placeholder='Viết Message'/>
                      </div>
                      <div className={styles.groupBtn}>
                            <button className={styles.btnSenMessage}> Send Message</button>
                      </div>

                </form>

              </div>

          </div>
      
    </div>

    
    </ComponentPage>
   
  )
}

export default Contact
