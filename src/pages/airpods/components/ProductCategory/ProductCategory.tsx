import React from 'react'
import styles from './styles/product-category.module.scss'


const ProductCategory = () => {
  return (
    <div className={`${styles.productCategory} container`} >
      <h2 className={styles.titleSection}>Sản phẩm thế hệ Airpods </h2>
      <div className={`${styles.contentLayout} item-center`}>
          <div className={`${styles.products} w-80 item-btw`}>
              <div className={styles.itemProduct}>
                    <div className={`${styles.image} ${styles.airG2}`}>

                    </div>
                    <p className={styles.nameProduct}>Airpods</p>
                    <p className={styles.gen}>Thế hệ thứ 2 </p>
              </div>
              <div className={styles.itemProduct}>
                    <div className={`${styles.image} ${styles.airG3}`}>

                    </div>
                    <p className={styles.nameProduct}>Airpods</p>
                    <p className={styles.gen}>Thế hệ thứ 3 </p>
              </div>
              <div className={styles.itemProduct}>
                    <div className={`${styles.image} ${styles.airProG2}`}>

                    </div>
                    <p className={styles.nameProduct}>Airpods</p>
                    <p className={styles.gen}>Thế hệ thứ 2 (USB-C) </p>
              </div>
              <div className={styles.itemProduct}>
                    <div className={`${styles.image} ${styles.airMax}`}>

                    </div>
                    <p className={styles.nameProduct}>Airpods</p>
                    <p className={styles.gen}>AirPods Max </p>
              </div>
          </div>

      </div>
    </div>
  )
}

export default ProductCategory