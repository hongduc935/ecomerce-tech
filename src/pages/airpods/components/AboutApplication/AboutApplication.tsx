import React from 'react'
import styles from './styles/about-application.module.scss'


const AboutApplication = () => {
  return (
    <div className={styles.aboutApplication}>
      <h2> IT QUY NHƠN - Chợ Mua Bán Trực Tuyến Đồ Dùng Apple Cũ  Hàng Đầu Của Người Việt</h2>
      <div className={styles.des}>
            IT QUY NHƠN, ra mắt vào đầu năm 2024, là một nền tảng mua bán trực tuyến chuyên biệt dành riêng cho các sản phẩm và phụ kiện Apple. Sứ mệnh của chúng tôi là tạo ra một không gian mua sắm tiện lợi, an toàn và đáng tin cậy cho các tín đồ của thương hiệu Apple, kết nối người mua và người bán một cách nhanh chóng và hiệu quả.

            <h3 className={styles.deslv1}>Tại sao chọn IT QUY NHƠN?</h3>
            IT QUY NHƠN tự hào là nền tảng hàng đầu Việt Nam chuyên cung cấp các sản phẩm và phụ kiện Apple chính hãng. Chúng tôi cung cấp hàng ngàn sản phẩm chất lượng, từ các thiết bị điện tử đến phụ kiện thời trang, đảm bảo đáp ứng nhu cầu đa dạng của khách hàng.


            <h3 className={styles.deslv1}>Các lĩnh vực chính trên IT QUY NHƠN:</h3>
            <p className={styles.deslv2}>1. Thiết bị Apple:</p>

            <p><b>- Điện thoại iPhone:</b> Mua bán các dòng iPhone từ iPhone 8 đến iPhone 14 Pro Max, đảm bảo chất lượng và bảo hành chính hãng.</p>
            <p><b>- Máy tính bảng iPad:</b> Từ iPad Mini đến iPad Pro, tất cả đều có sẵn với nhiều mức giá hợp lý.</p>
            <p><b>- Máy tính Mac:</b> MacBook, iMac, Mac Mini... đủ cấu hình, phù hợp với mọi nhu cầu sử dụng từ học tập đến công việc chuyên nghiệp.</p>
            
            <p className={styles.deslv2}>2. Phụ kiện Apple:</p>

            <p> <b>- AirPods:</b> Tai nghe không dây chất lượng cao, mang đến trải nghiệm âm thanh tuyệt vời.<br/></p>
            <p> <b>- Apple Watch:</b> Đồng hồ thông minh với nhiều tính năng hiện đại, phù hợp với mọi phong cách sống.<br/></p>
            <p> <b>- Phụ kiện bảo vệ:</b> Ốp lưng, kính cường lực, dây đeo đồng hồ, tất cả đều được thiết kế tinh tế và bền bỉ.</p>

            <p className={styles.deslv2}>3. Đồ dùng thời trang:</p>

            <p><b>- Túi xách, balo: </b>Được thiết kế để bảo vệ thiết bị Apple của bạn, từ các thương hiệu nổi tiếng và chính hãng.</p>
            <p><b>- Đồng hồ và trang sức:</b> Phong cách và hiện đại, phù hợp với các tín đồ Apple.</p>
            
            <p className={styles.deslv2}>4. Dịch vụ sửa chữa và bảo hành:</p><br/>

            <b>Sửa chữa chuyên nghiệp:</b> Dịch vụ sửa chữa chất lượng cao cho mọi thiết bị Apple, từ iPhone, iPad đến MacBook.<br/>
            <b>Bảo hành chính hãng: </b>Cam kết bảo hành dài hạn và đáng tin cậy.<br/>


            <h3 className={styles.deslv1}>Lợi ích khi sử dụng IT QUY NHƠN:</h3>
            <p><b>- Dễ dàng sử dụng:</b> Giao diện thân thiện và dễ sử dụng, giúp bạn tìm kiếm và mua sắm sản phẩm một cách nhanh chóng.<br/></p>
            <p><b>- Bảo mật cao:</b> Mọi giao dịch đều được bảo mật tối đa, đảm bảo an toàn thông tin cá nhân và thanh toán.<br/></p>
            <p><b>- Hỗ trợ khách hàng:</b> Đội ngũ hỗ trợ nhiệt tình, sẵn sàng giải đáp mọi thắc mắc và hỗ trợ trong quá trình mua bán.<br/></p>


            <p>
            Mua sắm thông minh và an toàn cùng IT QUY NHƠN
            Với IT QUY NHƠN, bạn có thể dễ dàng tìm kiếm và sở hữu những sản phẩm Apple chất lượng cao với mức giá hợp lý. Chỉ cần chụp hình sản phẩm, mô tả chi tiết và đăng tin miễn phí trên nền tảng của chúng tôi, sản phẩm của bạn sẽ nhanh chóng đến tay người cần.

            Ngoài ra, IT QUY NHƠN còn cung cấp các thông tin hữu ích về giá cả, xu hướng và mẹo mua sắm an toàn qua các bài viết trên blog của chúng tôi. Chúng tôi cam kết mang đến cho bạn trải nghiệm mua sắm tuyệt vời nhất.
            Đừng bỏ lỡ cơ hội sở hữu những sản phẩm Apple chính hãng và chất lượng ngay hôm nay.

            </p>
           
            <h3>IT QUY NHƠN - Nơi kết nối niềm đam mê công nghệ Apple.</h3>


      </div>
    </div>
  )
}

export default AboutApplication
