import React from 'react'
import styles from './styles/list-category.module.scss'


const ListCategory = () => {
  return (
    <div className={`${styles.listCategory} item-center w-100`}>
          <div className={`${styles.listItem} item-center w-70 `}>
                <ul className={`${styles.menu} item-btw`}>
                    <li className={`${styles.item} `}>
                        <a className={`${styles.link}`} href="/vn/macbook-air/" data-analytics-title="macbook air">
                          <figure className={`${styles.icon} ${styles.airG2}`} ></figure>
                          <p className={`${styles.label}`} role="text">Airpods thế hệ thứ 2</p>
                        </a>
                    </li>
                    <li className={`${styles.item} `}>
                        <a className={`${styles.link}`} href="/vn/macbook-air/" data-analytics-title="macbook air">
                          <figure className={`${styles.icon} ${styles.airProG3}`} ></figure>
                          <p className={`${styles.label}`} role="text">Airpods thế hệ thứ 3</p>
                        </a>
                    </li>
                    <li className={`${styles.item} `}>
                        <a className={`${styles.link}`} href="/vn/macbook-air/" data-analytics-title="macbook air">
                          <figure className={`${styles.icon} ${styles.airProG2}`} ></figure>
                          <p className={`${styles.label}`} role="text">Airpods Pro thế hệ thứ 2</p>
                        </a>
                    </li>
                    <li className={`${styles.item} `}>
                        <a className={`${styles.link}`} href="/vn/macbook-air/" data-analytics-title="macbook air">
                          <figure className={`${styles.icon} ${styles.airmax}`} ></figure>
                          <p className={`${styles.label}`} role="text">Airpods Max</p>
                        </a>
                    </li>
                    <li className={`${styles.item} `}>
                        <a className={`${styles.link}`} href="/vn/macbook-air/" data-analytics-title="macbook air">
                          <figure className={`${styles.icon} ${styles.appleMusic}`} ></figure>
                          <p className={`${styles.label}`} role="text">Apple Music</p>
                        </a>
                    </li>
                    
                </ul>

          </div>
    </div>
  )
}

export default ListCategory
