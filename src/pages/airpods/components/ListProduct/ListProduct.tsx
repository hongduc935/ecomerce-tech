import React from 'react'
import styles from './styles/list-product.module.scss'
import Image from 'next/image'
import Link from 'next/link'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowDown, faArrowUp } from '@fortawesome/free-solid-svg-icons'

// interface IProduct{
//   url_brand:string;
//   url_product:string;
//   alt_product:string;
//   date_post:string; 
//   price_product:number;
//   name_product:string;
// }

// interface IListProduct {
//   list_product : IProduct[]
// }



const ListProductDB:Number[] = [1,2,3,4,5,6,7,8,9,10,11,12]
interface IOption  {
  value:any,
  name:string
}

const GEN :IOption[]= [
  {
    value:"Airpods Gen 2",
    name:"Airpods Gen 2"
  },
  {
    value:"Airpods Gen 3",
    name:"Airpods Gen 3"
  },
  {
    value:"Airpods Gen 2 Pro",
    name:"Airpods Gen 2 Pro"
  },
]

const CHIP :IOption[]= [
  {
    value:"New Seal ",
    name:"New Seal "
  },
  {
    value:"Like New",
    name:"Like New"
  }
]


const ListProduct = () => {
  return (
    
    <div className={`${styles.listProduct} container`}>
      <h2 className={styles.titleSection}> AirPods tại <span className={styles.titleBrand}>IT QUY NHƠN</span> </h2>
      <div className={styles.desSection}>
          Tìm kiếm  các sản phẩm AirPods theo thương hiệu 
      </div>
      <div className={`${styles.multiSearch} item-btw`}>
        
          <div className={`${styles.filterOption} w-30 item-btw` }>
              <div className={styles.itemSearch}>
                  <select className={styles.monitor}>
                      {GEN.map((item:IOption,index:number)=>{
                        return <option key={`chip_${index}`}>{item.value}</option>
                      })}
                      
                  </select>
              </div>
              <div className={styles.itemSearch}>
                  <select className={styles.monitor}>
                      {CHIP.map((item:IOption,index:number)=>{
                        return <option key={`chip_${index}`}>{item.value}</option>
                      })}
                      
                  </select>
              </div>
          </div>
          <div className={`${styles.sortPrice} item-btw `}>
                  <div className={styles.titleOption}>
                      Giá:  
                  </div> 
                  <div className={`${styles.groupBtn} item-btw`}>
                      <button className={`${styles.btnSort} item-center`}>
                          <FontAwesomeIcon icon={faArrowUp} />
                      </button>
                      <button className={`${styles.btnSort} item-center`}>
                          <FontAwesomeIcon icon={faArrowDown} />
                      </button>
                  </div>
          </div>
          
      </div>
      <div className={styles.mainListProducts}>
         <div className={`${styles.grid} `}>
            {ListProductDB.flatMap(( value:any,index:number)=>{
                console.log(value)
                return <div className={styles.cardProduct} key={`product_${index}`}>
                          <div className={`${styles.logoBrand} item-center`}>
                              <Image
                                  src={'https://s3.amazonaws.com/cdn.designcrowd.com/blog/100-Famous-Brand%20Logos-From-The-Most-Valuable-Companies-of-2020/apple-logo.png'}
                                  alt='Brand From Apple  '
                                  width={0}
                                  height={0}
                                  sizes="100vw"
                                  style={{ width: '100%', height: 'auto' }} // optional
                              />
                          </div>
                          <div className={`${styles.image} item-center`}>
                              <Image
                              src={'https://store.storeimages.cdn-apple.com/8756/as-images.apple.com/is/MTJV3?wid=400&hei=400&fmt=jpeg&qlt=90&.v=1694014871985'}
                              alt='Lap top '
                              width={0}
                              height={0}
                              sizes="100vw"
                              style={{ width: '100%', height: 'auto' }} // optional
                              // layout="fill"
                              objectFit="contain"
                              objectPosition="center"
                              />
                          </div>
                          <div className={styles.shortInfo}>
                              <p className={styles.title}>Macbook Pro M1 14" 2024 Ram 16GB , SSD 512</p>
                              <p className={styles.price}>31.000.000đ </p>
                              <p className={styles.day}>Ngày đăng : 24/12/2024</p>
                              <div className={`${styles.groupBtn}  item-btw`}>
                                    {/* <button className={`${styles.addCart} ${styles.btn}`}>Add Cart</button> */}
                                    <Link href={`/airpods/${index}`} target='blank'><button className={`${styles.buy} ${styles.btn}`}>Mua</button></Link>
                              </div>
                          </div>
                      </div>
              })}
         </div>
         <div className={`${styles.listBtnPagination} item-center`}>
              <div className={`${styles.groupBtn} `}>
                  <button className={styles.btn}>1</button>
                  <button className={styles.btn}>2</button>
                  <button className={styles.btn}>3</button>
                  <button className={styles.btn}>4</button>
                  <button className={styles.btn}>5</button>
              </div>
         </div>
      </div>
    </div>
  )
}

export default ListProduct