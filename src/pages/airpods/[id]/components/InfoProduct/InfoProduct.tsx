import React from 'react'
import styles from './styles/info-product.module.scss'

const InfoProduct = () => {
  return (
    <div className={`${styles.infoProduct} w-100 item-btw`}>
      <div className={`${styles.technical} w-50`}>
        <h3 className={styles.technicalTitle}>Thông số kĩ thuật </h3>
        <div className={styles.line}></div>
        <p className={styles.condition}>Tên sản phẩm   : Airpod Pro 2  </p>
        <p className={styles.condition}>Thế hệ   : 2 </p>
        <p className={styles.condition}>Tình trạng  : Mới new Seal </p>
        <p className={styles.description }>Mô tả tình trạng : Nữ dùng nên giữ kĩ , mua được 1 năm , trầy xướt ít như hình ......  </p>
      </div>
      <div className={`${styles.package} `}>
        <h3 className={styles.packageTitle}>Thông tin thêm </h3>
        <div className={styles.line}></div>
        <p className={styles.vat}>VAT : Sản phẩm đã bao gồm thuê VAT </p>
        <p className={styles.policy}>Bảo hành  : Theo chính sách của IT QUY NHƠN | 3 tháng  </p>
        <p className={styles.policy}>Phụ kiện  : Sạc , ốp , .... </p>
        <p className={styles.policy}>Trả góp  : Lãi suất 0% </p>
      </div>
    </div>
  )
}

export  default InfoProduct