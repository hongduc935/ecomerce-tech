import React from 'react'
import styles from './styles/similar-product.module.scss'



const SimilarProduct = () => {
  return (
    <div className={styles.similarProduct}>
      <div className={styles.layoutContent}>
          <h3 className={styles.title}>Sản phẩm tương tự </h3>
      </div>
      <div className={styles.listProductSimilar}>
        <div className={styles.products}>

        </div>
        <div className={`${styles.products} ${styles.ml}`}>

        </div>
        <div className={`${styles.products} ${styles.ml}`}>

        </div>
      </div>
    </div>
  )
}

export default SimilarProduct
