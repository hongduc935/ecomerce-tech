import React from 'react'
import styles from './styles/info-brand.module.scss'
import { faCircleCheck, faLocationDot, faPhone, faTag } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Link from 'next/link'


const InfoBrand = () => {
  return ( 
    <div className={styles.infoBrand}>
       <h3 className={styles.storeName}>Cửa hàng MacShop24h | Chứng nhận Của IT QUY NHƠN <span className={styles.iconVerify}> <FontAwesomeIcon icon={faCircleCheck} /></span> </h3>
       <div className={styles.layoutContent}>
            <div className={styles.listag}>
              <p className={`${styles.tag} ${styles.open}`}> <FontAwesomeIcon icon={faTag} /> &nbsp; Open 24/7 </p>
              <p className={`${styles.tag} ${styles.freeship} ${styles.ml}`}> <FontAwesomeIcon icon={faTag} />&nbsp;   FreeShip </p>
              <p className={`${styles.tag} ${styles.policy} ${styles.ml}`}>  <FontAwesomeIcon icon={faTag} /> &nbsp; Đổi trả 3 ngày  </p>
            </div>      
            <p className={styles.address}> <FontAwesomeIcon icon={faLocationDot} /> 201/10 Lê Văn Việt ,Phường Hiệp Phú , Thành phố Thủ Đức </p>
            <p className={styles.phone}> <FontAwesomeIcon icon={faPhone} /> 0354298203 </p>
            <p className={styles.warehouse}>Còn 1 sản phẩm </p>
            <p className={styles.price}> 1.500.000 VND </p>
            <div className={styles.amount}>
                <span className={styles.titleAmount}>Số lượng:</span>  
                <select className={styles.count}>
                  <option value={1}>1</option>
                  <option value={2}>2</option>
                  <option value={3}>3</option>
                  <option value={4}>4</option>
                </select>
            </div>
            <div className={`${styles.buyProduct}`}>
                <button className={styles.btnBuy}>Mua hàng</button>
                <button className={`${styles.btnBuy} ${styles.installment}`}>Trả góp</button>
            </div>
            <div className={styles.social}>
                  <div className={styles.icSocial}>
                        <Link href={`https://www.facebook.com/profile.php?id=61560626134517`} target='_blank' >
                          <div className={`${styles.image} ${styles.iconFa}`}/>
                        </Link>
                  </div>
                  <div className={styles.icSocial}>
                        <Link href={`https://www.facebook.com/profile.php?id=61560626134517`} target='_blank' >
                            <div className={`${styles.image} ${styles.iconZalo} ${styles.ml}`}/>
                        </Link>
                  </div>
                  <div className={styles.icSocial}>
                      <Link href={`https://www.facebook.com/profile.php?id=61560626134517`} target='_blank' >
                        <div className={`${styles.image} ${styles.iconInstar} ${styles.ml}`}/>
                      </Link>
                  </div>
                  <div className={styles.icSocial}>
                      <Link href={`https://www.facebook.com/profile.php?id=61560626134517`} target='_blank' >
                        <div className={`${styles.image} ${styles.iconMes} ${styles.ml}`}/>
                      </Link>
                  </div>
                  <div className={styles.icSocial}>
                      <Link href={`https://www.facebook.com/profile.php?id=61560626134517`} target='_blank' >
                        <div className={`${styles.image} ${styles.iconTelegram} ${styles.ml}`}/>
                      </Link>
                  </div>
                 
                  
            </div>
       </div>
    </div>
  )
}

export  default InfoBrand