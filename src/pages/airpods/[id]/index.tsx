import React from 'react'
import styles from './styles/airpods.module.scss'
import { GetServerSideProps, NextPage } from 'next/types'
import ComponentPage from '@/components/ComponentPage/ComponentPage'
import { Meta } from '@/layout/Meta'
// import Repository from '@/configs/axiosClient'
// import Gallery  from './components/Gallery/Gallery'
import {IImage}  from './components/Gallery/Gallery'
import InfoBrand from './components/InfoBrand/InfoBrand'
import InfoProduct from './components/InfoProduct/InfoProduct'
import SimilarProduct from './components/SimilarProduct/SimilarProduct'
import BreakCrumb from '@/components/BreakCrumb/BreakCrumb'
import dynamic from 'next/dynamic'

// const Gallery = dynamic(() => import('./components/Gallery/Gallery'), { ssr: false })
const Gallery = dynamic(() => import('./components/Gallery/Gallery'), { ssr: false });
// const InfoBrand = dynamic(() => import('./components/InfoBrand/InfoBrand'));
// const InfoProduct = dynamic(() => import('./components/InfoProduct/InfoProduct'));

interface ISEO {
  title:string
  description:string 
}

type IProps = {
  product:any
  seo:ISEO
}



const AirPodsDetail :NextPage<IProps> = ({product,seo}) => {

  console.log("PRODUCT: "+product)
  console.log("seo: "+seo) 

  const imageList:IImage[]  = [
    "https://media.cnn.com/api/v1/images/stellar/prod/220921163441-airpods-pro-2-review-1.jpg?c=16x9&q=w_800,c_fill",
    "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/AirPods_Pro_%282nd_generation%29.jpg/1200px-AirPods_Pro_%282nd_generation%29.jpg",
    "https://media.cnn.com/api/v1/images/stellar/prod/220921163441-airpods-pro-2-review-1.jpg?c=16x9&q=w_800,c_fill",
    "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/AirPods_Pro_%282nd_generation%29.jpg/1200px-AirPods_Pro_%282nd_generation%29.jpg"

  ]

  console.log('imageList:'+imageList)

  // const brandInfo = {}

  // const productInfo = {}
 

  return (
    <ComponentPage isHiddenFooter={true} isHiddenHeader={true}>
    <Meta title={seo.title} description={seo.description} />
     
      <div className={`${styles.airpods} container`}>
        <BreakCrumb/>
        <h1 className={styles.titlePageDetail}>IT QUY NHƠNe - Airpods Pro Gen 2 | Cũ 99 % </h1>
        <div className={styles.layoutContent}>
            <div className={styles.listImage }>
                 <Gallery listImage={imageList}/> 
            </div>
            <div className={styles.infoBrand}>
               <InfoBrand/>
            </div>
        </div>
        <div className={styles.infoProduct }>
            <InfoProduct/>
        </div>
        <div className={styles.productList}>
            <SimilarProduct/>
        </div>
      </div>
    </ComponentPage>
  )
}
export const getServerSideProps: GetServerSideProps <IProps>= async (context)=>{

  console.log("Contexxt:"+context)
  // const {  params } = context

  // const { id } = params as { id: string };

  const productDetail = {}
  // await Repository({url:"https://api.chec.io/v1/products",method:"POST",payload:{}})

  console.log("productDetail:"+productDetail)

  // console.log("ID:"+id)
  
  return {
    props:{
      product:productDetail,
      seo:{
        title:"IT QUY NHƠN | Airpod detail 1 giá dưới 1 triệu  ",
        description:"Airpod detail 1 giá dưới 1 triệu "
      }
    }
  }
}
export default AirPodsDetail