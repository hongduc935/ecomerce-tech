import React from 'react'
// import styles from './styles/airpods.module.scss'
import { Meta } from '@/layout/Meta'
import ComponentPage from '@/components/ComponentPage/ComponentPage'
import ListCategory from './components/ListCategory/ListCategory';
import ListProduct from './components/ListProduct/ListProduct';
import Accessory from './components/Accessory/Accessory';
import BlogSuggest from './components/ListBlogSuggest/BlogSuggest';
import AboutApplication from './components/AboutApplication/AboutApplication';
import ProductCategory from './components/ProductCategory/ProductCategory';


const AIR_PODS = {
  site_name: 'Chợ AirPods giá rẻ',
  title: 'IT QUY NHƠN | Mua Bán AirPods Cũ Chính Hãng Giá Rẻ',
  description: 'Mua bán AirPods cũ chính hãng tại IT QUY NHƠN - Cam kết chất lượng, giá tốt, bảo hành uy tín. Chuyên cung cấp các dòng AirPods cũ từ AirPods 1, 2, Pro đến AirPods Max. Liên hệ ngay để nhận ưu đãi hấp dẫn!',
  locale: 'en',
};

const AirPods = () => {
  return (
    <ComponentPage isHiddenFooter={true} isHiddenHeader={true}>
        <Meta title={AIR_PODS.title} description={AIR_PODS.description} />
        <ListCategory/>
        <ProductCategory/>
        <ListProduct/>
        <Accessory/>
        <BlogSuggest/>
        <AboutApplication/>
    </ComponentPage>
  )
}

export default AirPods
