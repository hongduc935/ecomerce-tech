import ComponentPage from '@/components/ComponentPage/ComponentPage'
import { Meta } from '@/layout/Meta'
import { MACBOOK } from '@/utils/AppConfig'
import { GetStaticProps, NextPage } from 'next'
import React from 'react'
interface IBlogProps {
  product: any;
}


const BlogIphone:NextPage = () => {
  return (
    <ComponentPage isHiddenFooter={true} isHiddenHeader={true}>
    <Meta title={MACBOOK.title} description={MACBOOK.description} />
      This is Blog Introduce new Iphone
    </ComponentPage>
  )
}


export const getStaticProps: GetStaticProps<IBlogProps> = async (context) => {

  const {  params } = context
  console.log(params)

  // const { id } = params as { id: string };

  // const productData = await fetch(`https://example.com/api/mac/blogs/${id}`);
  
  // const product = await productData.json();

  return {
    props: {
      product:{}
    },
    revalidate: 60 * 60 // Revalidate sau mỗi giờ
  };
};

export default BlogIphone
