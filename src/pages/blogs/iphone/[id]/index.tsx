import ComponentPage from '@/components/ComponentPage/ComponentPage'
// import Repository from '@/configs/axiosClient'
import { Meta } from '@/layout/Meta'
// import { MACBOOK } from '@/utils/AppConfig';
import { GetStaticProps, NextPage } from 'next'
import React from 'react'
import Navbar from '../../components/navbar/Navbar'
// import SectionContent, { ISectionBlog } from '../../components/section-content/SectionContent'


interface IBlogIphone{
  blog:any;
  seo:any;
}

// const blogs:ISectionBlog[] = [
//   {
//     title:"Hướng dẫn bảo quản macbook ",
//     description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla malesuada dui urna, eget suscipit dolor consectetur eu. Nam dapibus dapibus arcu tincidunt tristique. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam lectus nulla, malesuada nec consequat at, suscipit mollis arcu. Integer convallis eleifend ligula, porta sodales urna aliquam vel. Proin vel justo eros. Maecenas vitae ipsum facilisis, porttitor justo eu, mollis elit. Pellentesque posuere lorem nec elit pellentesque, id tempus elit venenatis. Duis imperdiet ante vitae velit elementum, quis porttitor est vulputate. Vestibulum efficitur hendrerit justo at tristique. Nam ultricies et ex eget ultrices. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Donec velit risus, accumsan eu dolor vel, rhoncus tristique velit. Donec lacinia massa a ligula luctus, eu varius enim scelerisque. Vestibulum eu odio lectus."
//   },
//   {
//     title:"Các dụng cụ không thể thiếu khi dùng macbook ",
//     description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla malesuada dui urna, eget suscipit dolor consectetur eu. Nam dapibus dapibus arcu tincidunt tristique. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam lectus nulla, malesuada nec consequat at, suscipit mollis arcu. Integer convallis eleifend ligula, porta sodales urna aliquam vel. Proin vel justo eros. Maecenas vitae ipsum facilisis, porttitor justo eu, mollis elit. Pellentesque posuere lorem nec elit pellentesque, id tempus elit venenatis. Duis imperdiet ante vitae velit elementum, quis porttitor est vulputate. Vestibulum efficitur hendrerit justo at tristique. Nam ultricies et ex eget ultrices. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Donec velit risus, accumsan eu dolor vel, rhoncus tristique velit. Donec lacinia massa a ligula luctus, eu varius enim scelerisque. Vestibulum eu odio lectus. "
//   },
//   {
//     title:"Một số tips khi dùng mac",
//     description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla malesuada dui urna, eget suscipit dolor consectetur eu. Nam dapibus dapibus arcu tincidunt tristique. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam lectus nulla, malesuada nec consequat at, suscipit mollis arcu. Integer convallis eleifend ligula, porta sodales urna aliquam vel. Proin vel justo eros. Maecenas vitae ipsum facilisis, porttitor justo eu, mollis elit. Pellentesque posuere lorem nec elit pellentesque, id tempus elit venenatis. Duis imperdiet ante vitae velit elementum, quis porttitor est vulputate. Vestibulum efficitur hendrerit justo at tristique. Nam ultricies et ex eget ultrices. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Donec velit risus, accumsan eu dolor vel, rhoncus tristique velit. Donec lacinia massa a ligula luctus, eu varius enim scelerisque. Vestibulum eu odio lectus. "
//   },
//   {
//     title:"Một số tips khi dùng mac",
//     description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla malesuada dui urna, eget suscipit dolor consectetur eu. Nam dapibus dapibus arcu tincidunt tristique. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam lectus nulla, malesuada nec consequat at, suscipit mollis arcu. Integer convallis eleifend ligula, porta sodales urna aliquam vel. Proin vel justo eros. Maecenas vitae ipsum facilisis, porttitor justo eu, mollis elit. Pellentesque posuere lorem nec elit pellentesque, id tempus elit venenatis. Duis imperdiet ante vitae velit elementum, quis porttitor est vulputate. Vestibulum efficitur hendrerit justo at tristique. Nam ultricies et ex eget ultrices. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Donec velit risus, accumsan eu dolor vel, rhoncus tristique velit. Donec lacinia massa a ligula luctus, eu varius enim scelerisque. Vestibulum eu odio lectus. "
//   }
// ]

const BlogsIphoneDetail:NextPage<IBlogIphone> = ({blog,seo}) => {

  console.log("Blog:"+blog)
  console.log("SEO:"+seo)

  return (
    <ComponentPage isHiddenFooter={true} isHiddenHeader={false}>
    <Meta title={seo.title} description={seo.description} />
      <Navbar/>
     
      {/* {
        blogs.flatMap((value:ISectionBlog,index:number)=>{
          return <SectionContent key={`sectionBlog_${index}`} title={value.title} description={value.description}/>
        })
      } */}

    </ComponentPage>
  )
}

export async function getStaticPaths() {

  // const blogs = await Repository({url:"/",method:"POST",payload:{params:{},body:{}}})

  // get all blog của iphone
  const blogsFake = [{id:1},{id:2},{id:3},{id:4},{id:5},{id:6}];

  const paths = blogsFake.map(post => ({
         params: { id: post.id.toString() },
  }));


  return { paths, fallback: false };
}

export const getStaticProps : GetStaticProps<IBlogIphone> =async (context)=>{

  const {params} = context 

  const {id} = params as { id: string };

  // const blog = await Repository({url:"/",method:"POST",payload:{params:{},body:{}}})

  const seo = {
    title:"Test title",
    description:"Description"
  }

  console.log("ID: "+id)

  return {
    props:{
      seo,
      blog:{}
    }
  }

}
export default BlogsIphoneDetail
