import React from 'react'
import styles from './styles/navbar.module.scss'
import Link from 'next/link'


const Navbar = () => {
  return (
    <div className={`${styles.navbarBlog} container`}>
         
          <div className={styles.logoBlog}>
              <div className={styles.img}></div>BLOG IT 
          </div>
          <div className={styles.category}>
              <ul className={styles.menu}>
                  <li className={styles.option}>Tham khảo nhiều hơn </li>
                  <li className={styles.option}>Trợ giúp </li>
                  <li className={styles.option}><Link href="/home">Mua Hàng</Link></li>
              </ul>
          </div>
    </div>
  )
}

export default Navbar