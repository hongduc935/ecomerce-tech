import React from 'react'
import styles from './styles/breadcumbs.module.scss'
import {  faHome } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Link from 'next/link'


const BreadCumbs = ({name_blog}:any) => {
  return (
    <div className={styles.breadcumbs}>
      <ul className={styles.break}>
          <li className={styles.itemBreak}>
          <Link href='/home'> <FontAwesomeIcon icon={faHome} />&nbsp; Home &nbsp;</Link>
          </li>
          <li className={styles.itemBreak}>
              | &nbsp; <Link href='/blogs'>Blogs</Link> &nbsp;
          </li>
          <li className={`${styles.itemBreak} ${styles.activeBreak}`}>
              | &nbsp;  {name_blog.length > 40 ? `${name_blog.slice(0, 40)}...` : name_blog}
          </li>
      </ul>
    </div>
  )
}

export default BreadCumbs