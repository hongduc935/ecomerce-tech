import React from 'react'
import styles from './styles/related-blog.module.scss'
import Link from 'next/link'
import { faAddressBook, faCalendarAlt, faChevronRight } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


const list_blog = [1,2,3,4,5]

const RelatedBlog = () => {
  return (
    <div className={styles.related}>
      <h3 className={styles.title}><FontAwesomeIcon icon={faAddressBook} /> Có thể bạn quan tâm </h3>
      <ul className={styles.listBlog}>
        {list_blog.map((value:any,index:number)=>{
          console.log(value)
            return <li className={styles.itemBlog} key={`${index}-blog`}>
              <div className={styles.groupTitle}>
                 
                  <div className={styles.nameBlog}>
                    <Link href={"#"}>Tham khảo Tips mua Macbook cũ</Link>
                  </div>
              </div>
            <p className={styles.shortDes}>Bạn đang tìm kiếm sản phẩm Apple chính hãng like new với giá thành hợp lý? ...</p>
            <div className={styles.infoBlog}>
              <div className={styles.auth}>
              <FontAwesomeIcon icon={faChevronRight} /><FontAwesomeIcon icon={faChevronRight} /><FontAwesomeIcon icon={faChevronRight} />&nbsp; Tham khảo thêm
              </div>
              <div className={styles.break}>|</div>
              <div className={styles.time}>
              <FontAwesomeIcon icon={faCalendarAlt} />&nbsp;20/12/2001
              </div>
            </div>
          </li>
        })}
          
      </ul>
    </div>
  )
}

export default RelatedBlog