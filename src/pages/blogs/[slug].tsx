import React from 'react'
import { GetServerSideProps } from 'next'
import BlogService from '@/apis/blog/BlogService'
import ComponentPage from '@/components/ComponentPage/ComponentPage'
import { Meta } from '@/layout/Meta'
import styles from './styles/blog-style-guide.module.scss'
import layout from './styles/layout-slug.module.scss'
import 'react-quill/dist/quill.snow.css'; 
import Navbar from './components/navbar/Navbar'
import RelatedBlog from './components/related/RelatedBlog'
import BreadCumbs from './components/breadcumbs/BreadCumbs'
// import Link from 'next/link'
import { FacebookShareButton, FacebookIcon, FacebookMessengerIcon, FacebookMessengerShareButton, TwitterIcon, TwitterShareButton } from 'react-share'
import { faCalendar, faComment, faEye } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
interface Props {
  blog: {
    _id:string;
    blog_title: string;
    blog_short_content: string;
    blog_main_image?: string;
    blog_content: string;
    blog_meta_description?: string;
    category?: string;
    slug?: string;
    domain?: string;
    blog_og_image?:string;
    blog_keywords?:string;
    createdAt?:string;
    updatedAt?:string;
  } | null;
}

const BlogPost: React.FC<Props> = ({ blog }) => {

  console.log(blog)
  if (!blog) {
    // window.location.href='/mac-studio'
    return <div>Blog not found</div>;
  }
  const shareUrl = `${process.env.NEXT_PUBLIC_SITE_URL}/blog/${blog.blog_title}`;
  const shareTitle = blog.blog_title;
  const shareHashtag = '#YourHashtag';


  return (
    <ComponentPage isHiddenFooter={true} isHiddenHeader={false}>
      <Meta 
        title={blog.blog_title} 
        description={blog.blog_short_content} 
        image_url={blog.blog_main_image} 
      />
      
      <div className={layout.blogContainer}>
        <div className={layout.nav}>
          <Navbar />
         
        </div>
        
        <div className={`${layout.mainBlog} container`}>
          <div className={`${layout.blogs} `}>
            <div className={`${layout.headBlog} item-btw `}>
              <div className={layout.breakCumbs}>
                <BreadCumbs name_blog={blog.blog_title} />
               
                
              </div>
              
            </div>


            <div className={`${layout.report} item-btw `}>
              <div className={`${layout.detailPost} item-start`}>
                      <p className={`${layout.share} ${layout.time}`}><FontAwesomeIcon icon={faCalendar} /> 2024/12/02</p>
                      <p className={`${layout.share} ${layout.report}`} ><FontAwesomeIcon icon={faComment} /> 5 Đánh giá</p>
                      <p className={`${layout.share} ${layout.view}`}><FontAwesomeIcon icon={faEye} /> 1k Lượt Xem</p>
              </div>
              <div className={layout.shares}>
                  <div className={layout.social}>
                    <FacebookShareButton
                      url={shareUrl}
                      title={shareTitle}
                      hashtag={shareHashtag}
                    >
                      <FacebookIcon size={26} round={true} />
                    </FacebookShareButton>
                  </div>

                  <div className={layout.social}>
                    <TwitterShareButton
                      url={shareUrl}
                      title={shareTitle}
                      hashtags={[shareHashtag]}
                    >
                      <TwitterIcon size={26} round={true} />
                    </TwitterShareButton>
                  </div>

                  <div className={layout.social}>
                    <FacebookMessengerShareButton
                      url={shareUrl}
                      appId={process.env.NEXT_PUBLIC_FACEBOOK_APP_ID || ''}
                    >
                      <FacebookMessengerIcon size={26} round={true} />
                    </FacebookMessengerShareButton>
                  </div>
              </div>
            </div>
           

            <article className={`${styles.blogPost} `}>
              <div className={styles.titleBlog}>
                <h1 className={styles.titleH1}>{blog.blog_title}</h1>
                <p className={styles.shortDesription}>
                  <em>{blog.blog_short_content}</em>
                </p>
              </div>
              <div dangerouslySetInnerHTML={{ __html: blog.blog_content }} />
            </article>
          </div>

          <div className={layout.related}>
            <RelatedBlog />
          </div>
        </div>
      </div>
    </ComponentPage>
  );
};


export const getServerSideProps: GetServerSideProps = async ({ params }) => {
  try {
    const { slug } = params as { slug: string };
    const res = await BlogService.getBlogBySlug(slug);

    if (!res.data) {
      return { notFound: true };
    }

    return {
      props: {
        blog: res.data,
      },
    };
  } catch (error) {
    console.error('Error fetching blog:', error);
    return {
      props: {
        blog: null,
      },
    };
  }
};


export default BlogPost