import { GetStaticProps, NextPage } from 'next'
import React from 'react'

interface IBlogProps {
  product?: any;
}

const BlogMac:NextPage<IBlogProps> = ({product}) => {
  console.log(product)
  return (
    <div>
      This is Page Blog Mac
    </div>
  )
}


export const getStaticProps: GetStaticProps<IBlogProps> = async (context) => {

  const {  params } = context

  console.log(params)

  // const { id } = params as { id: string };

  // const productData = await fetch(`https://example.com/api/mac/blogs/${id}`);
  
  // const product = await productData.json();

  return {
    props: {
      product:{}
    },
    revalidate: 60 * 60 // Revalidate sau mỗi giờ
  };
};

export default BlogMac
