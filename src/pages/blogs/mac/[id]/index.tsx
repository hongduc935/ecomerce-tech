import ComponentPage from '@/components/ComponentPage/ComponentPage'
// import Repository from '@/configs/axiosClient'
import { Meta } from '@/layout/Meta'
import { MACBOOK } from '@/utils/AppConfig'
import { GetStaticProps, NextPage } from 'next'

import React from 'react'

interface IBlogMac{

  seo: any,
  blog:any

}

const BlogsMacDetail:NextPage<IBlogMac> = ({seo,blog}) => {

  console.log("SEO:"+seo)
  console.log("Blog:"+blog)

  return (
    <ComponentPage isHiddenFooter={true} isHiddenHeader={true}>
    <Meta title={MACBOOK.title} description={MACBOOK.description} />
      This is BlogsMacDetail
    </ComponentPage>
  )
}

export async function getStaticPaths() {

  // const blogs = await Repository({url:"/",method:"POST",payload:{params:{},body:{}}})

  // console.log(blogs)

  const blogsFake = [{id:1},{id:2},{id:3},{id:4},{id:5},{id:6}];

  const paths = blogsFake.map(post => ({
         params: { id: post.id.toString() },
  }));


  return { paths, fallback: false };
}

export const getStaticProps : GetStaticProps<IBlogMac> =async (context)=>{

  const {params} = context 

  const {id} = params as { id: string };

  // const blog = await Repository({url:"/",method:"POST",payload:{params:{},body:{}}})

  const seo = ""

  console.log("ID: "+id)

  return {
    props:{
      seo,
      blog:{}
    }
  }

}

export default BlogsMacDetail
