import ComponentPage from '@/components/ComponentPage/ComponentPage'
// import Repository from '@/configs/axiosClient'
import { Meta } from '@/layout/Meta'
import { GetStaticProps, NextPage } from 'next'
import React from 'react'

interface IBlogs{
  product_name? :string ,
  blog_title?:string,
  blog_description ?:string
}

interface ISEO {
  title  :string ,
  desctiption :string
}

interface IBlogAirPods{
  blog:IBlogs 
  seo:ISEO
}

const BlogAirPodsDetail:NextPage<IBlogAirPods> = ({blog,seo}) => {
  return (
    <ComponentPage isHiddenFooter={true} isHiddenHeader={true}>
    <Meta title={seo.title} description={seo.desctiption} />
      This is BlogAirPodsDetail
      <p>Product name :{blog.product_name}</p>
      <p>Product Title  :{blog.blog_title}</p>
      <p>Product Description :{blog.blog_description}</p>
    </ComponentPage>
  )
}

export async function getStaticPaths() {

  // const blogs = await Repository({url:"/",method:"POST",payload:{params:{},body:{}}})

  // console.log(blogs)

  const blogsFake = [{id:1},{id:2},{id:3},{id:4},{id:5},{id:6}];

  const paths = blogsFake.map(post => ({
         params: { id: post.id.toString() },
  }));

  return { paths, fallback: false };
}

export const getStaticProps : GetStaticProps<IBlogAirPods> =async (context)=>{

  const {params} = context 

  const {id} = params as { id: string };

  // const blog = await Repository({url:"/",method:"POST",payload:{params:{},body:{}}})




  console.log("ID: "+id)

  return {
    props:{
      seo:{
        title:"IT QUY NHƠN | AirPods Pro",
        desctiption:"AirPods Pro new Gen"
      },
      blog:{
        product_name:"AirPods Pro",
        blog_title:"AirPods Pro new Gen ",
        blog_description :"AirPods Pro new Gen "
      }
    }
  }

}
export default BlogAirPodsDetail
