import Repository from '@/configs/axiosClient'
import { GetServerSideProps, NextPage } from 'next'
import React from 'react'
import styles from './styles/blog-main.module.scss'
import Image from 'next/image';
import ComponentPage from '@/components/ComponentPage/ComponentPage';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {  faArrowRight, faHeadphones, faLaptopCode, faMagnifyingGlass, faMobileScreenButton, faTowerBroadcast } from '@fortawesome/free-solid-svg-icons';
import { Meta } from '@/layout/Meta';
import Link from 'next/link';

interface IBlog {
  _id: string;
  blog_title: string;
  blog_meta_description: string;
  blog_keywords: string;
  blog_short_content: string;
  blog_main_image: string;
  blog_og_image: string;
  blog_content: string;
  category: string;
  domain: string;
  slug: string;
  createdAt: string;
  updatedAt: string;
}

interface IBlogProps {
  blogs:IBlog[];
}

const TITLE_PAGE = "IT QUY NHƠN | Tổng hợp các thông tin mới nhất các sản đến từ Apple"

const META_DESCRIPTION = "Cập nhật thông tin mới nhất về các sản phẩm từ Apple như iPhone, iPad, MacBook, và phụ kiện. Khám phá tin tức, đánh giá, và ưu đãi hấp dẫn tại IT QUY NHƠN."

const Blogs: NextPage<IBlogProps> = ({ blogs }:IBlogProps) => {

  if (blogs) {
    console.log("Length:"+ blogs.length)
  }

  const ListBlog = [1, 2, 3, 4, 5, 6, 7]

  console.log(ListBlog)

  return (
    <ComponentPage isHiddenFooter={true} isHiddenHeader={true}>
      <Meta title={TITLE_PAGE} description={META_DESCRIPTION} />
      <div className={`${styles.blog} container`}>
        <div className={styles.breadCumbs}>
          <h1 className={styles.titleMain}>Tổng hợp các tin tức mới nhất về các sản phẩm Apple</h1>
        </div>
        <div className={styles.section}>

          
          <div className={styles.listblog}>
            <div className={styles.head}>
              < h2 className={styles.titlePage}>Bài viết mới nhất</h2>

              <div className={styles.tabCategory}>
                <div className={`${styles.item} item-btw`}>
                  <div><FontAwesomeIcon icon={faLaptopCode} /> &nbsp;Công Nghệ</div>
                 
                </div>
                <div className={`${styles.item} item-btw`}>
                  <div><FontAwesomeIcon icon={faHeadphones} /> &nbsp;Du Lịch</div>
                 
                </div>
                <div className={`${styles.item} item-btw`}>
                  <div><FontAwesomeIcon icon={faMobileScreenButton} /> &nbsp;Nhân Sự </div>
                 
                </div>
                <div className={`${styles.item} item-btw`}>
                  <div><FontAwesomeIcon icon={faLaptopCode} /> &nbsp;Sự Kiện</div>
                 
                </div>
                <div className={`${styles.item} item-btw`}>
                  <div><FontAwesomeIcon icon={faTowerBroadcast} /> &nbsp;Nhề nghiệp</div>
                  
                </div>
              </div>

              <div className={styles.search}>
                <input type='text' className={styles.inputSearch} placeholder='Tìm kiếm' />
                <span className={styles.iconSearch}><FontAwesomeIcon icon={faMagnifyingGlass} /></span>

              </div>
            </div>

            <div className={styles.listItem}>

              {
                blogs && blogs.length > 0 ? blogs.map((value: any, index: number) => {
                  return <Link href={`/blogs/${value.slug}`} key={`${index}_card`}>
                  <div className={styles.cardBlog} >
                    <div className={styles.image}>
                      <Image
                        src={"https://media-cdn-v2.laodong.vn/storage/newsportal/2023/8/26/1233821/Giai-Nhi-1--Nang-Tre.jpg"}
                        alt=''
                        width={0}
                        height={0}
                        sizes="100vw"
                        objectFit='contain'
                        blurDataURL="../../assets/images/loading-blur.gif"
                        placeholder="blur" 
                        loading="lazy" 
                        style={{ width: '100%', height: '200px' }} // optional
                      />
                    </div>
                    <div className={styles.contentBlog}>
                      <h3 className={styles.titleBlog}>{value.blog_title}. </h3>
                      <p className={styles.shortContent}>
                        {value.blog_meta_description && value.blog_meta_description.length > 100
                          ? `${value.blog_meta_description.slice(0, 180)}...`
                          : ''}
                      </p>

                    </div>
                    <div className={styles.contentBottom}>
                      <div className={styles.left}>
                        <p className={styles.readMore}> <FontAwesomeIcon icon={faArrowRight} /> &nbsp;Đọc Thêm  </p>
                      </div>
                      <div className={`${styles.right} item-center`}>
                        <p className={`${styles.createTime} `}> 2024-12-23  </p>
                      </div>
                    </div>
                  </div>
                  </Link>
                  
                }):<></>
              }



            </div>
            <div className={`${styles.pagination} item-center`}>
                  <div className={styles.groupPagination}>
                      <button className={styles.btnPn}> Pre </button>
                      <button className={`${styles.btnPn} ${styles.active}`}>1</button>
                      <button className={styles.btnPn}>2</button>
                      <button className={styles.btnPn}> Next </button>
                  </div>
            </div>
          </div>
        </div>
      </div>
    </ComponentPage>

  )
}


export const getServerSideProps: GetServerSideProps<IBlogProps> = async (context) => {

  // Mặc định 
  let page :any = 1; // Trang 

  let limit:any = 10; // Số lượng blog 

  let category:any = null

  let domain:any = 'choappleonline.com'

  const { query } = context

  if(query && query?.page){
      page = query.page
  }

  if(query && query?.limit){
      limit = query.limit
  }

  if(query && query?.category){
    category = query.category
  }

  const payload: any = {
    body: {
      category,
      page,
      limit,
      domain
    },
    params: {}
  }

  const blogs:any = await Repository({ url: 'http://localhost:4000/api/v1/blog/mac', method: 'POST', payload: payload })

  return {
    props: {
      blogs: blogs.data
    }
  };
};

export default Blogs
