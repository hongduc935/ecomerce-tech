import ComponentPage from '@/components/ComponentPage/ComponentPage'
import React from 'react'
import styles from './styles/gift.module.scss'
import { Meta } from '@/layout/Meta'

const MetaSeo = {
  title: 'Quà Tặng Đặc Biệt Tại Quy Nhơn - LD GIFT',
  description: 'Khám phá bộ sưu tập quà tặng độc đáo và ý nghĩa tại LD GIFT. Giao hàng tận nơi tại Quy Nhơn.',
  canonical: 'https://www.yourdomain.com/gift',
  image_url: 'https://www.yourdomain.com/path/to/default-image.jpg'
};

const services = [
  {
    name: "Giao hàng nhanh chóng",
    image: "https://images.unsplash.com/photo-1507643179773-3e975d7ac515?fm=jpg&q=60&w=3000&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8Z2FsbGVyeXxlbnwwfHwwfHx8MA%3D%3D",
    description: "Giao hàng nhanh chóng và miễn phí tại khu vực Quy Nhơn."
  },
  {
    name: "Đa dạng món quà",
    image: "https://images.unsplash.com/photo-1507643179773-3e975d7ac515?fm=jpg&q=60&w=3000&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8Z2FsbGVyeXxlbnwwfHwwfHx8MA%3D%3D",
    description: "Đa dạng các món quà cho các dịp lễ tết, sinh nhật, kỷ niệm."
  },
  {
    name: "Đóng gói tinh tế",
    image: "https://images.unsplash.com/photo-1507643179773-3e975d7ac515?fm=jpg&q=60&w=3000&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8Z2FsbGVyeXxlbnwwfHwwfHx8MA%3D%3D",
    description: "Đóng gói cẩn thận, tinh tế, phù hợp làm quà tặng."
  },
  {
    name: "Tư vấn miễn phí",
    image: "https://images.unsplash.com/photo-1507643179773-3e975d7ac515?fm=jpg&q=60&w=3000&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8Z2FsbGVyeXxlbnwwfHwwfHx8MA%3D%3D",
    description: "Dịch vụ tư vấn miễn phí để chọn món quà phù hợp nhất."
  }
];


const products = [
  {
    name: 'Bó Hoa Tươi Thắm',
    image: 'https://hanoihoa.net/wp-content/uploads/2019/02/hn-83021.jpg', // Thay đổi đường dẫn hình ảnh theo thực tế
    price: '350,000'
  },
  {
    name: 'Bó Hoa Tươi Thắm',
    image: 'https://hanoihoa.net/wp-content/uploads/2019/02/hn-83021.jpg', // Thay đổi đường dẫn hình ảnh theo thực tế
    price: '350,000'
  },
  {
    name: 'Bó Hoa Tươi Thắm',
    image: 'https://hanoihoa.net/wp-content/uploads/2019/02/hn-83021.jpg', // Thay đổi đường dẫn hình ảnh theo thực tế
    price: '350,000'
  },
  // Thêm nhiều sản phẩm tùy ý
];

const Gift = () => {
  return (
    <ComponentPage isHiddenFooter={true} isHiddenHeader={true}>
      <Meta {...MetaSeo} />

      <header className={styles.header}>
        <h1 className={styles.titlepage}>LD GIFT - Quà Tặng Đặc Biệt Tại Quy Nhơn</h1>
      </header>
      <main>
      <div className={styles.videoBackground}>
        <video autoPlay loop muted playsInline>
          <source src="/gift.mp4" type="video/mp4" />
          Your browser does not support the video tag.
        </video>
      </div>
        <section className={`${styles.introduction} container `}>
          <p>Chào mừng bạn đến với LD GIFT, nơi cung cấp các món quà đặc biệt cho mọi dịp lễ tại Quy Nhơn. Với kinh nghiệm lâu năm trong ngành quà tặng, chúng tôi tự hào mang đến cho khách hàng những sản phẩm độc đáo và ý nghĩa nhất, phù hợp với mọi lứa tuổi và sở thích.</p>
        </section>
        <section className={`${styles.features} container`}>
          <h2 className={styles.titleSection}>Dịch Vụ Nổi Bật</h2>
          {/* <ul>
            <li>Giao hàng nhanh chóng và miễn phí tại khu vực Quy Nhơn.</li>
            <li>Đa dạng các món quà cho các dịp lễ tết, sinh nhật, kỷ niệm.</li>
            <li>Đóng gói cẩn thận, tinh tế, phù hợp làm quà tặng.</li>
            <li>Dịch vụ tư vấn miễn phí để chọn món quà phù hợp nhất.</li>
          </ul> */}
          <div className={styles.gallery}>
          {services.map((service, index) => (
            <div key={index} className={styles.galleryItem}>
              <img src={service.image} alt={service.name} className={styles.galleryImage} />
              <div className={styles.overlay}>
                <h3 className={styles.galleryTitle}>{service.name}</h3>
                <p className={styles.galleryDescription}>{service.description}</p>
              </div>
            </div>
          ))}
        </div>
        </section>
        <section className={`${styles.gallery} `}>
          <h2 className='container'>Quà tặng 8/3 Quốc tế Phụ Nữ</h2>
          <div className='container'>
          <div className={`${styles.cartItemsContainer} `}>

              {
                products.map((product, index) => (<div className={styles.cartItem} key={`product${index}`}>
                  <img src={product.image} alt={product.name} className={styles.productImage} />
                  <div className={styles.productDetails}>
                    <h3 className={styles.productName}>{product.name}</h3>
                    <p className={styles.productPrice}>{`Giá: ${product.price} VNĐ`}</p>
                    <button className={styles.addButton} onClick={() => console.log("Thêm vào giỏ hàng")}>
                      Thêm vào giỏ hàng
                    </button>
                  </div>
                </div>))
              }


              </div>
          </div>
          
          {/* <img src="https://www.yourdomain.com/images/gift-1.jpg" alt="Quà Tặng Độc Đáo" />
          <img src="https://www.yourdomain.com/images/gift-2.jpg" alt="Quà Tặng Công Nghệ" />
          <img src="https://www.yourdomain.com/images/gift-3.jpg" alt="Quà Tặng Handmade" /> */}
        </section>
        <section className={`${styles.testimonials} container`}>
          <h2>Phản Hồi Từ Khách Hàng</h2>
          <blockquote>
            <p>"Tôi luôn tin tưởng chọn LD GIFT cho mọi dịp tặng quà. Sản phẩm đẹp, chất lượng và dịch vụ rất tốt!" - Nguyễn Văn A</p>
          </blockquote>
        </section>
        <section className={styles.contact}>
          <h2>Liên Hệ Với Chúng Tôi</h2>
          <p>Địa chỉ: 123 Đường ABC, Quy Nhơn, Bình Định</p>
          <p>Điện thoại: 090 123 4567</p>
          <p>Email: contact@ldgift.vn</p>
        </section>
      </main>
    </ComponentPage>
  )
}

export default Gift