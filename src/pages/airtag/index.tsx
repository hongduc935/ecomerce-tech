import React from 'react'
import styles from './styles/airtag.module.scss'
import ComponentPage from '@/components/ComponentPage/ComponentPage'
import { Meta } from '@/layout/Meta'

const AIR_TAG = {
  site_name: 'IT QUY NHƠN giá rẻ',
  title: 'IT QUY NHƠN | Mua Bán AirTag Chính Hãng Giá Rẻ',
  description: 'Khám phá AirTag chính hãng với giá rẻ tại IT QUY NHƠN - Sản phẩm chất lượng, giá tốt, bảo hành uy tín. Cung cấp các loại AirTag đa dạng, từ AirTag Pro đến AirTag Air. Liên hệ ngay để sở hữu sản phẩm với ưu đãi hấp dẫn!',
  locale: 'en',
};


const AirTag = () => {
  return (
    <ComponentPage isHiddenFooter={true} isHiddenHeader={true}>
    <Meta title={AIR_TAG.title} description={AIR_TAG.description} />
    <div className={styles.airtag}>
      This is air tag
    </div>
    </ComponentPage>
  )
}

export default AirTag
