import React from 'react'
import styles from './styles/blog-product.module.scss'
const BlogProduct = () => {
  return (
    <section className={`${styles.blogProduct} `}>
      <h2 className={styles.titleSection}>Tìm hiểu thêm về sản phẩm </h2>
      <div className={styles.layoutListProduct}>
          <div className={styles.cardBlog}>
              Cart 1 
          </div>
          <div className={`${styles.cardBlog} ${styles.ml}`}>
              Cart 1 
          </div>
          <div className={`${styles.cardBlog} ${styles.ml}`}>
              Cart 1 
          </div>
          <div className={`${styles.cardBlog} ${styles.ml}`}>
              Cart 1 
          </div>
      </div>
    </section>
  )
}

export default BlogProduct