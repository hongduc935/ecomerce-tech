import React, { useRef } from 'react'
import styles from './styles/gallery.module.scss'
// import { Swiper, SwiperSlide } from 'swiper/react'
import { Swiper as SwiperType } from 'swiper'
// import { FreeMode, Navigation, Thumbs } from 'swiper/modules'
// import Image from 'next/image'
import { faChevronLeft, faChevronRight } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import 'swiper/css'
// import 'swiper/css/free-mode'
// import 'swiper/css/navigation'
// import 'swiper/css/thumbs'

export type IImage = string

export type IGalery ={
  listImage : IImage[]
}

const Gallery = ({listImage}:IGalery) => {
  // const [thumbsSwiper, setThumbsSwiper] = useState<any>(null);
  const swiperRef = useRef<SwiperType>();
  console.log(listImage)
  return (
    <div className={styles.gallery}>
      {/* <Swiper
        loop={true}
        spaceBetween={10}
        navigation={true}
        thumbs={{ swiper: thumbsSwiper }}
        modules={[FreeMode, Navigation, Thumbs]}
        
        onBeforeInit={(swiper) => {
          swiperRef.current = swiper;
        }}
        className={`${styles.galeryProduct} listProduct `}
      >
        {
          listImage.map((value:string,index:number )=>{
            return <SwiperSlide key={`image_product1_${index}`}>
                      <div className={styles.imageProduct}>
                                    <Image
                                      src={`${value}`}
                                      alt='Lap top '
                                      width={0}
                                      height={0}
                                      loading="lazy"
                                      sizes="100vw"
                                      style={{ width: '100%', height: 'auto' }} // optional
                                    />
                      </div>
                  </SwiperSlide>
          })
        }
        
      </Swiper>
      <Swiper
        onSwiper={setThumbsSwiper}
        loop={true}
        spaceBetween={10}
        slidesPerView={4}
        freeMode={true}
        watchSlidesProgress={true}
        modules={[FreeMode, Navigation, Thumbs]}
        className={`${styles.galleryBottom}`}
      >
        {
          listImage.map((value:string,index:number )=>{
            return <SwiperSlide key={`image_product_${index}`}>
                    <div className={styles.imageProduct}>
                        <Image
                          src={`${value}`}
                          alt='Lap top '
                          width={0}
                          height={0}
                          loading="lazy"
                          sizes="100vw"
                          style={{ width: '100%', height: '100%' }} // optional
                        />
                  </div>
            </SwiperSlide>
          })
        }
        
      </Swiper> */}
      <div className={styles.listBtnSwiperCustom}>
          <div className={`${styles.btnSwiper} ${styles.btnNaviLeft} item-center`} onClick={() => swiperRef.current?.slidePrev()}><FontAwesomeIcon icon={faChevronLeft} /></div>
          <div  className={`${styles.btnSwiper} ${styles.btnNaviRight} item-center`}  onClick={() => swiperRef.current?.slideNext()}><FontAwesomeIcon icon={faChevronRight} /></div>
      </div> 
     
    </div>
  )
}

export default Gallery