import React, {  useRef } from 'react'
import styles from './styles/accessory.module.scss'
import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import 'swiper/css/free-mode'
import 'swiper/css/pagination'
import { Navigation } from 'swiper/modules'
import Image from 'next/image'
import Link from 'next/link'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleLeft, faAngleRight } from '@fortawesome/free-solid-svg-icons'

interface IAccessory {
  url:string
  name:string 
}

const AccessoryMac :IAccessory[] = [

  {
    url:"https://store.storeimages.cdn-apple.com/8756/as-images.apple.com/is/MUF82?wid=532&hei=582&fmt=png-alpha&.v=1590526633000",
    name:"Bộ Tiếp Hợp Nhiều Cổng USB-C Digital AV"
  },
  {
    url:"https://store.storeimages.cdn-apple.com/8756/as-images.apple.com/is/MJ1M2?wid=532&hei=582&fmt=png-alpha&.v=1591650473000",
    name:"Cáp Chuyển Đổi USB sang USB-C"
  },
  {
    url:"https://store.storeimages.cdn-apple.com/8756/as-images.apple.com/is/MLYV3?wid=532&hei=582&fmt=png-alpha&.v=1698168621458",
    name:" Cáp chuyển từ USB-C sang MagSafe 3 (2 m) - Bạc"
  },
  {
    url:"https://store.storeimages.cdn-apple.com/8756/as-images.apple.com/is/MUFG2?wid=532&hei=582&fmt=png-alpha&.v=1539383768884",
    name:"Đầu Đọc Thẻ SD sang USB-C"
  },
  {
    url:"https://store.storeimages.cdn-apple.com/8756/as-images.apple.com/is/MQLN3_GEO_VN?wid=532&hei=582&fmt=png-alpha&.v=1685466384365",
    name:"Bộ Tiếp Hợp Nguồn USB-C 70W"
  },
  {
    url:"https://store.storeimages.cdn-apple.com/8756/as-images.apple.com/is/MU2G3?wid=532&hei=582&fmt=png-alpha&.v=1693236163178",
    name:"Cáp Sạc USB-C 240W (2 m)"
  },
  {
    url:"https://store.storeimages.cdn-apple.com/8756/as-images.apple.com/is/MU883?wid=532&hei=582&fmt=png-alpha&.v=1693262391573",
    name:"Cáp Thunderbolt 4 (USB‑C) Pro (1 m)"
  },
  {
    url:"https://store.storeimages.cdn-apple.com/8756/as-images.apple.com/is/MM0A3?wid=532&hei=582&fmt=png-alpha&.v=1632956386000",
    name:"Cáp USB-C sang Lightning (1m)"
  },
  {
    url:"https://store.storeimages.cdn-apple.com/8756/as-images.apple.com/is/MQGH2?wid=532&hei=582&fmt=png-alpha&.v=1618617119000",
    name:"Cáp USB-C sang Lightning (2m)"
  },

  {
    url:"https://store.storeimages.cdn-apple.com/8756/as-images.apple.com/is/MK122_GEO_VN?wid=532&hei=582&fmt=png-alpha&.v=1694553976923",
    name:"Cáp Nối Dài Cho Bộ Tiếp Hợp Nguồn"
  },
 

]

const Accessory = () => {



  const navigationNextRef = useRef(null);
  const navigationPrevRef = useRef(null);

  
  return (
    <div className={`${styles.accessory} container`}>
      <h2 className={styles.accessoryTitle}>Phụ kiện đi kèm với Airpods </h2>
      <div className={styles.accessoryContent}>
      <Swiper
                slidesPerView={3}
                spaceBetween={80}
                modules={[ Navigation]}
                navigation={{
                  prevEl: navigationPrevRef.current,
                  nextEl: navigationNextRef.current,
                }}
                onBeforeInit={(swiper:any) => {
                  swiper.navigation.nextEl = navigationNextRef.current;
                  swiper.navigation.prevEl = navigationPrevRef.current;
                }}
                className={`${styles.listAccessory} accessoryMac`}
              >
                {
                  AccessoryMac.flatMap((value:any,index:number)=>{
                    return <SwiperSlide key={`slideItemAccessory_${index}`}>
                    <div className={styles.itemAccesory}>
                                <Image
                                  src={`${value.url}`}
                                  alt={`IT QUY NHƠN | ${value.name}`}
                                  width={0}
                                  height={0}
                                  sizes="100vw"
                                  style={{ width: '100%', height: 'auto' }} // optional
                                />
                                <div className={styles.name}>
                                    {value.name}
                                </div>
                                <div className={`${styles.link} item-center`}>
                                  <Link href ={'/'}>Tham Khảo Giá </Link>
                                </div>
                    </div>
                      
                  </SwiperSlide>
                  })
                }

              </Swiper>
              <div className={`${styles.prevArrow} ${styles.btnSwiperCustom} item-center `} ref={navigationPrevRef} ><FontAwesomeIcon icon={faAngleLeft} /></div>
              <div className={`${styles.nextArrow} ${styles.btnSwiperCustom} item-center`} ref={navigationNextRef} ><FontAwesomeIcon icon={faAngleRight} /></div>
      </div>
    </div>
  )
}

export default Accessory