import React from 'react'
import styles from './styles/product-category.module.scss'


const ProductCategory = () => {
  return (
    <div className={`${styles.productCategory} container`} >
      <h2 className={styles.titleSection}>Các dòng IPhone hiện đại</h2>
      <div className={`${styles.contentLayout} item-center`}>
          <div className={`${styles.products} w-80 item-btw`}>
              <div className={styles.itemProduct}>
                    <div className={`${styles.image} ${styles.pro15}`}>

                    </div>
                    <p className={styles.nameProduct}>IPhone 15 Pro </p>
                    <p className={styles.gen}>Thế hệ thứ 2 </p>
              </div>
              <div className={styles.itemProduct}>
                    <div className={`${styles.image} ${styles.ip15}`}>

                    </div>
                    <p className={styles.nameProduct}>IPhone 15</p>
                    <p className={styles.gen}>Thế hệ thứ 3 </p>
              </div>
              <div className={styles.itemProduct}>
                    <div className={`${styles.image} ${styles.ip14}`}>

                    </div>
                    <p className={styles.nameProduct}>IPhone 14</p>
                    <p className={styles.gen}>Thế hệ thứ 2 (USB-C) </p>
              </div>
              <div className={styles.itemProduct}>
                    <div className={`${styles.image} ${styles.ip13}`}>

                    </div>
                    <p className={styles.nameProduct}>IPhone 13</p>
                    <p className={styles.gen}>AirPods Max </p>
              </div>
              <div className={styles.itemProduct}>
                    <div className={`${styles.image} ${styles.ipSE}`}>

                    </div>
                    <p className={styles.nameProduct}>IPhone SE</p>
                    <p className={styles.gen}>AirPods Max </p>
              </div>
          </div>

      </div>
    </div>
  )
}

export default ProductCategory