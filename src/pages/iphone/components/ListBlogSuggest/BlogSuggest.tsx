import React from 'react'
import styles from './styles/blog-suggest.module.scss'
import Image from 'next/image'
import Link from 'next/link'

const BlogSuggest = () => {
  return (
    <div className={`${styles.blogSuggest} container `}>
        <h2 className={styles.blogSuggestTitle}>Một số chia sẻ hữu ích với IPhone của bạn</h2>
        <div className={styles.blogSuggestContent}>
            <div className={styles.itemBlog}>
                <div className={styles.shortContent}>
                    <h3 className={styles.title}>Một số tips giữ cho Pin bạn được tốt nhất</h3>
                    <p className={styles.content}>
                        Đối tượng học sinh sinh viên nên sử dụng nhưng loại macbook nào để phù hợp với nhu cầu học tập 
                    </p>
                    <div className={styles.readMore}><Link href="/">Đọc thêm </Link></div>
                </div>
                <div className={`${styles.imageContent} item-center` }>
                      <div className={styles.image}>
                                <Image
                                  src={`https://store.storeimages.cdn-apple.com/8756/as-images.apple.com/is/MK0U3_FV404?wid=1420&hei=930&fmt=png-alpha&.v=1706808234695`}
                                  alt={`IT QUY NHƠN | Bộ Tiếp Hợp Nhiều Cổng USB-C Digital AV`}
                                  width={0}
                                  height={0}
                                  sizes="100vw"
                                  objectFit='contain'
                                  style={{ width: '100%', height: 'auto' }} // optional
                                />
                      </div>     
                </div>
            </div>
            <div className={styles.itemBlog}>
                <div className={styles.shortContent}>
                    <h3 className={styles.title}>Mẹo tháo lắp kính cường lực IPhone </h3>
                    <p className={styles.content}>
                        Đối tượng học sinh sinh viên nên sử dụng nhưng loại macbook nào để phù hợp với nhu cầu học tập 
                    </p>
                    <div className={styles.readMore}><Link href="/">Đọc thêm </Link></div>
                </div>
                <div className={`${styles.imageContent} item-center` }>
                      <div className={styles.image}>
                                <Image
                                  src={`https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRTItKdgkfNTWl38L5qL1VZaeYK3ilgxGmj4MtEteskVffFWayELu3a78MmIwLBPrkAlaM&usqp=CAU`}
                                  alt={`IT QUY NHƠN | Bộ Tiếp Hợp Nhiều Cổng USB-C Digital AV`}
                                  width={0}
                                  height={0}
                                  sizes="100vw"
                                  objectFit='contain'
                                  style={{ width: '100%', height: 'auto' }} // optional
                                />
                      </div>     
                </div>
            </div>
            <div className={styles.itemBlog}>
                <div className={styles.shortContent}>
                    <h3 className={styles.title}>Hiểu về máy Lock và Unlock khác nhau như thế nào  </h3>
                    <p className={styles.content}>
                        Đối tượng học sinh sinh viên nên sử dụng nhưng loại macbook nào để phù hợp với nhu cầu học tập 
                    </p>
                    <div className={styles.readMore}><Link href="/">Đọc thêm </Link></div>
                </div>
                <div className={`${styles.imageContent} item-center` }>
                      <div className={styles.image}>
                                <Image
                                  src={`https://store.storeimages.cdn-apple.com/8756/as-images.apple.com/is/MUF82?wid=532&hei=582&fmt=png-alpha&.v=1590526633000`}
                                  alt={`IT QUY NHƠN | Bộ Tiếp Hợp Nhiều Cổng USB-C Digital AV`}
                                  width={0}
                                  height={0}
                                  sizes="100vw"
                                  // objectFit='contain'
                                  style={{ width: '100%', height: 'auto' }} // optional
                                />
                      </div>     
                </div>
            </div>
        </div>
    </div>
  )
}

export default BlogSuggest