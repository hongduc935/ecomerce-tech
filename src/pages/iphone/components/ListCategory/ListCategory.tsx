import React from 'react'
import styles from './styles/list-category.module.scss'


const ListCategory = () => {
  return (
    <div className={`${styles.listCategory} item-center w-100`}>
          <div className={`${styles.listItem} item-center w-70 `}>
                <ul className={`${styles.menu} item-btw`}>
                    <li className={`${styles.item} `}>
                        <a className={`${styles.link}`} href="/vn/macbook-air/" data-analytics-title="macbook air">
                          <figure className={`${styles.icon} ${styles.ip15Pro}`} ></figure>
                          <p className={`${styles.label}`} role="text">IPhone 15 Pro </p>
                        </a>
                    </li>
                    <li className={`${styles.item} `}>
                        <a className={`${styles.link}`} href="/vn/macbook-air/" data-analytics-title="macbook air">
                          <figure className={`${styles.icon} ${styles.ip15Pro}`} ></figure>
                          <p className={`${styles.label}`} role="text">IPhone 15 </p>
                        </a>
                    </li>
                    <li className={`${styles.item} `}>
                        <a className={`${styles.link}`} href="/vn/macbook-air/" data-analytics-title="macbook air">
                          <figure className={`${styles.icon} ${styles.ip14}`} ></figure>
                          <p className={`${styles.label}`} role="text">IPhone 14</p>
                        </a>
                    </li>
                    <li className={`${styles.item} `}>
                        <a className={`${styles.link}`} href="/vn/macbook-air/" data-analytics-title="macbook air">
                          <figure className={`${styles.icon} ${styles.ip13}`} ></figure>
                          <p className={`${styles.label}`} role="text">IPhone 13</p>
                        </a>
                    </li>
                    <li className={`${styles.item} `}>
                        <a className={`${styles.link}`} href="/vn/macbook-air/" data-analytics-title="macbook air">
                          <figure className={`${styles.icon} ${styles.ipSE}`} ></figure>
                          <p className={`${styles.label}`} role="text">IPhone SE</p>
                        </a>
                    </li>
                    
                    <li className={`${styles.item} `}>
                        <a className={`${styles.link}`} href="/vn/macbook-air/" data-analytics-title="macbook air">
                          <figure className={`${styles.icon} ${styles.ios}`} ></figure>
                          <p className={`${styles.label}`} role="text">IOS 17</p>
                        </a>
                    </li>
                    <li className={`${styles.item} `}>
                        <a className={`${styles.link}`} href="/vn/macbook-air/" data-analytics-title="macbook air">
                          <figure className={`${styles.icon} ${styles.airTag}`} ></figure>
                          <p className={`${styles.label}`} role="text">AirTag</p>
                        </a>
                    </li>
                    
                </ul>

          </div>
    </div>
  )
}

export default ListCategory
