import React from 'react'
// import styles from './styles/iphone.module.scss'
import ComponentPage from '@/components/ComponentPage/ComponentPage'
import { Meta } from '@/layout/Meta'
import ProductCategory from './components/ProductCategory/ProductCategory';
import ListProduct from './components/ListProduct/ListProduct';
import Accessory from './components/Accessory/Accessory';
import AboutApplication from './components/AboutApplication/AboutApplication';
import BlogSuggest from './components/ListBlogSuggest/BlogSuggest';
import ListCategory from './components/ListCategory/ListCategory';

const IPHONE = {
  site_name: 'Chợ IPhone giá rẻ',
  title: 'IT QUY NHƠN | Mua Bán Iphone Cũ Chính Hãng Giá Rẻ',
  description: 'Mua bán iPhone cũ chính hãng tại IT QUY NHƠN - Cam kết chất lượng, giá tốt, bảo hành uy tín. Chuyên cung cấp các dòng iPhone cũ từ iPhone 7, 8 đến iPhone X, XS, 11, 12, 13, 14, 15. Liên hệ ngay để nhận ưu đãi hấp dẫn!',
  locale: 'en',
};

const IPhone = () => {
  return (  
    <ComponentPage isHiddenFooter={true} isHiddenHeader={true}>
      <Meta title={IPHONE.title} description={IPHONE.description} />
      <ListCategory/>
        <ProductCategory/>
        <ListProduct/>
        <Accessory/>
        <BlogSuggest/>
        <AboutApplication/>

    </ComponentPage>
  )
}

export default IPhone
