import React from 'react'
import styles from './styles/new.module.scss'
import { Swiper, SwiperSlide } from 'swiper/react';
import { Pagination } from 'swiper/modules';
// Import Swiper styles
import 'swiper/css';
import 'swiper/css/pagination';
import Link from 'next/link';
// import Link from 'next/link';
const New = () => {
  return (
    <div className={`${styles.new} container`}>
      <div className={`${styles.lineContent} item-btw` }>
        <h2 className={styles.titleSection}>Tin đăng mới nhất </h2>
        <p className={styles.cta}><Link href={"/mac"} target='_blank'> <em>Xem thêm mặt hàng</em></Link> </p>
      </div>
      
      <div className={styles.listNew}>
      <Swiper
        slidesPerView={1}
        spaceBetween={10}
        pagination={{
          clickable: true,
        }}
        breakpoints={{
          640: {
            slidesPerView: 2,
            spaceBetween: 20,
          },
          768: {
            slidesPerView: 4,
            spaceBetween: 40,
          },
          1024: {
            slidesPerView: 5,
            spaceBetween: 50,
          },
        }}
        modules={[Pagination]}
        className={`${styles.newSwiper}`}
      >
        <SwiperSlide>
          <div className={styles.product}>
              <div className={styles.images}>
                  <div className={`${styles.image} `} style={{backgroundImage:`url("https://genk.mediacdn.vn/139269124445442048/2023/3/6/photo-1678096201160-16780962014221791750585-1678097088540-16780970891802110218923.jpeg")`}}>

                  </div>
              </div>
              <div className={styles.shortContent}>
                    <p className={styles.content}>
                        Macbook Pro M1 13in , 16GB , 256GB 
                    </p>
              </div>
              <div className={styles.info}>
                <div className={styles.badge}/> 
                <div className={styles.badgeNew}>
                  Mới nhất
                </div>
              </div>
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className={styles.product}>
              <div className={styles.images}>
                  <div className={`${styles.image} `} style={{backgroundImage:`url("https://genk.mediacdn.vn/139269124445442048/2023/3/6/photo-1678096201160-16780962014221791750585-1678097088540-16780970891802110218923.jpeg")`}}>

                  </div>
              </div>
              <div className={styles.shortContent}>
                    <p className={styles.content}>
                        Macbook Pro M1 13in , 16GB , 256GB 
                    </p>
              </div>
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className={styles.product}>
              <div className={styles.images}>
                  <div className={`${styles.image} `} style={{backgroundImage:`url("https://genk.mediacdn.vn/139269124445442048/2023/3/6/photo-1678096201160-16780962014221791750585-1678097088540-16780970891802110218923.jpeg")`}}>

                  </div>
              </div>
              <div className={styles.shortContent}>
                    <p className={styles.content}>
                        Macbook Pro M1 13in , 16GB , 256GB 
                    </p>
              </div>
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className={styles.product}>
              <div className={styles.images}>
                  <div className={`${styles.image} `} style={{backgroundImage:`url("https://genk.mediacdn.vn/139269124445442048/2023/3/6/photo-1678096201160-16780962014221791750585-1678097088540-16780970891802110218923.jpeg")`}}>

                  </div>
              </div>
              <div className={styles.shortContent}>
                    <p className={styles.content}>
                        Macbook Pro M1 13in , 16GB , 256GB 
                    </p>
              </div>
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className={styles.product}>
              <div className={styles.images}>
                  <div className={`${styles.image} `} style={{backgroundImage:`url("https://genk.mediacdn.vn/139269124445442048/2023/3/6/photo-1678096201160-16780962014221791750585-1678097088540-16780970891802110218923.jpeg")`}}>

                  </div>
              </div>
              <div className={styles.shortContent}>
                    <p className={styles.content}>
                        Macbook Pro M1 13in , 16GB , 256GB 
                    </p>
              </div>
          </div>
        </SwiperSlide>
      </Swiper>
      </div>
      
    </div>
  )
}

export default New