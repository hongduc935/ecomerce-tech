import React from 'react'
import styles from './styles/category.module.scss'
import Link from 'next/link'
import Image from 'next/image'

const Category = () => {
  return (
    <div className={`${styles.category} w-100 item-center`}>
      <ul className={`${styles.menu} w-90 item-btw`}>
          <li className={styles.itemCategory}>
                <div className={styles.image}>
                          <Image
                              src={'/assets/home/mac-nav-202310.webp'}
                              alt='Lap top '
                              width={0}
                              height={0}
                              loading="lazy" 
                              sizes="100vw"
                              style={{ width: '100%', height: 'auto' }} // optional
                              />
                </div>
                <div className={`${styles.nameItem} item-center`}>
                  <Link href={"/"}>
                        <span>Mac</span> 
                  </Link>
                </div>
                
          </li>
          <li className={styles.itemCategory}>
                <div className={styles.image}>
                          <Image
                              src={'/assets/home/iphone-nav-202309.webp'}
                              alt='Lap top '
                              width={0}
                              height={0}
                              loading="lazy"
                              sizes="100vw"
                              style={{ width: '100%', height: 'auto' }} // optional
                              />
                </div>
                <div className={`${styles.nameItem} item-center`}>
                  <Link href={"/"}>
                        <span>Phone</span> 
                  </Link>
                </div>
                
          </li>
          <li className={styles.itemCategory}>
                <div className={styles.image}>
                          <Image
                              src={'/assets/home/ipad-nav-202405.webp'}
                              alt='Lap top '
                              width={0}
                              height={0}
                              loading="lazy"
                              sizes="100vw"
                              style={{ width: '100%', height: 'auto' }} // optional
                              />
                </div>
                <div className={`${styles.nameItem} item-center`}>
                  <Link href={"/"}>
                        <span>Ipad</span> 
                  </Link>
                </div>
                
          </li>
          <li className={styles.itemCategory}>
                <div className={styles.image}>
                          <Image
                              src={'/assets/home/watch-nav-202309_GEO_VN.webp'}
                              alt='Lap top '
                              width={0}
                              height={0}
                              // loading="lazy"
                              // // placeholder='blur'
                              // // blurDataURL={'/assets/home/watch-nav-202309_GEO_VN.webp'}
                              sizes="100vw"
                              style={{ width: '100%', height: 'auto' }} // optional
                              />
                </div>
                <div className={`${styles.nameItem} item-center`}>
                  <Link href={"/"}>
                        <span>Apple Watch</span> 
                  </Link>
                </div>
                
          </li>
          <li className={styles.itemCategory}>
                <div className={styles.image}>
                          <Image
                              src={'/assets/home/airpods-nav-202209.webp'}
                              alt='Lap top '
                              width={0}
                              height={0}
                              // loading="lazy"
                              sizes="100vw"
                              style={{ width: '100%', height: 'auto' }} // optional
                              />
                </div>
                <div className={`${styles.nameItem} item-center`}>
                  <Link href={"/"}>
                        <span>AirPods</span> 
                  </Link>
                </div>
                
          </li>
          <li className={styles.itemCategory}>
                <div className={styles.image}>
                          <Image
                              src={'/assets/home/airtags-nav-202108.webp'}
                              alt='Lap top '
                              width={0}
                              height={0}
                              // loading="lazy"
                              sizes="100vw"
                              style={{ width: '100%', height: 'auto' }} // optional
                              />
                </div>
                <div className={`${styles.nameItem} item-center`}>
                  <Link href={"/"}>
                        <span>AirTag</span> 
                  </Link>
                </div>
                
          </li>
          <li className={styles.itemCategory}>
                <div className={styles.image}>
                          <Image
                              src={'/assets/home/appletv-nav-202210.webp'}
                              alt='Lap top '
                              width={0}
                              height={0}
                              // loading="lazy"
                              sizes="100vw"
                              style={{ width: '100%', height: 'auto' }} // optional
                              />
                </div>
                <div className={`${styles.nameItem} item-center`}>
                  <Link href={"/"}>
                        <span>Mac Studio</span> 
                  </Link>
                </div>
                
          </li>
          <li className={styles.itemCategory}>
                <div className={styles.image}>
                          <Image
                              src={'/assets/home/accessories-nav-202403.webp'}
                              alt='Lap top '
                              width={0}
                              height={0}
                              // loading="lazy"
                              sizes="100vw"
                              style={{ width: '100%', height: 'auto' }} // optional
                              />
                </div>
                <div className={`${styles.nameItem} item-center`}>
                  <Link href={"/"}>
                        <span>Iphone</span> 
                  </Link>
                </div>
                
          </li>
      </ul>

    </div>
  )
}

export default Category