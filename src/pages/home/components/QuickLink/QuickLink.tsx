import React from 'react'
import styles from './styles/quicklink.module.scss'


const QuickLink = () => {
  return (
    <div className={`${styles.quicklink} container`}>
        <h2 className={styles.title}>Liên kết nhanh</h2>    
        <div className={styles.listTag}>
            <div className={styles.tag}>Mac</div><div className={styles.divider}/>
            <div className={`${styles.tag} ${styles.tagLeft} ` }>Apple</div> <div className={styles.divider}/>
            <div className={`${styles.tag} ${styles.tagLeft} ` }>AirPods</div><div className={styles.divider}/>
            <div className={`${styles.tag} ${styles.tagLeft} ` }>Apple Watch </div><div className={styles.divider}/>
            <div className={`${styles.tag} ${styles.tagLeft} ` }>Blogs</div><div className={styles.divider}/>
            <div className={`${styles.tag} ${styles.tagLeft} ` }>Support</div>
        </div>  
    </div>
  )
}

export default QuickLink
