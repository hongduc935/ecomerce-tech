import React from 'react'
import styles from './styles/support.module.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleQuestion, faCopyright, faHeadphones } from '@fortawesome/free-solid-svg-icons'
import Image from 'next/image'
import FadeIn from '@/components/Animations/FadeIn' 


const Support = () => {
  return (
    <section className={`${styles.support} container`}>
      <h2 className={styles.supportTitle}>Đội ngũ tư vấn hỗ trợ </h2>
      {/* <div className={`${styles.supportInfo} `}>
          <div className={styles.itemSupport}>
                <div className={`${styles.iconNumber} item-center `}>
                    <span className={`${styles.number} item-center`}>
                      <FontAwesomeIcon icon={faCircleQuestion} />
                    </span>
                </div>
                <div className={styles.sologan}>
                  Giàu kinh nghiệm
                </div>
          </div>
          <div className={styles.itemSupport}>
              
                <div className={`${styles.iconNumber} item-center`}>
                    <span className={`${styles.number} item-center`}>
                      <FontAwesomeIcon icon={faCopyright} />
                    </span>
                </div>
                <div className={styles.sologan}> Kiểm định rõ ràng </div>
          </div>
          <div className={styles.itemSupport}>
             
                <div className={`${styles.iconNumber} item-center`}>
                    <span className={`${styles.number} item-center`}>
                    <FontAwesomeIcon icon={faHeadphones} />
                    </span>
                </div>
                <div className={styles.sologan}> Hỗ trợ 24/7</div>
          </div>
      </div> */}

      <FadeIn>
        <div className={`${styles.itemSupportSec} item-btw`}>
            <div className={styles.image}>
                <Image
                      src={`https://www.sharp.nl/sites/default/files/styles/social_media_share/public/2021-10/it-helpdesk-2560x800.jpg?h=38522d18&itok=BYKDLnN2`}
                      alt={`Test`}
                      width={0}
                      height={0}
                      sizes="100vw"
                      style={{ width: '100%', height: 'auto' }} 
                      objectFit="contain"
                      objectPosition="center" 
                />
            </div>
            <div className={styles.content}>
                <h3 className={styles.title}>Kinh nghiệm </h3>
                <div className={styles.underline}></div>
                <p className={styles.slogan}> Kinh Nghiệm Đội Ngũ IT Tại IT Quy Nhơn - Chuyên Nghiệp & Tận Tâm</p>
                <p className={styles.desSec}>
                  Đội ngũ IT tại IT Quy Nhơn sở hữu nhiều năm kinh nghiệm trong sửa chữa, bảo trì và nâng cấp máy tính, cam kết mang đến giải pháp nhanh chóng và chất lượng cao. Với kiến thức chuyên sâu cùng sự cập nhật liên tục công nghệ mới, chúng tôi luôn đảm bảo thiết bị của bạn hoạt động hiệu quả và an toàn. IT Quy Nhơn – Địa chỉ uy tín để bạn an tâm gửi gắm thiết bị của mình!
                </p>
            </div>
        </div>
      </FadeIn>
      <FadeIn>
        <div className={`${styles.itemSupportSec} item-btw`}>
          
          <div className={styles.content}>
              <h3 className={styles.title}>Kiểm định rõ ràng </h3>
              <div className={styles.underline}></div>
              <p className={styles.slogan}> Kinh Nghiệm Đội Ngũ IT Tại IT Quy Nhơn - Chuyên Nghiệp & Tận Tâm</p>
              <p className={styles.desSec}>
              Đội ngũ IT tại IT Quy Nhơn có nhiều năm kinh nghiệm trong sửa chữa, bảo trì và nâng cấp máy tính, cam kết mang đến giải pháp nhanh chóng và chất lượng cao. Chúng tôi thực hiện kiểm định rõ ràng trước và sau khi sửa chữa, giúp khách hàng nắm rõ tình trạng thiết bị và các hạng mục đã xử lý. Với kiến thức chuyên sâu và sự cập nhật liên tục về công nghệ, IT Quy Nhơn là nơi đáng tin cậy để bạn an tâm gửi gắm thiết bị của mình!
              </p>
          </div>

          <div className={styles.image}>
              <Image
                    src={`https://cdnphoto.dantri.com.vn/he-JW3gx2nuC93mwMR5lzugzqXw=/2024/08/22/kiem-tra-laptop-1724345808366.jpg`}
                    alt={`Test`}
                    width={0}
                    height={0}
                    sizes="100vw"
                    style={{ width: '100%', height: 'auto' }} 
                    objectFit="contain"
                    objectPosition="center" 
              />
          </div>
      </div>
      </FadeIn>
      <FadeIn>
        <div className={`${styles.itemSupportSec} item-btw`}>
            <div className={styles.image}>
                <Image
                      src={`https://t3.ftcdn.net/jpg/04/03/21/94/360_F_403219423_xh099IbwZ5NC4ivQCURSDo0c6wAkaMpN.jpg`}
                      alt={`Test`}
                      width={0}
                      height={0}
                      sizes="100vw"
                      style={{ width: '100%', height: 'auto' }} 
                      objectFit="contain"
                      objectPosition="center" 
                />
            </div>
            <div className={styles.content}>
                <h3 className={styles.title}>Support 24/7</h3>
                <div className={styles.underline}></div>
                <p className={styles.slogan}> Kinh Nghiệm Đội Ngũ IT Tại IT Quy Nhơn - Chuyên Nghiệp & Tận Tâm</p>
                <p className={styles.desSec}>
                Đội ngũ IT tại IT Quy Nhơn có nhiều năm kinh nghiệm trong sửa chữa, bảo trì và nâng cấp máy tính, cam kết mang đến giải pháp nhanh chóng, chất lượng cao. Chúng tôi thực hiện kiểm định rõ ràng trước và sau khi sửa chữa, giúp khách hàng nắm rõ tình trạng thiết bị. Đặc biệt, với dịch vụ hỗ trợ 24/7, IT Quy Nhơn luôn sẵn sàng giải đáp mọi thắc mắc và xử lý vấn đề của khách hàng bất cứ lúc nào. IT Quy Nhơn - Địa chỉ tin cậy để bạn yên tâm gửi gắm thiết bị của mình!
                </p>
            </div>
        </div>
      </FadeIn>
      
    </section>
  )
}

export default Support