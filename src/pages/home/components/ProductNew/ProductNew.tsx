
import React from 'react'
import styles from './styles/product-new.module.scss'
import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import 'swiper/css/free-mode'
import 'swiper/css/pagination'
import { FreeMode, Pagination } from 'swiper/modules'
import { faArrowLeft, faArrowRight } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const ProductNew = () => {
  const navigationPrevRef = React.useRef(null)
  const navigationNextRef = React.useRef(null)
  return (
    <div className={`${styles.productNew} w-100`}> 
      <h2 className={`${styles.titleProductNew} container`}>Các sản phẩm mới nhất từ Apple</h2>
      <div className={`${styles.slideProduct} container`}>
              <Swiper
                // slidesPerView={3}
                spaceBetween={80}
                freeMode={true}
                pagination={false}
                modules={[FreeMode, Pagination]}
                navigation={{
                  prevEl: navigationPrevRef.current,
                  nextEl: navigationNextRef.current,
                }}
               onBeforeInit={(swiper:any) => {
                    swiper.params.navigation.prevEl = navigationPrevRef.current;
                    swiper.params.navigation.nextEl = navigationNextRef.current;
               }}
               breakpoints={{
                  640: {
                      slidesPerView: 2, // Hiển thị 2 item cho màn hình từ 640px
                  },
                  768: {
                      slidesPerView: 2, // Hiển thị 3 item cho màn hình từ 768px
                  },
                  1024: {
                      slidesPerView: 3, // Hiển thị 4 item cho màn hình từ 1024px
                  },
              }}
                className={`${styles.listProduct} productNew`}

              >
                <SwiperSlide>
                      <div className={`${styles.cartProduct} container ${styles.ipad}`}>
                          <h3 className={styles.nameProduct}> IPAD PRO </h3>
                          <p className={styles.sologan}> MỎNG XUẤT CHÚNG </p>
                          <p className={styles.price}>New Seal 19.000.000đ </p>
                      </div>
                </SwiperSlide>
                <SwiperSlide>
                      <div className={`${styles.cartProduct} container ${styles.iphone}`}>
                          <h3 className={styles.nameProduct}> IPHONE </h3>
                          <p className={styles.sologan}> TITAN </p>
                          <p className={styles.price}>New Seal 19.000.000đ </p>
                      </div>
                </SwiperSlide>
                <SwiperSlide>
                      <div className={`${styles.cartProduct} container ${styles.ipadair}`}>
                          <h3 className={styles.nameProduct}> IPAD AIR </h3>
                          <p className={styles.sologan}> MỚI MẺ </p>
                          <p className={styles.price}>New Seal 19.000.000đ </p>
                      </div>
                </SwiperSlide>
                <SwiperSlide>
                      <div className={`${styles.cartProduct} container ${styles.pencil}`}>
                          <h3 className={styles.nameProduct}> IPAD PRO </h3>
                          <p className={styles.sologan}> ĐƯỚC THIẾT KẾ ĐẶC BIỆT CHO SÁNG TẠO KHÔNG GIỚI HẠN  </p>
                          <p className={styles.price}>New Seal 19.000.000đ </p>
                      </div>
                </SwiperSlide>
                <SwiperSlide>
                      <div className={`${styles.cartProduct} container ${styles.watch}`}>
                          <h3 className={styles.nameProduct}> APPLE WATCH SERIES 9 </h3>
                          <p className={styles.sologan}> ĐA CHỨC NĂNG HƠN </p>
                          <p className={styles.price}>New Seal 19.000.000đ </p>
                      </div>
                </SwiperSlide>
                <SwiperSlide>
                      <div className={`${styles.cartProduct} container ${styles.macbook}`}>
                          <h3 className={styles.nameProduct}> MACBOOK PRO </h3>
                          <p className={styles.sologan}> MỎNG NHẸ TIỆN LỢI</p>
                          <p className={styles.price}>New Seal 19.000.000đ </p>
                      </div>
                </SwiperSlide>
               
                
              </Swiper>
      </div>
      <div className={`${styles.customNavProduct} item-end container`}>
        <div className={`${styles.pre} ${styles.customNav} item-center`} ref={navigationPrevRef} >
          <FontAwesomeIcon icon={faArrowLeft} />
        </div>
        <div className={`${styles.next} ${styles.customNav} item-center`}  ref={navigationNextRef} >
          <FontAwesomeIcon icon={faArrowRight} />
        </div>
      </div>
    </div>
  )
}

export default ProductNew