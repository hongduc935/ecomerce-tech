import React from 'react'
import styles from './styles/banner.module.scss'
import { motion } from 'framer-motion';

const fadeInUp = {
  hidden: { opacity: 0, y: 50 },
  visible: { opacity: 1, y: 0 },
};


const Banner = () => {
  return (
    <motion.div 
      className={`${styles.banner} w-100 item-btw`}
      initial="hidden"
      whileInView="visible"
      viewport={{ once: true }}
      transition={{ duration: 0.6 }}
      variants={fadeInUp}
    >
      <div className={`${styles.layoutBanner} w-50 container`}>
        <h1 className={styles.titleBanner}>IT QUY NHƠN - <span className={styles.txtColor}>Bán những sản phẩm của Apple giá rẻ hơn chất lượng.</span></h1>
      </div>
      <div className={`${styles.layoutBanner} w-50 container item-center`}>
          <div className={`${styles.support}`}>
              <div className={styles.image}>
                  
              </div>
              <div className={styles.pbx}>
                <p className={styles.installment}>Tư vấn chính sách trả góp ? </p>
                <div className={`${styles.support}`}>
                    <div className={styles.image}/>
                    <div className={styles.txt}>&nbsp; Hỏi chuyên gia tư vấn . </div>
                </div>
                <button className={styles.contact}>Liên Hệ Ngay</button>
              </div>
          </div> 
      </div>
    </motion.div>
  )
} 

export default Banner