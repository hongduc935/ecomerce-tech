import React, { useState } from 'react'
import styles from './styles/become-partner.module.scss'
import Link from 'next/link'
import { faArrowRight, faChevronDown, faChevronUp } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


const BecomePartner = () => {

  const [expandedItem, setExpandedItem] = useState<any>(null); // Trạng thái để lưu mục nào đang mở

  const toggleExpand = (index:number) => {
    // Kiểm tra xem mục đã được mở chưa, nếu có thì đóng, nếu chưa thì mở
    setExpandedItem(expandedItem === index ? null : index);
  };

  const items = [
    {
      title: 'Data khách hàng lớn',
      description: 'Hệ thống của IT Quy Nhơn thu hút hàng ngàn khách hàng mỗi ngày',
      image : 'dataClient'
    },
    {
      title: 'Quảng bá và bán hàng',
      description: 'Đội ngũ chuyên nghiệp sẽ hỗ trợ tối đa trong việc quảng bá sản phẩm và chăm sóc khách hàng.',
      image : 'sell'
    },
    {
      title: 'Quản lý thông minh',
      description: 'Quản lý kho hàng, theo dõi doanh thu, và cập nhật đơn hàng dễ dàng qua hệ thống hiện đại.',
      image : 'smart'
    },
    {
      title: 'Chi phí hợp lý',
      description: 'Mô hình hợp tác tối ưu chi phí, giúp bạn tối đa hóa lợi nhuận.',
      image : 'reasonable'
    },
  ];
  return (
    <section className={`${styles.becomePartner} container`}>
      <h2 className={styles.titleSection}>Chương trình đối tác bán hàng</h2>
      <p className={styles.desSection}>Bạn đang tìm kiếm cơ hội mở rộng kinh doanh và tiếp cận hàng ngàn khách hàng tiềm năng? Hãy gia nhập hệ thống sinh thái của IT Quy Nhơn! Khi trở thành đối tác của chúng tôi, bạn sẽ được tận dụng nền tảng uy tín và hệ thống hỗ trợ toàn diện từ IT Quy Nhơn để dễ dàng quản lý sản phẩm, gia tăng doanh số và xây dựng thương hiệu của mình. </p>
      {/* <div className={`${styles.outStanding} item-btw`}>
          <div className={styles.item}>
              <h3 className={styles.titleItem}>Data khách hàng lớn</h3>
              <p className={styles.desItem}>Hệ thống của IT Quy Nhơn thu hút hàng ngàn khách hàng mỗi ngày</p>
          </div>
          <div className={styles.item}>
              <h3 className={styles.titleItem}>Quảng bá và bán hàng</h3>
              <p className={styles.desItem}>Đội ngũ chuyên nghiệp sẽ hỗ trợ tối đa trong việc quảng bá sản phẩm và chăm sóc khách hàng.</p>
          </div>
          <div className={styles.item}>
              <h3 className={styles.titleItem}>Quản lý thông minh</h3>
              <p className={styles.desItem}> Quản lý kho hàng, theo dõi doanh thu, và cập nhật đơn hàng dễ dàng qua hệ thống hiện đại.</p>
          </div>
          <div className={styles.item}>
              <h3 className={styles.titleItem}>Chi phí hợp lý</h3>
              <p className={styles.desItem}>Mô hình hợp tác tối ưu chi phí, giúp bạn tối đa hóa lợi nhuận.</p>
          </div>
      </div> */}
      <div className={`${styles.outStanding} item-btw`}>
      {items.map((item, index) => (
        <div key={index} className={styles.item}>
          <div className={styles.image}>
              <div className={`${styles.img} ${styles[item.image]}`}></div>
          </div>
          <h3 className={styles.titleItem}>{item.title}</h3>
          
          {/* Kiểm tra xem mục có đang mở không để hiển thị mô tả */}
          {expandedItem === index && (
            <p className={`${styles.desItem} ${expandedItem === index ? styles.expanded : ''}`}>{item.description}</p>
          )}
          <div className={`${styles.header} item-center`} onClick={() => toggleExpand(index)}>
            
            {expandedItem === index ? <FontAwesomeIcon icon={faChevronUp} /> : <FontAwesomeIcon icon={faChevronDown} />}
          </div>
        </div>
      ))}
    </div>
      <h2 className={styles.title}>Trở thành đối tác với IT QUY NHƠN !</h2>
      <div className={`${styles.btn} item-center`}><Link href={"/partner"}><button className={styles.buttonBecomePartner}> Đăng kí đối tác  &nbsp;<FontAwesomeIcon icon={faArrowRight} /> </button></Link></div>
    </section>
  )
}

export default BecomePartner
