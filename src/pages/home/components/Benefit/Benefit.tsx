import React from 'react'
import styles from './styles/benefit.module.scss'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; // Import the FontAwesomeIcon component
import { faSearch ,faTag,faMedal,faNewspaper,faCreditCard,faHeadset,faMoneyCheck,faUserCheck} from "@fortawesome/free-solid-svg-icons"; // import the icons you need

const Benefit = () => {
  return (
    <div className={`${styles.benefit} container`}>
      <h2 className={styles.benefitTitle}>8 Yếu tố tạo nên uy tín IT QUY NHƠN </h2>
      <div className={styles.benefitContent}>
          <div className={styles.listBenefit}>
              <div className={styles.benefitItem}>
                  <h3 className={styles.titleItem}> <FontAwesomeIcon style={{fontSize:"18px"}} icon={faSearch}></FontAwesomeIcon> &nbsp; BẢO HÀNH</h3>
                  <p className={styles.desItem}>Gói bảo hành với chi phí thấp nhất thị trường, bảo đảm sự an tâm cho khách hàng.</p> 
              </div>
              <div className={styles.benefitItem}>
                  <h3 className={styles.titleItem}><FontAwesomeIcon icon={faTag} />&nbsp; GIÁ TỐT</h3>
                  <p className={styles.desItem}>Giá cả cạnh tranh nhất trên thị trường, mang lại giá trị tối đa cho người tiêu dùng.</p> 
              </div>
              <div className={styles.benefitItem}>
                  <h3 className={styles.titleItem}><FontAwesomeIcon icon={faMedal} /> &nbsp; CHẤT LƯỢNG</h3>
                  <p className={styles.desItem}>Sản phẩm chất lượng cao, kiểm định nghiêm ngặt, đảm bảo sự hài lòng của khách hàng.</p> 
              </div>
              <div className={styles.benefitItem}>
                  <h3 className={styles.titleItem}><FontAwesomeIcon icon={faNewspaper} /> &nbsp; NEW</h3>
                  <p className={styles.desItem}>Sản phẩm và dịch vụ mới nhất, luôn cập nhật xu hướng hiện đại.</p> 
              </div>
          </div>   
          <div className={styles.listBenefit}>
              <div className={styles.benefitItem}>
                  <h3 className={styles.titleItem}><FontAwesomeIcon icon={faCreditCard} /> &nbsp; EARNING</h3>
                  <p className={styles.desItem}>Tăng thu nhập với các chương trình hấp dẫn và cơ hội kinh doanh vượt trội.</p> 
              </div>
              <div className={styles.benefitItem}>
                  <h3 className={styles.titleItem}><FontAwesomeIcon icon={faHeadset} /> &nbsp;TƯ VẤN</h3>
                  <p className={styles.desItem}>Hỗ trợ tư vấn chuyên nghiệp, tận tâm, giải đáp mọi thắc mắc của khách hàng.</p> 
              </div>
              <div className={styles.benefitItem}>
                  <h3 className={styles.titleItem}><FontAwesomeIcon icon={faMoneyCheck} /> &nbsp;THANH TOÁN</h3>
                  <p className={styles.desItem}>Hệ thống thanh toán nhanh chóng, an toàn, tiện lợi cho mọi giao dịch.</p> 
              </div>
              <div className={styles.benefitItem}>
                  <h3 className={styles.titleItem}><FontAwesomeIcon icon={faUserCheck} /> &nbsp; KIỂM ĐỊNH</h3>
                  <p className={styles.desItem}>Sản phẩm được kiểm định chất lượng chặt chẽ, đảm bảo uy tín và an toàn.</p> 
              </div>
          </div>                  
      </div>
    </div>
  )
}

export default Benefit
