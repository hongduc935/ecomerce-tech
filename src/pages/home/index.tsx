import ComponentPage from '@/components/ComponentPage/ComponentPage'
import { Meta } from '@/layout/Meta'
// import { MACBOOK } from '@/utils/AppConfig'
import { NextPage } from 'next'
import React from 'react'
import Banner from './components/Banner/Banner'
import Category from './components/Category/Category'
import Support from './components/Support/Support'
import ProductNew from './components/ProductNew/ProductNew'
import Benefit from './components/Benefit/Benefit'
import BecomePartner from './components/BecomePartner/BecomePartner'
import QuickLink from './components/QuickLink/QuickLink'
// import New from './components/New/New'


interface ISEO {
  title:string 
  description :string
} 

const SEO_HOMEPAGE: ISEO = {
  title:"IT QUY NHƠN | Mua Bán Sản Phẩm Apple Chính Hãng Giá Tốt",
  description:"IT QUY NHƠN chuyên cung cấp các sản phẩm Apple chính hãng như iPhone, iPad, MacBook, Apple Watch, AirPods với giá cạnh tranh. Mua sắm ngay để nhận nhiều ưu đãi hấp dẫn và dịch vụ tư vấn chuyên nghiệp."
}


const HomePage: NextPage = () => {
  return (
    <ComponentPage isHiddenFooter={true} isHiddenHeader={true}>
    <Meta title={SEO_HOMEPAGE.title} description={SEO_HOMEPAGE.description} />
      <Banner/>
      <Category/> 
      <ProductNew/>
      {/* <New/> */}
      <Benefit/>
      <Support/>    
      <BecomePartner/>
      <QuickLink/>
    </ComponentPage>
  )
}

export default HomePage
