import { AuthProvider } from '@/contexts/AuthProvider'
import '../styles/global.scss'
import type { AppProps } from 'next/app'
import "@fortawesome/fontawesome-svg-core/styles.css" // import Font Awesome CSS
import { config } from "@fortawesome/fontawesome-svg-core"
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import 'swiper/css';
import 'swiper/css/free-mode';
import 'swiper/css/navigation';
import 'swiper/css/thumbs';

import { QueryClientProvider } from "@tanstack/react-query";
import { queryClient } from "../infra/react-query";
import { PageTransition } from '@/components/Animations/Transition'
config.autoAddCss = false; // Tell Font Awesome to skip adding the CSS automatically since it's being imported above

const MyApp = ({ Component, pageProps }: AppProps) => {
  return (
    <QueryClientProvider client={queryClient}>
        <AuthProvider>
        <PageTransition>
          <Component {...pageProps}/>
            <ToastContainer />
        </PageTransition>
        </AuthProvider> 
    </QueryClientProvider>
 )
}

export default MyApp;
