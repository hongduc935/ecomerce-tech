import { NextPage } from 'next'
import React from 'react'
import styles from './styles/404.module.scss'

const index :NextPage= () => {
  return (
    <div className={styles.notfound}>
        <div className={styles.content}>
            <svg viewBox="0 0 960 300">
              <symbol id="s-text">
                <text text-anchor="middle" x="50%" y="50%">404</text>
              </symbol>

              <g className = "g-ants">
                <use href="#s-text" className={styles.text}></use>
                <use href="#s-text" className={styles.text}></use>
                <use href="#s-text" className={styles.text}></use>
                <use href="#s-text" className={styles.text}></use>
                <use href="#s-text" className={styles.text}></use>
              </g>
            </svg>
                <h1 className={styles.titleNotFound}>Page Not Found</h1>
                <a className ={styles.link} href="#">Back to Home</a>
          </div>
    </div>
  )
}

export default index
