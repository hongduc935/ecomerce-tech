import { DataPage } from '@/@types/DataPage/index'
import { TYPE_PAGE } from '@/@types/TypePage/typePage'
import { GetServerSideProps } from 'next'
import React, { Suspense  } from 'react'
import ObjectSWPage from '@/helpers/ObjectSWPage'
import Loading from '@/components/Loading/Loading'

export interface DynamicRouteProps {
   dataPage: DataPage ;
   typePage: TYPE_PAGE;
}

const DynamicRoute = ({ 
   dataPage,
   typePage,
    // seo 
  }: DynamicRouteProps) => {
   
  const Pages = {
    [TYPE_PAGE.HOME]: React.lazy(() => import('../pages/home/index')),
    [TYPE_PAGE.ABOUT]: React.lazy(() => import("../pages/about/index")),
    [TYPE_PAGE.NOT_FOUND]: React.lazy(() => import("../pages/404/index")),
    [TYPE_PAGE.DASHBOARD]: React.lazy(() => import("../templates/dashboard/index")),
  };

  const Component = React.useMemo(() => {
    const _objectSWPage = new ObjectSWPage(Pages)
    return _objectSWPage.get(typePage)
  }, [typePage])

  return (
    <>
      <Suspense fallback={<Loading />}>
          <Component dataPage={dataPage}  />
      </Suspense>
    </>
  )
}



export const getServerSideProps: GetServerSideProps = async (context) => {


  const { query, res } = context

  console.log(res)

  const  slug  = query.slug ;

  let user = undefined

  let typePage:TYPE_PAGE  = TYPE_PAGE.NOT_FOUND;

  if (slug) {
    typePage = TYPE_PAGE[slug.toString().toUpperCase() as keyof typeof TYPE_PAGE] || TYPE_PAGE.NOT_FOUND;

    if(typePage ===  TYPE_PAGE.NOT_FOUND){
      const data ={
        typePage,
        dataPage:{}
      }
      return {
        props:data
      }
    }
    else{


    }

  }


  // BUSINESS AUTHENTICATION 


  // XÁC THỰC VỚI NHỮNG ROUTER PRIVATE CAO  

  // CẦN VERIFY SSR BẰNG CÁCH DÙNG TOKEN  REQUEST SERVER

  // XÁC THỰC VỚI NHƯNG ROUTER BẢO MẬT THẤP 

  // CHỈ CẦN CÓ TOKEN LÀ PASS

  // PUBLIC ROUTER THÌ VIẾT TRONG FOLDER PAGES VÀ KHÔNG CẦN BẢO MẬT 


  if (slug?.includes('profile')) { // NHỮNG ROUTER BẢO MẬT CAO 
    //  user = Auth.Authenticated(context) VERIFY VỚI BACKEND 
    user ="is Authen "
  }
  else{ // NHỮNG ROUTER BẢO MẬT THẤP 
    //user = Auth.UnAuthenticated(context)
    user ="is un Authen "
  }

  if (user !== undefined && typeof user === 'object') {// VERYFI SUCCESS TOKEN 

    //FETCH DATA PAGE 

  }
  else{// VERYFI SUCCESS TOKEN FAIL  CHO CÁC TRƯỜNG HỢP KHÔNG HỢP LỆ HOẶC HẾT HẠN
    // vể trang login 
    return {
      redirect: {
        destination: '/login',
        permanent: false,
      },
    };
  }


  await fetch(`https://jsonplaceholder.typicode.com/todos/1`).then((res:any)=>{
      console.log("RES:" + JSON.stringify(res))
   })
   .catch((error:any)=>{
    console.log(JSON.stringify(error))
   })

  const data ={
    typePage,
    dataPage:{}
    
  }
  return {
    props: {
      ...data,
      // dataPage: {}, // Dữ liệu trang
      // typePage: "dashboard", // Loại trang
      user: user, // Thông tin người dùng (nếu cần)
    },
  }
}
export default DynamicRoute