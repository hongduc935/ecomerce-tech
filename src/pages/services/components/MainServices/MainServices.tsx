import React from 'react'
import styles from './styles/main-service.module.scss'

const MainServices = () => {
  return (
    <section id="mainService" className={`${styles.mainServices} container`}>
      <h1 className={styles.titleService}>Dịch vụ chính tại IT QUY NHƠN</h1>
      <div className={styles.groupService}>
          <div className={styles.boxService}>
              <div className={styles.boxIcon}>
              <div className={`${styles.icons} ${styles.tool}`}></div>
              </div>
             
              <div className={styles.nameService}>
                <h2 className={styles.name}>Sửa chữa</h2>
              </div>
          </div>  
          <div className={`${styles.boxService}`}>
              <div className={styles.boxIcon}>
              <div className={`${styles.icons} ${styles.cart}`}></div>
              </div>
             
              <div className={styles.nameService}>
                <h2 className={styles.name}>Mua Bán</h2>
              </div>
          </div>  
          <div className={`${styles.boxService} `}>
              <div className={styles.boxIcon}>
              <div className={`${styles.icons} ${styles.tech}`}></div>
              </div>
             
              <div className={styles.nameService}>
                <h2 className={styles.name}>Review IT</h2>
              </div>
          </div>  
          <div className={`${styles.boxService} ${styles.ml}`}>
              <div className={styles.boxIcon}>
              <div className={`${styles.icons} ${styles.support}`}></div>
              </div>
             
              <div className={styles.nameService}>
                <h2 className={styles.name}>Tư Vấn</h2>
              </div>
          </div>  
          <div className={`${styles.boxService} ${styles.ml}`}>
              <div className={styles.boxIcon}>
              <div className={`${styles.icons} ${styles.seo}`}></div>
              </div>
             
              <div className={styles.nameService}>
                <h2 className={styles.name}>SEO Service</h2>
              </div>
          </div>  
          <div className={`${styles.boxService} ${styles.ml}`}>
              <div className={styles.boxIcon}>
              <div className={`${styles.icons} ${styles.web}`}></div>
              </div>
             
              <div className={styles.nameService}>
                <h2 className={styles.name}>Lập Trình</h2>
              </div>
          </div> 
          
      </div>
    </section>
  )
}

export default MainServices