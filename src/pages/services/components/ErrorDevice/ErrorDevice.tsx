import React from 'react'
import styles from './styles/error-device.module.scss'


const ErrorDevice = () => {
  return (
    <section id="errorDevice" className={`${styles.errorDeviceSection} container`}>
      <h2 className={styles.title}>Lỗi thiết bị Laptop</h2>
      <p className={styles.des}> 
        Đối với các dòng laptop hiện nay . Khách hàng đến với chúng tôi thường gặp một số vấn đề  phổ biến về laptop cho nên chúng tôi đã liệt kê những dịch vụ thường được khách hàng yêu cầu để chăm sóc cho chiếc máy tính . 
      </p>
      <div className={styles.groupDeviceLaptop}>
        {/* ErrorDevice */}
        <div className={`${styles.itemDevice}`}>
          <div className={`${styles.charger} ${styles.img}`}></div>
          <div className={styles.nameDevice}>
            Thay Sạc
          </div>
        </div>
        <div className={`${styles.itemDevice} ${styles.ml}`}>
          <div className={`${styles.battery} ${styles.img} `} />
          <div className={styles.nameDevice}>
            Thay Pin
          </div>
        </div>
        <div className={`${styles.itemDevice} ${styles.ml}`}> 
          <div className={`${styles.keyboard} ${styles.img}`} />
          <div className={styles.nameDevice}>
            Bàn Phím
          </div>
        </div>

        <div className={`${styles.itemDevice}  ${styles.ml}`}>
          <div className={`${styles.face} ${styles.img}`} />
          <div className={styles.nameDevice}>
            Vỏ Laptop
          </div>
        </div>
        <div className={`${styles.itemDevice}  ${styles.ml}`}>
          <div className={`${styles.charger} ${styles.img}`} />
          <div className={styles.nameDevice}>
            Vệ Sinh Laptop
          </div>
        </div>
        <div className={`${styles.itemDevice}  ${styles.ml}`}>
          <div className={`${styles.heatsink} ${styles.img}`} />
          <div className={styles.nameDevice}>
            Tản Nhiệt
          </div></div>
        <div className={`${styles.itemDevice}  ${styles.ml}`}>
          <div className={`${styles.main} ${styles.img}`} />
          <div className={styles.nameDevice}>
            Sửa Main
          </div>
        </div>
        <div className={`${styles.itemDevice}  ${styles.ml}`}>
        <div className={`${styles.speakers} ${styles.img}`} />
          <div className={styles.nameDevice}>
            Thay Loa
          </div>
        </div>
      </div>
      <div className={styles.groupDeviceLaptop}>
        
      </div>

    </section>
  )
}

export default ErrorDevice