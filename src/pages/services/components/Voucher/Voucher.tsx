import React from 'react'
import styles from './styles/voucher.module.scss'
import { faTag } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


const Voucher = () => {
  return (
    <section className={`${styles.voucher} container`}>
      <div className={styles.voucherLayout}>
        <div className={`${styles.voucherItem} ${styles.vcStudent}`}>
          <FontAwesomeIcon icon={faTag} /> Sinh viên tựu trường | -15%
        </div>
        <div className={`${styles.voucherItem} ${styles.vcAccount} ${styles.ml}`}>
          <FontAwesomeIcon icon={faTag} /> Đăng kí hệ thống | -30k
        </div>
        <div className={`${styles.voucherItem} ${styles.vcMember} ${styles.ml}`}>
          <FontAwesomeIcon icon={faTag} /> Member | -70k
        </div>
        <div className={`${styles.voucherItem} ${styles.vcBooking} ${styles.ml}`}>
          <FontAwesomeIcon icon={faTag} /> Đặt trước lịch | -10k
        </div>
        <div className={`${styles.voucherItem} ${styles.vcBooking} ${styles.ml}`}>
          Ưu đãi khác
        </div>
      </div>
      
      
    </section>
  )
}

export default Voucher