import React from 'react'
import styles from './styles/new-notice.module.scss'

const NewNotice = () => {
  return (
    <section id='new-notice' className={`${styles.newNotice} container`}>
      
      
      <div className={styles.notice}>
        <div className={styles.image}>
            <div className={styles.img}></div>
        </div>
        <div className={styles.content}>
          <h2 className={styles.title}>Thông báo giảm giá cho sinh viên tưu trường </h2>
          <p className={styles.details}>
  * Áp dụng giảm 30% đối với sinh viên năm nhất khi mua laptop trực tiếp tại của hàng.<a className={styles.btnGetNow} href='#'> Get Now</a>
          </p>
        </div>
        
      </div>
    </section>
  )
}

export default NewNotice