import React from 'react'
import styles from './styles/banner-service.module.scss'
import Image from 'next/image'
import { faHandshakeAngle, faPlus } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const BannerService = () => {
  return (
    <section className={`${styles.bannerService} container`}>
      <div className={styles.listBanner}>

            <div className={styles.bannerItem}>
              <Image
                src={'https://img.freepik.com/free-vector/digital-marketing-agency-isometric-vector-web-banner_33099-1176.jpg?semt=ais_hybrid'}
                alt='IT QUY NHƠN '
                width={0}
                height={0}
                loading="lazy"
                sizes="100vw"
                style={{ width: '100%', height: '100%' }} // optional
              />
            </div>
            <div className={styles.bannerItem}>
              <Image
                src={'https://img.freepik.com/free-vector/digital-marketing-agency-isometric-vector-web-banner_33099-1176.jpg?semt=ais_hybrid'}
                alt='IT QUY NHƠN '
                width={0}
                height={0}
                loading="lazy"
                sizes="100vw"
                style={{ width: '100%', height: '100%' }} // optional
              />
            </div>
      </div>
      <div className={styles.board}>
            <h2 className={styles.titleSp}><FontAwesomeIcon icon={faHandshakeAngle} /> &nbsp;Hỗ Trợ khách hàng</h2>
            <ul className={styles.menuBlog}>
              <li className={styles.item}>
              <span ><FontAwesomeIcon icon={faPlus} /></span> <p className={styles.text}>&nbsp; Dán keo tản nhiệt Laptop chỉ 10k </p>
              </li>
              <li className={styles.item}>
              <span ><FontAwesomeIcon icon={faPlus} /></span> <p className={styles.text}>&nbsp;Tư Vấn laptop Sinh Viên Miễn Phí</p>
              </li>
              <li className={styles.item}>
              <span><FontAwesomeIcon icon={faPlus} /> </span><p className={styles.text}>&nbsp;Review Ngành IT và định hướng cho sinh viên</p> 
              </li>
              <li className={styles.item}>
              <span><FontAwesomeIcon icon={faPlus} /> </span><p className={styles.text}>&nbsp;Cài Win</p> 
              </li>
              <li className={styles.item}>
              <span><FontAwesomeIcon icon={faPlus} /> </span><p className={styles.text}>&nbsp;Cài đặt phần mềm</p> 
              </li>
            </ul>
      </div>
      
    </section>
  )
}

export default BannerService