import React from 'react'
import styles from './styles/brand-device.module.scss'
import { faChevronRight } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


const Brandevice = () => {
  return (
    <section id="brandDevice" className={`${styles.brandSection} container`}>
      <h2 className={styles.title}>Hãng Laptop</h2>
      <p className={styles.des}>Chúng tôi có đội ngũ kỹ thuật viên giàu kinh nghiệm, sẵn sàng hỗ trợ khắc phục mọi lỗi từ phần mềm đến phần cứng trên tất cả các hãng laptop như Apple, Dell, HP, Asus, Lenovo, và nhiều hơn nữa. Dù bạn gặp bất kỳ vấn đề nào, hãy liên hệ ngay để được hỗ trợ nhanh chóng và hiệu quả!</p>
      <div className={styles.groupBrandLaptop}>
          <div className={`${styles.logoBrand} ${styles.dell}`}/>
          <div className={styles.icons}> <FontAwesomeIcon icon={faChevronRight} /></div>
          <div className={`${styles.logoBrand} ${styles.hp} `}/>
          <div className={styles.icons}> <FontAwesomeIcon icon={faChevronRight} /></div>
          <div className={`${styles.logoBrand} ${styles.mac} `}/>
          <div className={styles.icons}> <FontAwesomeIcon icon={faChevronRight} /></div>
          <div className={`${styles.logoBrand} ${styles.lenovo} `}/>
          <div className={styles.icons}> <FontAwesomeIcon icon={faChevronRight} /></div>
          <div className={`${styles.logoBrand} ${styles.msi} `}/>
          <div className={styles.icons}> <FontAwesomeIcon icon={faChevronRight} /></div>
          <div className={`${styles.logoBrand} ${styles.assus} `}/>
          <div className={`${styles.logoBrand} ${styles.assus} ${styles.surface}`}/>
      </div>
    
    </section>
  )
}

export default Brandevice