import React from 'react'
import styles from './styles/at-store.module.scss'
import { faSquareCheck } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


const AtStore = () => {
  return (
    <section id="atStore" className={`${styles.atStore} container`}>
      <h2>Tại IT Quy Nhơn</h2>
      <div className={styles.benefit}>
          <div className={styles.groupItem}>
            <h2 className={styles.title}>Miễn Phí </h2>
            <ul className={styles.list}> 
                <li className={styles.itemContent}><FontAwesomeIcon icon={faSquareCheck} /> Miễn phí vệ sinh Laptop </li>
                <li className={styles.itemContent}><FontAwesomeIcon icon={faSquareCheck} /> Miễn phí tư vấn Laptop </li>
                <li className={styles.itemContent}><FontAwesomeIcon icon={faSquareCheck} /> Miễn phí cài đặt phần mềm </li>
            </ul>
          </div>
          <div className={styles.groupItem}>
            <h2 className={styles.title}>Quy Trình</h2>
            <ul className={styles.list}> 
                <li className={styles.itemContent}><FontAwesomeIcon icon={faSquareCheck} /> Nghiệm Thu Laptop </li>
                <li className={styles.itemContent}><FontAwesomeIcon icon={faSquareCheck} /> Hướng dẫn sử dụng </li>
                <li className={styles.itemContent}><FontAwesomeIcon icon={faSquareCheck} /> Xem trực tiếp Quy Trình Bảo dưỡng</li>
            </ul>
          </div>
         

      </div>
    </section>
  )
}

export default AtStore