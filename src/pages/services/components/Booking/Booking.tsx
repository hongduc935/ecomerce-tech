import React from 'react'
import styles from './styles/booking.module.scss'


const Booking = () => {
  return (
    <section id='booking' className={`${styles.booking} container`}>
      <h2 className={styles.titleBooking}>Đặt lịch tư vấn </h2>
      <p className={styles.desBook}>Bạn đang phân vân không biết nên chọn chiếc laptop nào phù hợp với nhu cầu học tập hay làm việc? Đừng lo lắng! Hãy đặt lịch ngay hôm nay để nhận được tư vấn cá nhân hóa, giúp bạn lựa chọn sản phẩm phù hợp nhất. Chúng tôi sẽ lắng nghe nhu cầu của bạn và đưa ra những gợi ý chính xác.</p>
      <div className={styles.formContainer}>
          <form className={styles.formBooking}>
                <div className={styles.rowForm}>
                    <div className={styles.formField}>
                        <p className={styles.labelField}>Họ và tên * </p>
                        <input className={styles.inputField} type='text'/>
                    </div>
                    <div className={styles.formField}>
                        <p className={styles.labelField}>Số điện thoại * </p>
                        <input className={styles.inputField} type='text'/>
                    </div>
                </div>
                <div className={styles.rowForm}>
                    <div className={styles.formField}>
                        <p className={styles.labelField}>Email * </p>
                        <input className={styles.inputField} type='text'/>
                    </div>
                    <div className={styles.formField}>
                        <p className={styles.labelField}>Thời gian * </p>
                        <input className={styles.inputField} type='date'/>
                    </div>
                </div>
                <div className={styles.rowForm}>
                    <div className={styles.formField}>
                        <p className={styles.labelField}>Địa chỉ tư vấn * </p>
                        <input className={styles.inputField} type='text'/>
                    </div>
                    <div className={styles.formField}>
                        <p className={styles.labelField}>Hình thức tư vấn * </p>
                        <input className={styles.inputField} type='text'/>
                    </div>
                </div>
                {/* <div className={styles.rowForm}>
                    <div className={styles.frameTime}>
                        <p className={styles.labelField}>Khung Giờ </p>
                    
                    </div>
                </div> */}
                <div className={styles.rowForm}>
                    <div className={styles.textarea}>
                        <p className={styles.labelField}>Nội dung cần giải đáp * </p>
                        <textarea className={styles.des} ></textarea>
                    </div>
                </div>
                <div className={styles.rowForm}>
                  <button className={styles.submit}>Đặt lịch tư vấn</button>
                </div>
          </form>
      </div>
    </section>
  )
}

export default Booking