import React from 'react'
import styles from './styles/services.module.scss'
import ComponentPage from '@/components/ComponentPage/ComponentPage'
import { Meta } from '@/layout/Meta'
import Brandevice from './components/BrandDevice/Brandevice'
import ErrorDevice from './components/ErrorDevice/ErrorDevice'
import BannerService from './components/BannerService/BannerService'
import Voucher from './components/Voucher/Voucher'
import Booking from './components/Booking/Booking'
import MainServices from './components/MainServices/MainServices'
import AtStore from './components/AtStore/AtStore'
import NewNotice from './components/NewNotice/NewNotice'


const Services = () => {
  return (
    <ComponentPage isHiddenFooter={true} isHiddenHeader={true}>
      <Meta title={"Dịch Vụ sửa chữa laptop - Chuyên sửa chữa các dòng laptop Window và Macbook với giá ưu đãi chỉ từ 30k | IT QUY NHƠN Việt Nam"} description={'Đăng nhập tài khoản  - Mua sắm Online | Shopee Việt Nam'} />
      <div className={styles.services}>
        <MainServices/>
        <NewNotice/>
        <BannerService/>
        <Voucher/>
        <Brandevice/>
        <ErrorDevice/>
        <Booking/>
        <AtStore/>
      </div>
    </ComponentPage>

  )
}

export default Services