import { NextPage } from 'next'
import React from 'react'
import styles from './styles/login.module.scss'
import Link from 'next/link'
import { useForm, SubmitHandler } from "react-hook-form"
import ComponentPage from '@/components/ComponentPage/ComponentPage'
import { Meta } from '@/layout/Meta'
import { useAuth } from '@/contexts/AuthProvider'
import { useRouter } from 'next/router'

type ILoginFormInputs ={
  username: string;
  password: string;
} 

const Login:NextPage  = () => {
  
  // const { login, account, isLoading, isError } = useAuth();
  const { login, isAuthenticated, loading } = useAuth();
  const { register, handleSubmit } = useForm<ILoginFormInputs>();


  const router = useRouter();

  
  const onSubmitLogin: SubmitHandler<ILoginFormInputs> = async (data:ILoginFormInputs) => {
    try {

      console.log("Data: "+JSON.stringify(data))
      login(data.username, data.password);
      router.push('/login');
      if (isAuthenticated) {
        // return <div>Welcome, {user.email}</div>;
        // console.log("okekeke")
        router.push('/login');
      }
      // console.log(JSON.stringify(res))
      // Redirect hoặc thực hiện hành động khác sau khi đăng nhập thành công
    } catch (err) {
      console.error(err);
    }
  }

  if (loading) return <div>Loading...</div>;

  // if (isAuthenticated) {
  //   // return <div>Welcome, {user.email}</div>;
  //   // console.log("okekeke")
  //   router.push('/mac');
  // }
  

  return (

    <ComponentPage isHiddenFooter={false} isHiddenHeader={false}>
      <Meta title={"Đăng nhập tài khoản  - Chuyên mua bán đồ công nghệ Apple Online | IT QUY NHƠN Việt Nam"} description={'Đăng nhập tài khoản  - Mua sắm Online | Shopee Việt Nam'} />
            <div className={`${styles.login} item-center w-100 `}>

            <div className={styles.loginFrame}>
                  <form className={styles.formLogin}  onSubmit={handleSubmit(onSubmitLogin)} >
                      <div  className={styles.rowHead}>
                        <div className={styles.logo}>
                            <div className={`${styles.layoutImage} item-center`}>
                                <div className={styles.image}/>
                            </div>
                            <div className={styles.name}>
                                IT QUY NHƠN
                            </div>
                        </div>
                        <span className={styles.nameForm}>Đăng nhập</span>
                      </div>
                      <div  className={styles.rowField}>
                        <div className={styles.field}>
                              <input 
                                type='text' 
                                placeholder='Email/Phone/Tên đăng nhập' 
                                className={styles.fieldInput} 
                                {...register('username', { required: 'Username is required' })}
                              />
                              <span className={styles.err}></span>
                        </div>
                      </div>
                      <div  className={styles.rowField}>
                        <div className={styles.field}>
                              <input 
                                type='password' 
                                placeholder='Mật khẩu' 
                                className={styles.fieldInput} 
                                {...register('password', { required: 'Password is required' })}
                              />
                              <span className={styles.err}></span>
                        </div>
                      </div>
                      <div  className={styles.rowField}>
                        <div className={styles.field}>
                              <button className={styles.btnLogin}>ĐĂNG NHẬP</button>
                        </div>
                      </div>
                      <div className={styles.rowAction}>
                            <div className={styles.forgot}>
                                  <Link href={'/'}>Quên mật khẩu</Link>
                            </div>
                      </div>
                      <div className={styles.line}>
                              <div className={styles.li}></div> &nbsp;Hoặc &nbsp;<div className={styles.li}></div>
                      </div>
                      <div className={styles.rowSocial}>
                            <button className={styles.btnSocial}><div className={`${styles.icons} ${styles.fb}`}/> &nbsp;<div className={`${styles.txt} item-center`}>Facebook</div></button>
                            <button className={styles.btnSocial}><div className={`${styles.icons} ${styles.gg}`}/> &nbsp;&nbsp; &nbsp; <div className={`${styles.txt} item-center`}>Google</div></button>
                      </div>
                      <div className={`${styles.rowRegister} item-center`}>
                           Bạn mới biết đến &nbsp; <strong>IT QUY NHƠN</strong> ? &nbsp;<Link href={'/register'}>Đăng ký</Link>
                      </div>
                  </form>
            </div>
          </div>
    </ComponentPage>
    
  )
}

export default Login
