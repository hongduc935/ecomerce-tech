import ComponentPage from '@/components/ComponentPage/ComponentPage'
import { Meta } from '@/layout/Meta'
import React from 'react'



interface ISeoWatchPage{
  title:string 
  description:string 
}


const SEO_ACCESSORY_PAGE :ISeoWatchPage= {
  title:"IT QUY NHƠN | Chuyên bán đồng hồ Apple Watch giá rẻ !",
  description:""
}

const AppleWatch = () => {
  return (
    <ComponentPage isHiddenFooter={true} isHiddenHeader={true}>
        <Meta title={SEO_ACCESSORY_PAGE.title} description={SEO_ACCESSORY_PAGE.description} />
    </ComponentPage>
  )
}

export default AppleWatch
