import React from 'react'
import styles from './styles/success-story.module.scss'
import { faQuoteLeft, faQuoteRight } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


const SuccessStory = () => {
  return (
    <div className={`${styles.successStory} container`}>
      <h2 className={styles.titleSection}>Đối tác nói gì về chúng tôi ?</h2> 
      <div className={`${styles.contentSuccessStory} item-center`}>
          <div className={`${styles.quotes} item-btw container`}>
              <div className={`${styles.quote} `}>
                  <div className={`${styles.imageProfile}`}>
                      <div className={`${styles.image} ${styles.image0}`}>
                          
                      </div>
                  </div>
                  <div className={`${styles.info}`}>
                      <p className={styles.name}>Jerry Edmonds</p>
                      <p className={styles.pos}>Chief Executive Officer (CEO) & Founder at TSP Contracting</p>
                      <p className={styles.content}> <FontAwesomeIcon icon={faQuoteLeft} /> We greatly appreciate the flexibility and collaborative spirit of the company in meeting the specific requirements of the Zoho CRM project. They consistently listen to and understand our needs, thereby providing the most suitable and effective solutions. <FontAwesomeIcon icon={faQuoteRight} /></p>
                  </div>
              </div>

              <div className={`${styles.quote} `}>
                  <div className={`${styles.imageProfile}`}>
                      <div className={`${styles.image} ${styles.image1}`}>
                          
                      </div>
                  </div>
                  <div className={`${styles.info}`}>
                      <p className={styles.name}>황명욱</p>
                      <p className={styles.pos}>Chief Executive Officer (CEO) & Founder at SHSOFT VINA</p>
                      <p className={styles.content}><FontAwesomeIcon icon={faQuoteLeft} /> We are very satisfied with the results achieved; the Zoho CRM system is not only stable but also user-friendly and well-suited to our business needs. This has helped us enhance work efficiency and achieve important business objectives. <FontAwesomeIcon icon={faQuoteRight} /></p>
                  </div>
              </div>
          </div>

         
      </div>
    </div>
  )
}

export default SuccessStory
