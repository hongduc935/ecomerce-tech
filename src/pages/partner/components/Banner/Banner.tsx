import React from 'react'
import styles from './styles/banner.module.scss'
import { faChevronRight } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Image from 'next/image'
const Banner = () => {
  return (
    <div className={`${styles.bannerPartner} `}>
      <div className={styles.layoutBanner}>
          <h1 className={styles.titleBanner}>
            Trở Thành Đối Tác Kinh Doanh  IT QUY NHƠN
          </h1>
          <p className={styles.des}>  Chúng tôi mang đến cơ hội kinh doanh tuyệt vời cho các đối tác muốn mở rộng thị trường và tối ưu hóa lợi nhuận. Hãy cùng chúng tôi khám phá những lợi ích tuyệt vời khi trở thành đối tác kinh doanh trên IT QUY NHƠN. </p>
          <div className={styles.lBtn}>
              <button className={styles.par}>Tham gia ngay &nbsp; <FontAwesomeIcon icon={faChevronRight} /></button>
          </div>
      </div>
      <div className={styles.imageBanner}>
        <Image
          src={`/assets/partner/apple-partner.webp`}
          alt='Lap top '
          width={0}
          height={0}
          loading="lazy"
          sizes="100vw"
          style={{ width: '100%', height: 'auto' }} // optional
        />
      </div>
      
    </div>
  )
}

export default Banner