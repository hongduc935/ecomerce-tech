import React from 'react'
import styles from './styles/partner-list.module.scss'
import Image from 'next/image'
import SlideInLeft from '@/components/Animations/SlideInLeft'

const PartnerList = () => {
  return (
    <div className={`${styles.partnerList} container`}>
      <h2 className={styles.titleSection}> Đối tác thân thiết của chúng tôi </h2>
      <SlideInLeft>
      <div className={`${styles.listPartner} w-100 item-btw`}>
          <div className={styles.partner}>
                <Image
                  src={`https://www.west-advisory.com/logo2.webp`}
                  alt={`IT QUY NHƠN | Bộ Tiếp Hợp Nhiều Cổng USB-C Digital AV`}
                  width={0}
                  height={0}
                  sizes="100vw"
                  objectFit='contain'
                  style={{ width: '300px', height: 'auto' }} // optional
                />
          </div>
          <div className={styles.partner}>
                <Image
                  src={`https://images.crunchbase.com/image/upload/c_pad,h_256,w_256,f_auto,q_auto:eco,dpr_1/kpnwy8vbihpbdh3ni1qc`}
                  alt={`IT QUY NHƠN | Bộ Tiếp Hợp Nhiều Cổng USB-C Digital AV`}
                  width={0}
                  height={0}
                  sizes="100vw" 
                  objectFit='contain'
                  style={{ width: '250px', height: 'auto' }} // optional
                />
          </div>
          <div className={styles.partner}>
                <Image
                  src={`https://static.ybox.vn/2023/12/5/1701996950174-SHSOFTVINA_LOGO.png`}
                  alt={`IT QUY NHƠN | Bộ Tiếp Hợp Nhiều Cổng USB-C Digital AV`}
                  width={0}
                  height={0}
                  sizes="100vw"
                  objectFit='contain'
                  style={{ width: '250px', height: 'auto' }} // optional
                />
          </div>
          <div className={styles.partner}>
                <Image
                  src={`https://www.west-advisory.com/tsp.webp`}
                  alt={`IT QUY NHƠN | Bộ Tiếp Hợp Nhiều Cổng USB-C Digital AV`}
                  width={0}
                  height={0}
                  sizes="100vw"
                  objectFit='contain'
                  style={{ width: '200px', height: 'auto' }} // optional
                />
          </div>
      </div>
      </SlideInLeft>
      
    </div>
  )
}

export default PartnerList