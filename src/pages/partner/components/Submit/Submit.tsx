import React from 'react'
import styles from './styles/submit.module.scss'

const Submit = () => {
  return (
    <div className={`${styles.submitPartner} container`}>
      <div className={styles.titleSection }>
          <h2 className={styles.title}>Đối tác của IT QUY NHƠN</h2>
          <div className={styles.line}></div>
          <p className={styles.introduce}>IT QUY NHƠN – Nơi hội tụ đầy đủ các sản phẩm Apple chính hãng với giá cạnh tranh, dịch vụ hỗ trợ chuyên nghiệp và trải nghiệm mua sắm tiện lợi, nhanh chóng!</p>
      </div>
      <div className={styles.formSection}>
          <div className={styles.formLayout}>
            <form className={styles.formSubmit}>
                <div className={`${styles.rowForm} item-btw`}>
                    <div className={`${styles.field} w-40`}>
                        <p className={styles.nameStore}>Tên Cửa Hàng <span className={styles.require}> * </span></p>
                        <input className={styles.inputField} placeholder='Bạn vui Lòng nhập tên cửa hàng '/>
                    </div>
                    <div className={`${styles.field} w-40`}>
                        <p className={styles.nameStore}>Email <span className={styles.require}> * </span></p>
                        <input className={styles.inputField} placeholder='Bạn vui Lòng nhập email của bạn '/>
                    </div>
                </div>
                <div className={`${styles.rowForm} item-btw`}>
                    <div className={`${styles.field} w-40`}>
                        <p className={styles.nameStore}>Số điện thoại <span className={styles.require}> * </span></p>
                        <input className={styles.inputField} placeholder='Bạn vui Lòng nhập số điện thoại của bạn '/>
                    </div>
                    <div className={`${styles.field} w-40`}>
                        <p className={styles.nameStore}>Địa chỉ <span className={styles.require}> * </span></p>
                        <input className={styles.inputField} placeholder='Bạn vui lòng nhập đầy đủ địa chỉ cửa hàng '/>
                    </div>
                </div>
                <div className={`${styles.rowForm} item-btw`}>
                    
                    <div className={styles.field}>
                      <input type='checkbox'/>
                        <label className={styles.agreePolicy}>Tôi đồng với ý với các <span className={styles.strong}>Chính sách</span> và <span className={styles.strong}>Quy định </span>của IT QUY NHƠN</label>
                    </div>
                </div>
                <div className={`${styles.rowForm} item-center`}>
                    
                    <div className={styles.field}>
                        <button className={`${styles.btnSubmit} ${styles.btnRegister}`}>Đăng kí</button>
                        <button className={`${styles.btnSubmit} ${styles.cancel}`}>Làm mới </button>
                    </div>
                </div>

            </form>

          </div>
      </div>
    </div>
  )
}

export default Submit