import React from 'react'
import styles from './styles/benefit.module.scss'
import FadeIn from '@/components/Animations/FadeIn'

const Benefit = () => {
  return (
    <div className={`${styles.benefitPartner} container`}>
      <h2 className={styles.titleSection }>Lợi Ích Khi Trở Thành Đối Tác Của IT QUY NHƠN </h2>
      <div className={`${styles.layoutLine} item-center`}>
          <div className={styles.line}> </div>
      </div>
      <div className={`${styles.sectionShowCase} container item-center`}>
          <div className={`${styles.listBenefit} w-90`}>
            <FadeIn>
                <div className={styles.rowBenefit}>
                      <div className={`${styles.itemBenfit}`}>
                            <div className={styles.icons}>
                                  <div className={`${styles.imageIcon} ${styles.icGrowth}`}/>
                            </div>
                            <div className={styles.contentBenefit}>
                                  <h3 className={styles.titleBenefit}>Tiếp cận khách hàng</h3>
                                  <p className={styles.desBenefit}>Với hàng ngàn người dùng truy cập mỗi ngày, IT QUY NHƠN là nơi lý tưởng để bạn giới thiệu sản phẩm đến hàng triệu khách hàng tiềm năng. Mở rộng thị trường của bạn một cách dễ dàng và nhanh chóng, nâng cao doanh số bán hàng mà không cần phải đầu tư thêm vào các kênh tiếp thị khác.</p>
                            </div>
                      </div>
                      <div className={`${styles.itemBenfit} w-40`}>
                            <div className={styles.icons}>
                                  <div className={`${styles.imageIcon} ${styles.icManage}`}/>
                            </div>
                            <div className={styles.contentBenefit}>
                                  <h3 className={styles.titleBenefit}>Quản lý bán hàng chuyên nghiệp</h3>
                                  <p className={styles.desBenefit}>
                                    Hệ thống quản lý bán hàng tiên tiến của chúng tôi giúp bạn theo dõi từng đơn hàng, từ khi khách hàng đặt mua cho đến khi sản phẩm được giao thành công. Với giao diện thân thiện và dễ sử dụng, bạn có thể quản lý sản phẩm, kho hàng và đơn hàng một cách hiệu quả và tiết kiệm thời gian
                                  </p>
                            </div>
                      </div>
                </div>
            </FadeIn>
            <FadeIn>
                <div className={styles.rowBenefit}>
                      <div className={`${styles.itemBenfit}`}>
                            <div className={styles.icons}>
                                  <div className={`${styles.imageIcon} ${styles.icResearch}`}/>
                            </div>
                            <div className={styles.contentBenefit}>
                                  <h3 className={styles.titleBenefit}>Tìm kiếm khách hàng tiềm năng</h3>
                                  <p className={styles.desBenefit}>
                                    Chúng tôi cung cấp các công cụ phân tích và dữ liệu khách hàng giúp bạn dễ dàng nhận diện và tiếp cận nhóm khách hàng tiềm năng nhất. Từ đó, bạn có thể tối ưu hóa chiến lược kinh doanh và tiếp thị của mình, mang lại lợi nhuận cao hơn.
                                  </p>
                            </div>
                      </div>
                      <div className={`${styles.itemBenfit} w-40`}>
                            <div className={styles.icons}>
                                  <div className={`${styles.imageIcon} ${styles.icSetting}`}/>
                            </div>
                            <div className={styles.contentBenefit}>
                                  <h3 className={styles.titleBenefit}>Hỗ trợ quảng bá sản phẩm</h3>
                                  <p className={styles.desBenefit}> Đội ngũ marketing chuyên nghiệp của chúng tôi sẽ hỗ trợ bạn trong việc xây dựng các chiến dịch quảng cáo hiệu quả. Bạn sẽ nhận được sự hỗ trợ từ việc tạo nội dung, thiết kế hình ảnh đến việc chạy các quảng cáo trực tuyến, đảm bảo sản phẩm của bạn được nhiều người biết đến hơn.   </p>
                            </div>
                      </div>
                </div>
            </FadeIn>
            <FadeIn>
                <div className={styles.rowBenefit}>
                      <div className={`${styles.itemBenfit}`}>
                            <div className={styles.icons}>
                                  <div className={`${styles.imageIcon} ${styles.icSetting}`}/>
                            </div>
                            <div className={styles.contentBenefit}>
                                  <h3 className={styles.titleBenefit}>Hỗ trợ 24/7</h3>
                                  <p className={styles.desBenefit}> Đội ngũ chăm sóc khách hàng của chúng tôi luôn sẵn sàng hỗ trợ bạn mọi lúc mọi nơi. Bất kể bạn gặp vấn đề gì, chúng tôi đều sẽ nhanh chóng giải quyết, giúp bạn yên tâm tập trung vào việc kinh doanh </p>
                            </div>
                      </div>
                      <div className={`${styles.itemBenfit} w-40`}>
                            <div className={styles.icons}>
                                  <div className={`${styles.imageIcon} ${styles.icAnalytics}`}/>
                            </div>
                            <div className={styles.contentBenefit}>
                                  <h3 className={styles.titleBenefit}>Phân tích dữ liệu bán hàng</h3>
                                  <p className={styles.desBenefit}>Với công cụ phân tích dữ liệu mạnh mẽ, bạn sẽ nắm được tình hình kinh doanh một cách chi tiết và chính xác. Từ số liệu về doanh thu, lượng hàng tồn kho đến hành vi mua sắm của khách hàng, tất cả đều được cập nhật liên tục, giúp bạn đưa ra các quyết định kinh doanh thông minh và kịp thời.</p>
                            </div>
                      </div>
                </div>
            </FadeIn>
                
               
          </div>
      </div>
    </div>
  )
}

export default Benefit