import React from 'react'
import styles from './styles/faq.module.scss'
import { faCaretRight } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const FAQ = () => {
  return (
    <div className={`${styles.faq} container`}>
      <h2 className={styles.titleSection}>Câu hỏi thường gặp ?</h2>
      <div className={styles.line}></div>
      <div className={styles.contentFaq}>
          <>
            <div className={styles.c}>
              <input className={styles.input} type="checkbox" id="faq-1" />
              <h1 className={styles.h1}>
                <label htmlFor="faq-1" className={styles.label}><FontAwesomeIcon icon={faCaretRight} /> &nbsp; Công thức chia hoa hồng khi bán một đơn hàng thành công  ?</label>
              </h1>
              <div className={styles.p}>
                <p>" Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque vitae lorem ultricies, egestas tellus nec, molestie felis. Praesent mollis aliquam metus tempor euismod. Nam porta dictum blandit. Cras sit amet odio tempus, consequat lorem sagittis, ullamcorper urna. Proin vitae tincidunt dolor. Nam sapien justo, ultricies sed cursus ut, feugiat ac metus. Suspendisse condimentum at ex pharetra egestas. Suspendisse at dapibus dolor. "</p>
              </div>
            </div>
            <div className={styles.c}>
              <input className={styles.input} type="checkbox" id="faq-2" />
              <h1 className={styles.h1}>
                <label htmlFor="faq-2" className={styles.label}><FontAwesomeIcon icon={faCaretRight} /> &nbsp; Điều kiện để trở thành đối tác cấp cao là gì  ?</label>
              </h1>
              <div className={styles.p}>
                <p>" Donec scelerisque finibus ultricies. Aliquam erat volutpat. Pellentesque eget diam aliquam, iaculis eros vel, sollicitudin massa. Curabitur porttitor, elit vel gravida lobortis, enim justo commodo sem, in suscipit urna ipsum eget ante. Vestibulum ac erat pretium orci blandit sollicitudin. Mauris pretium mi eget metus lacinia, ac fermentum dolor malesuada. In viverra est in enim mattis, eu malesuada elit cursus. Vestibulum fringilla tellus eget venenatis luctus. Cras ultricies aliquet aliquam. "</p>
              </div>
            </div>
            <div className={styles.c}>
              <input className={styles.input} type="checkbox" id="faq-3" />
              <h1 className={styles.h1}>
                <label htmlFor="faq-3" className={styles.label}> <FontAwesomeIcon icon={faCaretRight} /> &nbsp; Chương trình tiếp thị liên kết hoa hồng như thế nào  ?</label>
              </h1>
              <div className={styles.p}>
                <p>
                " Donec scelerisque finibus ultricies. Aliquam erat volutpat. Pellentesque eget diam aliquam, iaculis eros vel, sollicitudin massa. Curabitur porttitor, elit vel gravida lobortis, enim justo commodo sem, in suscipit urna ipsum eget ante. Vestibulum ac erat pretium orci blandit sollicitudin. Mauris pretium mi eget metus lacinia, ac fermentum dolor malesuada. In viverra est in enim mattis, eu malesuada elit cursus. Vestibulum fringilla tellus eget venenatis luctus. Cras ultricies aliquet aliquam..{" "}
                  <a href="https://css-tricks.com/the-checkbox-hack/">link to article</a> "
                </p>
              </div>
            </div>
          </>


      </div>
    </div>
  )
}

export default FAQ
