import React from 'react'
import CountUp from 'react-countup';
import styles from './styles/info.module.scss'

const Info = () => {
  return (
    <div className={styles.infoPartner}>
      {/* Các con số chứng minh lợi ích khi tham gia hệ thống IT QUY NHƠN */}
      <div className={`${styles.titleSection} item-center`}>
        <h2 className={styles.numberSystem}>Hệ thống của chúng tôi</h2>
      </div>
      
      <div className={ `${styles.layoutItem} item-center`}>
          <div className={`${styles.mainLayout} w-80 item-btw`}>
              <div className={styles.item}>
                  <h3 className={styles.number}> <CountUp end={2000} duration={2} /> + </h3>
                  <div className={styles.line}></div>
                  <p className={styles.title}>Cửa hàng đối tác</p>
              </div>
              <div className={styles.item}>
                  <h3 className={styles.number}><CountUp end={426} duration={1.5} />+ </h3>
                  <div className={styles.line}></div>
                  <p className={styles.title}>Tiếp thị liên kết</p>
              </div>
              <div className={styles.item}>
                  <h3 className={styles.number}><CountUp end={5839} duration={2.5} /> + </h3>
                  <div className={styles.line}></div>
                  <p className={styles.title}>Sản phẩm</p>
              </div>
              <div className={styles.item}>
                  <h3 className={styles.number}><CountUp end={5} duration={1} /> + </h3>
                  <div className={styles.line}></div>
                  <p className={styles.title}>Thành lập</p>
              </div>
          </div>
      </div>
    </div>
  )
}

export default Info