import React from 'react'
import styles from './styles/progress-registration.module.scss' 
import { faListCheck, faMagnifyingGlass, faPenClip, faShop } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const ProgressRegistration = () => {
  return (
    <div className={`${styles.progressRegis} container`}>
      <h2 className={styles.titleSection}> Quy Trình Đăng Ký Đối Tác </h2>
      <div className={styles.contentProgress}>
          {/* <ul className={styles.progressList}>
              <li className={styles.progressStep}>Bước 1: Đăng ký tài khoản đối tác. </li>
              <li className={styles.progressStep}>Bước 2: Xác minh thông tin và phê duyệt tài khoản. </li>
              <li className={styles.progressStep}>Bước 3: Đăng tải sản phẩm và bắt đầu bán hàng </li>
              <li className={styles.progressStep}>Liên kết đến trang đăng ký: Đính kèm liên kết đăng ký đối tác.</li>
          </ul> */}
          <div className={`${styles.rowStep} container`}>
              <div className={styles.step}>
                    <div className={`${styles.icons} item-center`}>
                        <FontAwesomeIcon icon={faPenClip} />
                    </div>
                    <div className={styles.content}>
                        <h3 className={styles.titleStep}>ĐĂNG KÍ</h3>
                        <p className={styles.desStep}>- Yêu cầu Đăng kí tài khoản  </p>
                        <p className={styles.desStep}>- Yêu cầu Đăng kí tài khoản  </p>
                        <p className={styles.desStep}>- Yêu cầu Đăng kí tài khoản  </p>
                    </div>
              </div>
              <div className={`${styles.space} `}/>
              <div className={styles.step}>
                    <div className={`${styles.icons} item-center`}>
                          <FontAwesomeIcon icon={faListCheck} />
                    </div>
                    <div className={styles.content}>
                        <h3 className={styles.titleStep}>XÁC MINH</h3>
                        <p className={styles.desStep}>- Yêu cầu Đăng kí tài khoản  </p>
                        <p className={styles.desStep}>- Yêu cầu Đăng kí tài khoản  </p>
                        <p className={styles.desStep}>- Yêu cầu Đăng kí tài khoản  </p>
                    </div>
              </div>
              <div className={`${styles.space} `}/>
              <div className={styles.step}>
                    <div className={`${styles.icons} item-center`}>
                      <FontAwesomeIcon icon={faMagnifyingGlass} />
                    </div>
                    <div className={styles.content}>
                        <h3 className={styles.titleStep}>TÌM KIẾM</h3>
                        <p className={styles.desStep}>- Yêu cầu Đăng kí tài khoản  </p>
                        <p className={styles.desStep}>- Yêu cầu Đăng kí tài khoản  </p>
                        <p className={styles.desStep}>- Yêu cầu Đăng kí tài khoản  </p>
                    </div>
              </div>
              <div className={`${styles.space} `}/>
              <div className={styles.step}>
                    <div className={`${styles.icons} item-center`}>
                      <FontAwesomeIcon icon={faShop} />
                    </div>
                    <div className={styles.content}>
                        <h3 className={styles.titleStep}>BÁN HÀNG</h3>
                        <p className={styles.desStep}>- Yêu cầu Đăng kí tài khoản  </p>
                        <p className={styles.desStep}>- Yêu cầu Đăng kí tài khoản  </p>
                        <p className={styles.desStep}>- Yêu cầu Đăng kí tài khoản  </p>
                    </div>
              </div>
          </div>
      </div>
    </div>
  )
}

export default ProgressRegistration
