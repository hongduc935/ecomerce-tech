import React from 'react'
import ComponentPage from '@/components/ComponentPage/ComponentPage'
import { Meta } from '@/layout/Meta'
import Banner from './components/Banner/Banner'
import Benefit from './components/Benefit/Benefit'
import Info from './components/Info/Info'
import Submit from './components/Submit/Submit'
import ProgressRegistration from './components/ProgressRegistration/ProgressRegistration'
import SuccessStory from './components/SuccessStory/SuccessStory'
import FAQ from './components/FAQ/FAQ'
import PartnerList from './components/PartnerList/PartnerList'

const SEO_PARTNER = {
  site_name: 'IT QUY NHƠN',
  title: 'Trở Thành Đối Tác Kinh Doanh Tại IT QUY NHƠN | Mở Rộng Thị Trường Cùng Chúng Tôi',
  description: 'Tham gia làm đối tác kinh doanh tại IT QUY NHƠN, nền tảng thương mại điện tử chuyên về sản phẩm Apple like new. Tăng doanh thu và tiếp cận khách hàng hiệu quả ngay hôm nay!',
  locale: 'en',
}


const Partner = () => {
  return (
    <ComponentPage isHiddenFooter={true} isHiddenHeader={true}>
      <Meta title={SEO_PARTNER.title} description={SEO_PARTNER.description} /> 
        <Banner/> 
        <Benefit/>
        <ProgressRegistration/>
        <PartnerList/>
        <SuccessStory/>
        <Info/>       
        <Submit/>
        <FAQ/>
    </ComponentPage>
  )
}

export default Partner