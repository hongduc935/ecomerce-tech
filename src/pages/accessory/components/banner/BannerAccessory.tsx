import React from 'react'
import styles from './styles/banner.module.scss'
import { Swiper, SwiperSlide } from 'swiper/react';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/pagination';
import { Pagination } from 'swiper/modules';
import Image from 'next/image'
import Link from 'next/link';


interface IBannerAccessory {
  title_banner?:string
  des_banner?:string 
  url_btn?:string
  url_img?:string
  alt_img?:string
}

const BannerDataList:IBannerAccessory[]  = [
  {
    title_banner:"MegaSafe",
    des_banner:"Gắn thêm ốp lưng, ví hoặc bộ sạc không dây." ,
    url_btn:"/",
    url_img:'/assets/accessory/magsafe.webp',
    alt_img:'Lap top '
  },
  {
    title_banner:"Mege Safe",
    des_banner:"Gắn thêm ốp lưng, ví hoặc bộ sạc không dây." ,
    url_btn:"/",
    url_img:'/assets/accessory/magsafe.webp',
    alt_img:'Lap top '
  },
  {
    title_banner:"Mege Safe",
    des_banner:"Gắn thêm ốp lưng, ví hoặc bộ sạc không dây." ,
    url_btn:"/",
    url_img:'/assets/accessory/magsafe.webp',
    alt_img:'Lap top '
  }
]


const BannerAccessory = () => {

  const pagination = {
    clickable: true,
    renderBullet: function (index:number, className:string) {
      console.log(className)
      return '<span class="' + 'stylePagination'  + '">' + (index + 1) + '</span>';
    },
  };
  return (
    <div className={styles.bannerAccessory}>

        <Swiper
                pagination={pagination}
                modules={[Pagination]}
                className={styles.swiperBannerAccessory}
              >
                {
                  BannerDataList.map((value:IBannerAccessory,index:number)=>{
                    return <SwiperSlide key={`banner_accessory_${index}`}>
                              <div className={styles.mainSlideAccessory}>
                                    <div className={styles.contentBanner}>
                                          <h2 className={styles.titleBanner}>
                                            {value.title_banner}
                                          </h2>
                                          <p className={styles.desBanner}>
                                            {value.des_banner}
                                          </p>
                                          <Link href={`${value.url_btn}`} target='_blank'><button className={styles.btnBuyAccessory}>Mua ngay</button></Link>
                                    </div>
                                    <Image
                                          src={`${value.url_img}`}
                                          alt={`${value.alt_img}`}
                                          width={0}
                                          height={0}
                                          sizes="100vw"
                                          style={{ width: '100%', height: 'auto' }} 
                                          objectFit="contain"
                                          objectPosition="center" 
                                    />
                              </div>
                            </SwiperSlide>
                  })
                }
      </Swiper>
      <div>
        
      </div>
    </div>
  )
}

export default BannerAccessory