import React from 'react'
import styles from './styles/search-accessory.module.scss'
import Image from 'next/image'
import { faMagnifyingGlass } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const AccessoryData:number[] = [1,2,3,4,5,6,7,8,9,10,11,12]


const SearchAccessory = () => {

  const chunkArray = (array:any, chunkSize:number) => {
    const results = [];
    for (let i = 0; i < array.length; i += chunkSize) {
      results.push(array.slice(i, i + chunkSize));
    }
    return results;
  };
   // Hàm xáo trộn mảng
  const shuffleArray = (array:any) => {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
  };

  // Tạo các cặp class
  const classPairs = [
    { widthClass: 'w-50', bgClass: styles.bgWith50 ,layoutInfo:styles.layoutInfo50},
    { widthClass: 'w-30', bgClass: styles.bgWith30 ,layoutInfo:styles.layoutInfo30},
    { widthClass: 'w-20', bgClass: styles.bgWith20 ,layoutInfo:styles.layoutInfo20}
  ];
  const groupedData = chunkArray(AccessoryData, 3);


  return (
    <div className={styles.searchAccessory}>
      <h2 className={styles.searchTitle}>Bạn đang tìm kiếm phụ kiện cho </h2>
      <div className={`${styles.productCategory} w-100 item-center`}>
          <div className={`${styles.listCategory} w-50 item-btw`}>
                <div className={`${styles.categoryItem} `}>
                  <div className={`${styles.image} item-center`}>
                        <div className={`${styles.icon} ${styles.itemMac}`}>

                        </div>
                  </div>
                  <div className={styles.nameAccessory}> Mac</div>
                </div>
                
                <div className={`${styles.categoryItem} `}>
                  <div className={`${styles.image} item-center`}>
                        <div className={`${styles.icon} ${styles.itemIpad}`}>

                        </div>
                  </div>
                  <div className={styles.nameAccessory}> Ipad</div>
                </div>
                <div className={`${styles.categoryItem} `}>
                  <div className={`${styles.image} item-center`}>
                        <div className={`${styles.icon} ${styles.itemIphone}`}>

                        </div>
                  </div>
                  <div className={styles.nameAccessory}> IPhone</div>
                </div>
                <div className={`${styles.categoryItem} `}>
                  <div className={`${styles.image} item-center`}>
                        <div className={`${styles.icon} ${styles.itemWatch}`}>

                        </div>
                  </div>
                  <div className={styles.nameAccessory}> Watch</div>
                </div>
          </div>
      </div>
      <div className={`${styles.searchInput} item-center`}>
          <div className={`${styles.layoutInput} w-40 item-center`}>
              <input placeholder='Tìm kiếm phụ kiện' className={styles.inputSearch}/>
              <div className={styles.iconSearch}>
                  <FontAwesomeIcon icon={faMagnifyingGlass} />
              </div>
             
          </div>
      </div>
      <div className={`${styles.contentAccessory} w-100 item-center`}>
          <div className={`${styles.layoutContent} w-80`}>

          {groupedData.map((group, rowIndex) => {
              const shuffledClassPairs :any= [...classPairs]
              shuffleArray(shuffledClassPairs)
              return (
                <div key={rowIndex} className={`${styles.rowGallery} w-100`}>
                  {group.map((item:any, idx:number) => {
                    console.log("Item :"+item)
                    const { widthClass, bgClass ,layoutInfo } = shuffledClassPairs[idx % shuffledClassPairs.length];
                    return (
                      <div key={idx} className={`${styles.itemGallery} ${widthClass} ${bgClass}`}>
                        <div className={styles.contentGallery}>
                          <div className={styles.imageGallery}>
                            <Image
                                          src={`https://store.storeimages.cdn-apple.com/8756/as-images.apple.com/is/MX3H3ref_AV1?wid=1420&hei=930&fmt=png-alpha&.v=1715270332030`}
                                          alt={`Sample`}
                                          width={0}
                                          height={0}
                                          sizes="100vw"
                                          style={{ width: '100%', height: 'auto' }} 
                                          objectFit="contain"
                                          objectPosition="center" 
                                    />
                          </div>
                          <div className={`${styles.infoGallery} ${layoutInfo} `}>
                            {/* Add additional info here if needed */}
                            <div className={styles.nameProduct}>
                                Apple Watch Seri 9 
                            </div>
                            <div className={styles.priceProduct}>
                                2.345.690đ
                            </div>
                            <div className={styles.btnBuyAcc}>
                                <button className={styles.btnB}>Mua ngay</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </div>
              );
            })}
              {/* <div className={`${styles.rowGallery} w-100`}>
                    <div className={`${styles.itemGellery} w-50 ${styles.bgWith50}`}>
                          Gallery 1
                          <div className={styles.contentGallery}>
                                <div className={styles.imageGallery}>

                                </div>
                                <div className={styles.infoGallery}>

                                </div>
                          </div>
                    </div>
                    <div className={`${styles.itemGellery} w-30 ${styles.bgWith30}`}>
                          Gallery 2
                    </div>
                    <div className={`${styles.itemGellery} w-20 ${styles.bgWith20}`}>
                          Gallery 3
                    </div>
              </div> */}

          </div>
      </div>
    </div>
  )
}

export default SearchAccessory