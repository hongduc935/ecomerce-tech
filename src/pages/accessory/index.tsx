import ComponentPage from '@/components/ComponentPage/ComponentPage'
import { Meta } from '@/layout/Meta'
import React from 'react'
import BannerAccessory from './components/banner/BannerAccessory'
import SearchAccessory from './components/search/SearchAccessory'


interface ISeoAcessoryPage{
  title:string 
  description:string 
}


const SEO_ACCESSORY_PAGE :ISeoAcessoryPage= {
  title:"IT QUY NHƠN | Chuyên bán phụ kiện Apple giá rẻ !",
  description:""
}

const Accessory = () => {
  return (
    <ComponentPage isHiddenFooter={true} isHiddenHeader={true}>
      <Meta title={SEO_ACCESSORY_PAGE.title} description={SEO_ACCESSORY_PAGE.description} />
      <BannerAccessory/>
      <SearchAccessory/>
    </ComponentPage>
  )
}

export default Accessory