import React from 'react'
import styles from './styles/register.module.scss'
import ComponentPage from '@/components/ComponentPage/ComponentPage'
import { Meta } from '@/layout/Meta'
import Link from 'next/link'
import { SubmitHandler, useForm } from 'react-hook-form'
import AuthService from '@/apis/auth/AuthService'
import NotifyUtils from '@/utils/notice/toastify'
import { useRouter } from 'next/router'
import { IRegisterRes } from '@/@types/interface'
import { AxiosResponse } from 'axios'
interface IRegisterFormInputs {
  email: string;
  phone: string;
  username: string;
  password: string;
  confirm_password: string;
}


const Register = () => {

  const { register,watch, handleSubmit, formState: { errors } } = useForm<IRegisterFormInputs>();
  const router = useRouter();
  const onSubmitRegister: SubmitHandler<IRegisterFormInputs> = async (data_submit:IRegisterFormInputs) => {
    try {

      // console.log("Data: "+JSON.stringify(data_submit))
      const response:any = await AuthService.register(data_submit);
      const statusCode = response.statusCode
      const msg = response.msg

      if(Number(statusCode) === 2000){
        NotifyUtils.error(msg)
      }
      else{
        NotifyUtils.success(msg)
        router.push('/login');
      }
     
      
      // console.log("JSON REGISTER"+JSON.stringify(resultRegister))
    } catch (err) {
      console.error(err);
      NotifyUtils.error('Đã xảy ra lỗi. Vui lòng thử lại.');
    }
  }
  return (
    <ComponentPage isHiddenFooter={false} isHiddenHeader={false}>
    <Meta title={"Đăng kí tài khoản  - Chuyên mua bán đồ công nghệ Apple Online | IT QUY NHƠN Việt Nam"} description={'Đăng nhập tài khoản  - Mua sắm Online | Shopee Việt Nam'} />
          <div className={`${styles.register} item-center w-100 `}>

          <div className={styles.registerFrame}>
                <form className={styles.formRegister } onSubmit={handleSubmit(onSubmitRegister)} >
                    <div  className={styles.rowHead}>
                      <div className={styles.logo}>
                          <div className={`${styles.layoutImage} item-center`}>
                              <div className={styles.image}/>
                          </div>
                          <div className={styles.name}>
                              IT QUY NHƠN
                          </div>
                      </div>
                      <span className={styles.nameForm}>Đăng kí</span>
                    </div>
                    <div  className={styles.rowField}>
                      <div className={styles.field}>
                            <input 
                              type='text' 
                              placeholder='User Name' 
                              {...register('username', { required: 'Username is required' })} 
                              className={styles.fieldInput}
                            />
                            {errors.username && <span className={styles.err}>{errors.username.message}</span>}
                            {/* <span className={styles.err}></span> */}
                      </div>
                    </div>
                    <div  className={styles.rowField}>
                      <div className={styles.field}>
                            <input 
                              type='text' 
                              placeholder='Nhập số điện thoại' 
                              {...register('phone', { required: 'Trường số điện thoại bắt buộc' })} 
                              className={styles.fieldInput}
                            />
                            {errors.username && <span className={styles.err}>{errors.username.message}</span>}
                            {/* <span className={styles.err}></span> */}
                      </div>
                    </div>
                    <div  className={styles.rowField}>
                      <div className={styles.field}>
                            <input 
                              type='text' 
                              placeholder='Nhập Email' 
                              {...register('email', { required: 'Email is required' })} 
                              className={styles.fieldInput}
                            />
                            {errors.username && <span className={styles.err}>{errors.username.message}</span>}
                            {/* <span className={styles.err}></span> */}
                      </div>
                    </div>
                    <div  className={styles.rowField}>
                      <div className={styles.field}>
                            <input 
                              type='password' 
                              placeholder='Mật khẩu' 
                              {...register('password', { required: 'Password is required' })} 
                              className={styles.fieldInput}
                            />
                            {errors.password && <span className={styles.err}>{errors.password.message}</span>}
                            {/* <span className={styles.err}></span> */}
                      </div>
                    </div>
                    <div  className={styles.rowField}>
                      <div className={styles.field}>
                            <input 
                              type='password' 
                              placeholder='Nhập lại mật khẩu' 
                              className={styles.fieldInput} 
                              {...register('confirm_password', { 
                                required: 'Confirm password is required',
                                validate: value => value === watch('password') || 'Passwords do not match'
                              })} 
                            />
                            {errors.confirm_password && <span className={styles.err}>{errors.confirm_password.message}</span>}
                      </div>
                    </div>
                    <div  className={styles.rowField}>
                      <div className={styles.field}>
                            <button className={styles.btnLogin}>ĐĂNG KÍ</button>
                      </div>
                    </div>
                    <div className={styles.rowAction}>
                          <div className={styles.forgot}>
                                <Link href={'/'}>ĐĂNG KÍ VỚI </Link>
                          </div>
                    </div>
                    <div className={styles.line}>
                            <div className={styles.li}></div> &nbsp;Hoặc &nbsp;<div className={styles.li}></div>
                    </div>
                    <div className={styles.rowSocial}>
                          <button className={styles.btnSocial}><div className={`${styles.icons} ${styles.fb}`}/> &nbsp;<div className={`${styles.txt} item-center`}>Facebook</div></button>
                          <button className={styles.btnSocial}><div className={`${styles.icons} ${styles.gg}`}/> &nbsp;&nbsp; &nbsp; <div className={`${styles.txt} item-center`}>Google</div></button>
                    </div>
                    <div className={`${styles.rowRegister} item-center`}>
                         Bạn đã có tài khoản &nbsp; <strong>IT QUY NHƠN</strong> ? &nbsp;<Link href={'/login'}>Đăng nhập</Link>
                    </div>
                </form>
          </div>
        </div>
  </ComponentPage>
  )
}

export default Register
