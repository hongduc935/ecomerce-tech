import React from 'react'
import styles from './styles/banner.module.scss'
import { faArrowRight } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


const Banner = () => {
  return (
    <div className={`${styles.banner} container`}>
        <div className={styles.leftBanner}>
            <h1 className={styles.titleBanner}>IT QUY NHƠN </h1>
            <h2 className={styles.desBanner}>Nền Tảng Mua Bán đồ công nghệ Apple . </h2>
            <div className={styles.btnBanner}>
                <button className={styles.btn}>Tìm hiểu thêm &nbsp;<FontAwesomeIcon icon={faArrowRight} /></button>
            </div>
        </div>
        <div className={styles.rightBanner}>
        
        </div>
    </div>
  )
}

export default Banner
