import React from 'react'
import styles from './styles/introduce-system.module.scss'

const IntroduceSystem = () => {
  return (
    <div className={`${styles.introduceSystem} container`}>
      <h1 className={styles.titlePage}>Nền tảng IT QUY NHƠN </h1>
      <div className={styles.layoutContent}>
          <div className={styles.serviceProvide}>
              <h2 className={styles.titleService}>Nơi giao lưu bán hàng  </h2>
              <div className={styles.contentService}>
                  Nội dung dịch vụ 
              </div>
          </div>
          <div className={styles.serviceProvide}>
              <h2 className={styles.titleService}>Dịch vụ bán hộ </h2>
              <div className={styles.contentService}>

                  Nội dung dịch vụ 
              </div>
          </div>
          <div className={styles.serviceProvide}>
              <h2 className={styles.titleService}>Dịch vụ sửa chữa </h2>
              <div className={styles.contentService}>
                  Nội dung dịch vụ 
              </div>
          </div>
      </div>
    </div>
  )
}

export default IntroduceSystem