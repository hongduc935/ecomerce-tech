import React from 'react'
import styles from './styles/warranty-policy.module.scss'


const WarrantyPolicy = () => {
  return (
    <div className={`${styles.warrantyPolicy} container`}>
      <h2 className={styles.titleSection}>Chính sách bảo hành của IT QUY NHƠN </h2>
      Điều Kiện Bảo Hành

      Sản phẩm còn trong thời gian bảo hành.
      Sản phẩm gặp lỗi kỹ thuật do nhà sản xuất.

      Quy Trình Bảo Hành

      Bước 1: Kiểm tra và xác nhận điều kiện bảo hành.
      Bước 2: Liên hệ bộ phận chăm sóc khách hàng để yêu cầu bảo hành.
      Bước 3: Gửi sản phẩm đến trung tâm bảo hành của IT QUY NHƠN.
      Bước 4: Nhận sản phẩm sau khi bảo hành hoàn tất.

      Thời Gian Bảo Hành

      Thời gian bảo hành tùy thuộc vào từng sản phẩm, thông thường từ 6 tháng đến 1 năm.
    </div>
  )
}

export default WarrantyPolicy