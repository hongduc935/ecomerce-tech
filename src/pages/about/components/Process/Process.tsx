import React from 'react'
import styles from './styles/process.module.scss'


const Process = () => {
  return (
    <div className={`${styles.process} container`}>
      <h2 className={styles.titleSection}>
          Quy Trình Bán Hàng
      </h2>
      This is section Process
    </div>
  )
}

export default Process
