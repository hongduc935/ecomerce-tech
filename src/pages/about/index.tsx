import ComponentPage from '@/components/ComponentPage/ComponentPage'
import { Meta } from '@/layout/Meta'

import React from 'react'
import Banner from './components/Banner/Banner'
import Process from './components/Process/Process'
import { NextPage } from 'next'
import IntroduceSystem from './components/IntroduceSystem/IntroduceSystem'
import WarrantyPolicy from './components/WarrantyPolicy/WarrantyPolicy'

export const ABOUT_PAGE = {
  site_name: 'Starter',
  title: 'IT QUY NHƠN | HOME PAGE',
  description: 'HOME PAGE',
  locale: 'en',
};

const AboutPage: NextPage = () => {
  return (
    <ComponentPage isHiddenFooter={true} isHiddenHeader={true}>
      <Meta title={ABOUT_PAGE.title} description={ABOUT_PAGE.description} />
      <Banner/>
      <IntroduceSystem/>
      <Process/>
      <WarrantyPolicy/>
      {/* giới thiệu nền tảng 
      đội ngũ kiểm định 
      chế độ bảo hành 
      nguồn gốc  */}
    </ComponentPage>
  )
}

export default AboutPage
