// pages/api/auth/[...nextauth].js
import NextAuth from "next-auth";
import GoogleProvider from "next-auth/providers/google";

export default NextAuth({
  providers: [
    GoogleProvider({
      clientId: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    }),
  ],
  secret: process.env.NEXTAUTH_SECRET,
  callbacks: {
    async signIn({ user, account, profile, email, credentials }) {
      // Tùy chỉnh nếu cần, ví dụ: gửi thông tin người dùng tới backend Express.js
      return true;
    },
    async session({ session, token, user }) {
      // Thêm thông tin vào session nếu cần
      return session;
    },
    async jwt({ token, user, account, profile, isNewUser }) {
      // Tùy chỉnh token nếu cần
      return token;
    },
  },
});
