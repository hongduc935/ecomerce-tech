import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { io, Socket } from 'socket.io-client';

// Định nghĩa kiểu dữ liệu của tin nhắn
interface Message {
  message: string;
  sender: string;
  timestamp: string;
}

interface ChatProps {
  sender: string;
  receiver: string;
}

let socket: Socket;

const Chat = ({ sender, receiver }: ChatProps) => {


  sender = "user1"
  receiver = "user2"
  const [message, setMessage] = useState<string>(''); // Lưu tin nhắn hiện tại
  const [messages, setMessages] = useState<Message[]>([]); // Lưu danh sách tin nhắn
  
  const router = useRouter();
  const { roomId, userId } = router.query; 


  // Kết nối tới Socket.IO khi component được mount
  useEffect(() => {
    // Kết nối tới server Socket.IO
    socket = io('http://localhost:4000');

    
    // Tham gia phòng dựa trên ID người gửi
    // const { roomId, userId } = router.query; 
    console.log(roomId)
    console.log(userId) 

    if(roomId && userId ){
      socket.emit('joinRoom', {roomId,userId}); 
       // Lắng nghe tin nhắn mới từ server
      socket.on('receiveMessage', (newMessage: Message) => {

        console.log("receiveMessage: "+ newMessage)
        setMessages((prevMessages) => [...prevMessages, newMessage]);
      });

      return () => {
        socket.off('receiveMessage');
        socket.emit('leaveRoom', { roomId, userId }); // Thông báo khi rời room
      };
    }
    

   

    // Cleanup khi component unmount
    return () => {
      socket.disconnect();
    };
  }, [roomId, userId]);

  // Hàm gửi tin nhắn
  const sendMessage = () => {
    if (message.trim()) {
      // Gửi tin nhắn tới server
      socket.emit('sendMessage', {
        message,
        roomId,
        user_id: userId,
      });

      // Clear nội dung tin nhắn sau khi gửi
      setMessage('');
    }
  };

   // Hàm gửi tin nhắn
  //  const sendMessage = () => {
  //   if (message.trim()) {
  //     const roomId = 1 
  //     socket.emit('sendMessage', {roomId,message}); // Gửi tin nhắn
  //     setMessage(''); // Xóa input sau khi gửi
  //   }
  // };
  return (
    <div>
    <h1>Chat giữa {sender} và {receiver}</h1>

    {/* Hiển thị tin nhắn */}
    <div
      style={{
        border: '1px solid black',
        padding: '10px',
        marginBottom: '20px',
        height: '300px',
        overflowY: 'scroll',
      }}
    >
      {messages.map((msg, index) => (
        <div key={index}>
          <strong>{msg.sender}</strong>: {msg.message}
        </div>
      ))}
    </div>

    {/* Input để nhập nội dung tin nhắn */}
    <input
      type="text"
      value={message}
      onChange={(e) => setMessage(e.target.value)}
      placeholder="Enter your message..."
      style={{ padding: '10px', width: '80%' }}
    />
    <button onClick={sendMessage} style={{ padding: '10px' }}>
      Send
    </button>
  </div>
  )
}

export default Chat