import ComponentPage from '@/components/ComponentPage/ComponentPage'
import React from 'react'
import  {MACBOOK}  from '@/utils/AppConfig'
import { Meta } from '@/layout/Meta'
const Order = () => {
  return (
    <ComponentPage isHiddenFooter={true} isHiddenHeader={true}>
    <Meta title={MACBOOK.title} description={MACBOOK.description} />
    <div>Order Page</div>
    
    </ComponentPage>
  )
}

export default Order