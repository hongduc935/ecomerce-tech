import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

// Định nghĩa kiểu Product
export interface Product {
  id: string;
  name: string;
  price: number;
  description: string;
  category: string;
}

// Định nghĩa kiểu lỗi
interface ApiError {
  status: number;
  message: string;
}

export const api = createApi({
  reducerPath: 'api',
  baseQuery: fetchBaseQuery({
    baseUrl: '/api',
    // Tùy chọn để xử lý lỗi
    prepareHeaders: (headers) => {
      // Có thể thêm các header tùy chỉnh ở đây
      return headers;
    },
  }),
  endpoints: (builder) => ({
    // Endpoint lấy tất cả sản phẩm
    getProducts: builder.query<Product[], void>({
      query: () => 'products',
      // Xử lý lỗi
      transformResponse: (response: Product[] | ApiError) => {
        if ('status' in response) {
          // Nếu có lỗi, bạn có thể xử lý nó ở đây
          throw new Error(response.message);
        }
        return response;
      },
    }),
    // Endpoint lấy sản phẩm theo ID
    getProductById: builder.query<Product, string>({
      query: (id) => `products/${id}`,
      // Xử lý lỗi
      transformResponse: (response: Product | ApiError) => {
        if ('status' in response) {
          throw new Error(response.message);
        }
        return response;
      },
    }),
  }),
});

// Export hooks để sử dụng trong component
export const { useGetProductsQuery, useGetProductByIdQuery } = api;
