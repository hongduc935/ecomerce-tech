// import { configureStore } from "@reduxjs/toolkit";
// import helloWorldReducer from "./reducers/helloWorld";

// export const store = configureStore({
//   reducer: {
//     helloWorld: helloWorldReducer,
//   },
// });

// export type RootState = ReturnType<typeof store.getState>;
// export type AppDispatch = typeof store.dispatch;



// import { configureStore } from '@reduxjs/toolkit'

// export const makeStore = () => {
//   return configureStore({
//     reducer: {},
//   })
// }

// // Infer the type of makeStore
// export type AppStore = ReturnType<typeof makeStore>
// // Infer the `RootState` and `AppDispatch` types from the store itself
// export type RootState = ReturnType<AppStore['getState']>
// export type AppDispatch = AppStore['dispatch']

import { configureStore } from '@reduxjs/toolkit';
import rootReducer from './rootReducer';
import { api } from './services/api';
import { createWrapper } from 'next-redux-wrapper';
import { loggerMiddleware, thunkMiddlewares } from './middleware';
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';

export const store = configureStore({
  reducer: rootReducer, // Thiết lập reducer

  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware()
      .concat(thunkMiddlewares)  // Thêm middleware thunk
      .concat(loggerMiddleware)   // Thêm middleware logger
      .concat(api.middleware),    // Thêm middleware RTK Query

  devTools: process.env.NODE_ENV !== 'production', // Chế độ sử dụng DevTools
});

// Xuất kiểu dữ liệu để sử dụng sau này
export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;

// Cấu hình sử dụng cho server-side
export const makeStore = () => store;
export const wrapper = createWrapper(makeStore);

export type AppStore = ReturnType<typeof makeStore>;