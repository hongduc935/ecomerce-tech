

import { Middleware } from '@reduxjs/toolkit';
import { thunk } from 'redux-thunk';

export const loggerMiddleware: Middleware = (store) => (next) => (action) => {
  console.log('Dispatching:', action);
  let result = next(action);
  console.log('Next state:', store.getState());
  return result;
};


export const thunkMiddlewares: Middleware = thunk;

