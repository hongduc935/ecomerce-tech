import { RootState } from '../../store';

// Lấy trạng thái xác thực (isAuthenticated)
export const selectIsAuthenticated = (state: RootState) => state.auth.isAuthenticated;

// Lấy thông tin người dùng (user)
export const selectUser = (state: RootState) => state.auth.user;
