import { combineReducers } from '@reduxjs/toolkit';
import authReducer from './features/auth/authSlice';
import productReducer from './features/product/productSlice';
import { api } from './services/api';
import storage from "redux-persist/lib/storage";
import { persistReducer } from "redux-persist";


// Khai báo những state cần giữ lại trạng thái khi refresh
const authPersistConfig = {
  key: "auth",
  storage: storage,
  whitelist: ["authState"],
};


const rootReducer = combineReducers({
  reducer: {
    auth: persistReducer(authPersistConfig, authReducer),
    product: productReducer,
    [api.reducerPath]: api.reducer, 
  }
  
  // thêm các reducer khác nếu cần
});

export default rootReducer;
