import axios, {
	AxiosRequestConfig,
	AxiosRequestHeaders,
	AxiosResponse
} from 'axios';
import queryString from 'query-string';
import LocalStorage from './localStorage';
import DefaultConstant from'@/constant/DefaultConstant'
const axiosClient = axios.create({
	baseURL: DefaultConstant.DOMAIN_SERVER,
	timeout: 20000,
	headers: {
		'Content-Type': 'application/json',
		'Access-Control-Allow-Origin': '*'
	},
	paramsSerializer: (params: Record<string, any>) => {
		return queryString.stringify(params);
	}
});

axiosClient.interceptors.request.use(
	(config: AxiosRequestConfig) => {
		const token = LocalStorage.getFromLocalStorage('auth_token'); //token is value
    console.log(token)
		const headers: AxiosRequestHeaders = {
			Authorization: `Bearer ${token}`
		};

		if (token) config.headers = { ...config.headers, ...headers };
		return config;
	},
	err => {
		console.log(err.response);
		return Promise.reject(err);
	}
);

axiosClient.interceptors.response.use(
  (res: AxiosResponse) => {
      // Nếu trả về thành công và có dữ liệu, trả lại dữ liệu từ response
      if (res && res.data) {
          return res.data;
      }
      return res;
  },
  async (err) => {
      // Kiểm tra lỗi từ response
      if (err.response) {
          // Nếu lỗi là 401 (Unauthorized), tức là access token đã hết hạn
          if (err.response.status === 401) {
              try {
                  // Gửi yêu cầu đến API để lấy access token mới thông qua refresh token từ cookie
                  const response = await axiosClient.post('/api/v1/auth/refreshtoken', {}, {
                      // Gửi cookie (chứa refresh token) cùng với request
                      withCredentials: true,
                  });  // Không cần truyền refresh token từ client

                  // Lưu access token mới vào header Authorization
                  const headers: AxiosRequestHeaders = {
                    Authorization: `Bearer ${response.data.access_token}`
                  };
                  err.config.headers = { ...err.config.headers, ...headers }

                  // Retry lại request ban đầu với access token mới
                  return axiosClient(err.config); // retry lại yêu cầu ban đầu
              } catch (refreshError) {
                  // Nếu refresh token không hợp lệ hoặc hết hạn, yêu cầu người dùng đăng nhập lại
                  window.location.href = '/login';
                  return Promise.reject(refreshError);
              }
          }
          // Trường hợp lỗi khác, trả về thông tin lỗi
          return Promise.reject(err.response.data);
      }
      // Nếu không có response, trả về lỗi gốc
      return Promise.reject(err);
  }
);



const Repository = ({
	url,
	method,
	payload
}: {
	url: string;
	method: 'GET' | 'POST' | 'PUT' | 'DELETE';
	payload: any;
}) => {
	const { params, body } = payload;
	switch (method) {
		case 'GET':
			return axiosClient.get(url, { params: params });
		case 'POST':
			return axiosClient.post(`${url}`, body);
		case 'PUT':
			return axiosClient.put(url, body);
		case 'DELETE':
			return axiosClient.delete(url, { params: params });
		default:
			throw new Error('Method not supported');
	}
};

export default Repository;
