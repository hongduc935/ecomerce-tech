import ProductService from '@/apis/product/ProductService';
import useSWR, { mutate } from 'swr';

const fetcher = (url:string) => fetch(url).then(res => res.json());

export default function useProduct() {

  const { data, error } = useSWR( `https://jsonplaceholder.typicode.com/posts` , fetcher);
  // get detail product handle bye id 

  // update product api handle 
  const getAllPoduct = async (newData:any,id:string) => {

    console.log(id)
    const apiUrl = `https://jsonplaceholder.typicode.com/posts`;
    try {
      // Cập nhật dữ liệu tạm thời trong catch để hiển thị lên UI 
      await mutate(apiUrl, async (data:any) => {
        return { ...data, ...newData };
      }, false); 
      const request = {
        category_name:"Mac"
      }
      const res:any = await ProductService.getAllProductByCategory(request);

      if (!res.ok) {
        throw new Error('Failed to update product.');
      }

      // Revalidate dữ liệu từ server cập nhật dữ liệu thực mới nhất lên và change init data
      await mutate(apiUrl);
    } catch (error) {
      console.error('Failed to update product:', error);
      throw error;
    }
  };

  // delete product by id
  const deleteProduct = async (id:string) => {
    const apiUrl = `/api/product/${id}`;
    try {
      // Cập nhật dữ liệu tạm thời
      await mutate(apiUrl, null, false);
      
      // Gửi yêu cầu xóa đến API
      const response = await fetch(apiUrl, { method: 'DELETE' });

      const res = await ProductService.deleteProducts("1")

      console.log("Res"+res)

      if (!response.ok) {
        throw new Error('Failed to delete product.');
      }

      // Revalidate dữ liệu từ server
      await mutate(apiUrl, null);
    } catch (error) {
      console.error('Failed to delete product:', error);
      throw error;
    }
  };

  
  // Tạo mới sản phẩm
  const createProduct = async (newProduct:any) => {
    try {
      // Gửi yêu cầu tạo mới sản phẩm đến API
      const response :any= await ProductService.createProducts(newProduct);

      if (!response.ok) {
        throw new Error('Failed to create product.');
      }

      // Revalidate dữ liệu từ server để cập nhật dữ liệu mới nhất
      await mutate('/api/products');
    } catch (error) {
      console.error('Failed to create product:', error);
      throw error;
    }
  };

  return {
    product: data,
    isLoading: !error && !data,
    isError: error,
    getAllPoduct,
    deleteProduct,
    createProduct
  };
}
